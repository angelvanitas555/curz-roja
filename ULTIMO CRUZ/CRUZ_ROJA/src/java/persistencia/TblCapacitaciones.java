/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_capacitaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblCapacitaciones.findAll", query = "SELECT t FROM TblCapacitaciones t")
    , @NamedQuery(name = "TblCapacitaciones.findByIdCapacitaciones", query = "SELECT t FROM TblCapacitaciones t WHERE t.idCapacitaciones = :idCapacitaciones")
    , @NamedQuery(name = "TblCapacitaciones.findByNombreCapacitacion", query = "SELECT t FROM TblCapacitaciones t WHERE t.nombreCapacitacion = :nombreCapacitacion")
    , @NamedQuery(name = "TblCapacitaciones.findByAnio", query = "SELECT t FROM TblCapacitaciones t WHERE t.anio = :anio")
    , @NamedQuery(name = "TblCapacitaciones.findByInpartidaPor", query = "SELECT t FROM TblCapacitaciones t WHERE t.inpartidaPor = :inpartidaPor")
    , @NamedQuery(name = "TblCapacitaciones.findByNumHoras", query = "SELECT t FROM TblCapacitaciones t WHERE t.numHoras = :numHoras")})
public class TblCapacitaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_capacitaciones")
    private Integer idCapacitaciones;
    @Basic(optional = false)
    @Column(name = "nombre_capacitacion")
    private String nombreCapacitacion;
    @Basic(optional = false)
    @Column(name = "anio")
    private int anio;
    @Basic(optional = false)
    @Column(name = "inpartida_por")
    private String inpartidaPor;
    @Basic(optional = false)
    @Column(name = "num_horas")
    private int numHoras;
    @Lob
    @Column(name = "odserveciones")
    private String odserveciones;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCapacitacion")
    private Collection<TblRegistroCapacidad> tblRegistroCapacidadCollection;

    public TblCapacitaciones() {
    }

    public TblCapacitaciones(Integer idCapacitaciones) {
        this.idCapacitaciones = idCapacitaciones;
    }

    public TblCapacitaciones(Integer idCapacitaciones, String nombreCapacitacion, int anio, String inpartidaPor, int numHoras) {
        this.idCapacitaciones = idCapacitaciones;
        this.nombreCapacitacion = nombreCapacitacion;
        this.anio = anio;
        this.inpartidaPor = inpartidaPor;
        this.numHoras = numHoras;
    }

    public Integer getIdCapacitaciones() {
        return idCapacitaciones;
    }

    public void setIdCapacitaciones(Integer idCapacitaciones) {
        this.idCapacitaciones = idCapacitaciones;
    }

    public String getNombreCapacitacion() {
        return nombreCapacitacion;
    }

    public void setNombreCapacitacion(String nombreCapacitacion) {
        this.nombreCapacitacion = nombreCapacitacion;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getInpartidaPor() {
        return inpartidaPor;
    }

    public void setInpartidaPor(String inpartidaPor) {
        this.inpartidaPor = inpartidaPor;
    }

    public int getNumHoras() {
        return numHoras;
    }

    public void setNumHoras(int numHoras) {
        this.numHoras = numHoras;
    }

    public String getOdserveciones() {
        return odserveciones;
    }

    public void setOdserveciones(String odserveciones) {
        this.odserveciones = odserveciones;
    }

    @XmlTransient
    public Collection<TblRegistroCapacidad> getTblRegistroCapacidadCollection() {
        return tblRegistroCapacidadCollection;
    }

    public void setTblRegistroCapacidadCollection(Collection<TblRegistroCapacidad> tblRegistroCapacidadCollection) {
        this.tblRegistroCapacidadCollection = tblRegistroCapacidadCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCapacitaciones != null ? idCapacitaciones.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblCapacitaciones)) {
            return false;
        }
        TblCapacitaciones other = (TblCapacitaciones) object;
        if ((this.idCapacitaciones == null && other.idCapacitaciones != null) || (this.idCapacitaciones != null && !this.idCapacitaciones.equals(other.idCapacitaciones))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblCapacitaciones[ idCapacitaciones=" + idCapacitaciones + " ]";
    }
    
}
