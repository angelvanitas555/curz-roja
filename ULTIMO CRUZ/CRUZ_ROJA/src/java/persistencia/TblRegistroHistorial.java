/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_registro_historial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblRegistroHistorial.findAll", query = "SELECT t FROM TblRegistroHistorial t")
    , @NamedQuery(name = "TblRegistroHistorial.findByIdRegistroHistorial", query = "SELECT t FROM TblRegistroHistorial t WHERE t.idRegistroHistorial = :idRegistroHistorial")})
public class TblRegistroHistorial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro_historial")
    private Integer idRegistroHistorial;
    @JoinColumn(name = "id_registro", referencedColumnName = "id_registro")
    @ManyToOne(optional = false)
    private TblRegistro idRegistro;
    @JoinColumn(name = "id_historial_cargo", referencedColumnName = "id_historial_cargo")
    @ManyToOne(optional = false)
    private TblHistorialCargo idHistorialCargo;

    public TblRegistroHistorial() {
    }

    public TblRegistroHistorial(Integer idRegistroHistorial) {
        this.idRegistroHistorial = idRegistroHistorial;
    }

    public Integer getIdRegistroHistorial() {
        return idRegistroHistorial;
    }

    public void setIdRegistroHistorial(Integer idRegistroHistorial) {
        this.idRegistroHistorial = idRegistroHistorial;
    }

    public TblRegistro getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(TblRegistro idRegistro) {
        this.idRegistro = idRegistro;
    }

    public TblHistorialCargo getIdHistorialCargo() {
        return idHistorialCargo;
    }

    public void setIdHistorialCargo(TblHistorialCargo idHistorialCargo) {
        this.idHistorialCargo = idHistorialCargo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistroHistorial != null ? idRegistroHistorial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblRegistroHistorial)) {
            return false;
        }
        TblRegistroHistorial other = (TblRegistroHistorial) object;
        if ((this.idRegistroHistorial == null && other.idRegistroHistorial != null) || (this.idRegistroHistorial != null && !this.idRegistroHistorial.equals(other.idRegistroHistorial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblRegistroHistorial[ idRegistroHistorial=" + idRegistroHistorial + " ]";
    }
    
}
