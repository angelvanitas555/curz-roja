/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_datos_medicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblDatosMedicos.findAll", query = "SELECT t FROM TblDatosMedicos t")
    , @NamedQuery(name = "TblDatosMedicos.findByIdDatosMedicos", query = "SELECT t FROM TblDatosMedicos t WHERE t.idDatosMedicos = :idDatosMedicos")
    , @NamedQuery(name = "TblDatosMedicos.findByTipoSanguineo", query = "SELECT t FROM TblDatosMedicos t WHERE t.tipoSanguineo = :tipoSanguineo")
    , @NamedQuery(name = "TblDatosMedicos.findByPeso", query = "SELECT t FROM TblDatosMedicos t WHERE t.peso = :peso")
    , @NamedQuery(name = "TblDatosMedicos.findByAltura", query = "SELECT t FROM TblDatosMedicos t WHERE t.altura = :altura")
    , @NamedQuery(name = "TblDatosMedicos.findByAlergias", query = "SELECT t FROM TblDatosMedicos t WHERE t.alergias = :alergias")
    , @NamedQuery(name = "TblDatosMedicos.findByEpilepcia", query = "SELECT t FROM TblDatosMedicos t WHERE t.epilepcia = :epilepcia")
    , @NamedQuery(name = "TblDatosMedicos.findByAsma", query = "SELECT t FROM TblDatosMedicos t WHERE t.asma = :asma")
    , @NamedQuery(name = "TblDatosMedicos.findByRiniones", query = "SELECT t FROM TblDatosMedicos t WHERE t.riniones = :riniones")
    , @NamedQuery(name = "TblDatosMedicos.findByCardiaco", query = "SELECT t FROM TblDatosMedicos t WHERE t.cardiaco = :cardiaco")
    , @NamedQuery(name = "TblDatosMedicos.findByCancer", query = "SELECT t FROM TblDatosMedicos t WHERE t.cancer = :cancer")
    , @NamedQuery(name = "TblDatosMedicos.findByHepatitis", query = "SELECT t FROM TblDatosMedicos t WHERE t.hepatitis = :hepatitis")
    , @NamedQuery(name = "TblDatosMedicos.findByCirrosis", query = "SELECT t FROM TblDatosMedicos t WHERE t.cirrosis = :cirrosis")
    , @NamedQuery(name = "TblDatosMedicos.findByAparatoDigestivo", query = "SELECT t FROM TblDatosMedicos t WHERE t.aparatoDigestivo = :aparatoDigestivo")
    , @NamedQuery(name = "TblDatosMedicos.findBySida", query = "SELECT t FROM TblDatosMedicos t WHERE t.sida = :sida")
    , @NamedQuery(name = "TblDatosMedicos.findByProbRespiratorio", query = "SELECT t FROM TblDatosMedicos t WHERE t.probRespiratorio = :probRespiratorio")
    , @NamedQuery(name = "TblDatosMedicos.findByMareos", query = "SELECT t FROM TblDatosMedicos t WHERE t.mareos = :mareos")
    , @NamedQuery(name = "TblDatosMedicos.findByDiabetico", query = "SELECT t FROM TblDatosMedicos t WHERE t.diabetico = :diabetico")
    , @NamedQuery(name = "TblDatosMedicos.findByEnfermedadesVenereas", query = "SELECT t FROM TblDatosMedicos t WHERE t.enfermedadesVenereas = :enfermedadesVenereas")
    , @NamedQuery(name = "TblDatosMedicos.findByNinguna", query = "SELECT t FROM TblDatosMedicos t WHERE t.ninguna = :ninguna")
    , @NamedQuery(name = "TblDatosMedicos.findByServAsistencialMedico", query = "SELECT t FROM TblDatosMedicos t WHERE t.servAsistencialMedico = :servAsistencialMedico")
    , @NamedQuery(name = "TblDatosMedicos.findByTelefono", query = "SELECT t FROM TblDatosMedicos t WHERE t.telefono = :telefono")
    , @NamedQuery(name = "TblDatosMedicos.findByAccidenteNombre", query = "SELECT t FROM TblDatosMedicos t WHERE t.accidenteNombre = :accidenteNombre")
    , @NamedQuery(name = "TblDatosMedicos.findByAccidenteDireccion", query = "SELECT t FROM TblDatosMedicos t WHERE t.accidenteDireccion = :accidenteDireccion")
    , @NamedQuery(name = "TblDatosMedicos.findByAccidenteTelefono", query = "SELECT t FROM TblDatosMedicos t WHERE t.accidenteTelefono = :accidenteTelefono")})
public class TblDatosMedicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_datos_medicos")
    private Integer idDatosMedicos;
    @Column(name = "tipo_sanguineo")
    private String tipoSanguineo;
    @Basic(optional = false)
    @Column(name = "peso")
    private long peso;
    @Basic(optional = false)
    @Column(name = "altura")
    private long altura;
    @Column(name = "alergias")
    private Boolean alergias;
    @Column(name = "epilepcia")
    private Boolean epilepcia;
    @Column(name = "asma")
    private Boolean asma;
    @Column(name = "riniones")
    private Boolean riniones;
    @Column(name = "cardiaco")
    private Boolean cardiaco;
    @Column(name = "cancer")
    private Boolean cancer;
    @Column(name = "hepatitis")
    private Boolean hepatitis;
    @Column(name = "cirrosis")
    private Boolean cirrosis;
    @Column(name = "aparato_digestivo")
    private Boolean aparatoDigestivo;
    @Column(name = "sida")
    private Boolean sida;
    @Column(name = "prob_respiratorio")
    private Boolean probRespiratorio;
    @Column(name = "mareos")
    private Boolean mareos;
    @Column(name = "diabetico")
    private Boolean diabetico;
    @Column(name = "enfermedades_venereas")
    private String enfermedadesVenereas;
    @Lob
    @Column(name = "otras")
    private String otras;
    @Column(name = "ninguna")
    private Boolean ninguna;
    @Column(name = "serv_asistencial_medico")
    private String servAsistencialMedico;
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @Column(name = "accidente_nombre")
    private String accidenteNombre;
    @Basic(optional = false)
    @Column(name = "accidente_direccion")
    private String accidenteDireccion;
    @Basic(optional = false)
    @Column(name = "accidente_telefono")
    private String accidenteTelefono;
    @JoinColumn(name = "id_solicitud", referencedColumnName = "id_solicitud")
    @ManyToOne(optional = false)
    private TblSolicitudIngreso idSolicitud;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatosMedicos")
    private Collection<TblDireccion> tblDireccionCollection;

    public TblDatosMedicos() {
    }

    public TblDatosMedicos(Integer idDatosMedicos) {
        this.idDatosMedicos = idDatosMedicos;
    }

    public TblDatosMedicos(Integer idDatosMedicos, long peso, long altura, String accidenteNombre, String accidenteDireccion, String accidenteTelefono) {
        this.idDatosMedicos = idDatosMedicos;
        this.peso = peso;
        this.altura = altura;
        this.accidenteNombre = accidenteNombre;
        this.accidenteDireccion = accidenteDireccion;
        this.accidenteTelefono = accidenteTelefono;
    }

    public Integer getIdDatosMedicos() {
        return idDatosMedicos;
    }

    public void setIdDatosMedicos(Integer idDatosMedicos) {
        this.idDatosMedicos = idDatosMedicos;
    }

    public String getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(String tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    public long getPeso() {
        return peso;
    }

    public void setPeso(long peso) {
        this.peso = peso;
    }

    public long getAltura() {
        return altura;
    }

    public void setAltura(long altura) {
        this.altura = altura;
    }

    public Boolean getAlergias() {
        return alergias;
    }

    public void setAlergias(Boolean alergias) {
        this.alergias = alergias;
    }

    public Boolean getEpilepcia() {
        return epilepcia;
    }

    public void setEpilepcia(Boolean epilepcia) {
        this.epilepcia = epilepcia;
    }

    public Boolean getAsma() {
        return asma;
    }

    public void setAsma(Boolean asma) {
        this.asma = asma;
    }

    public Boolean getRiniones() {
        return riniones;
    }

    public void setRiniones(Boolean riniones) {
        this.riniones = riniones;
    }

    public Boolean getCardiaco() {
        return cardiaco;
    }

    public void setCardiaco(Boolean cardiaco) {
        this.cardiaco = cardiaco;
    }

    public Boolean getCancer() {
        return cancer;
    }

    public void setCancer(Boolean cancer) {
        this.cancer = cancer;
    }

    public Boolean getHepatitis() {
        return hepatitis;
    }

    public void setHepatitis(Boolean hepatitis) {
        this.hepatitis = hepatitis;
    }

    public Boolean getCirrosis() {
        return cirrosis;
    }

    public void setCirrosis(Boolean cirrosis) {
        this.cirrosis = cirrosis;
    }

    public Boolean getAparatoDigestivo() {
        return aparatoDigestivo;
    }

    public void setAparatoDigestivo(Boolean aparatoDigestivo) {
        this.aparatoDigestivo = aparatoDigestivo;
    }

    public Boolean getSida() {
        return sida;
    }

    public void setSida(Boolean sida) {
        this.sida = sida;
    }

    public Boolean getProbRespiratorio() {
        return probRespiratorio;
    }

    public void setProbRespiratorio(Boolean probRespiratorio) {
        this.probRespiratorio = probRespiratorio;
    }

    public Boolean getMareos() {
        return mareos;
    }

    public void setMareos(Boolean mareos) {
        this.mareos = mareos;
    }

    public Boolean getDiabetico() {
        return diabetico;
    }

    public void setDiabetico(Boolean diabetico) {
        this.diabetico = diabetico;
    }

    public String getEnfermedadesVenereas() {
        return enfermedadesVenereas;
    }

    public void setEnfermedadesVenereas(String enfermedadesVenereas) {
        this.enfermedadesVenereas = enfermedadesVenereas;
    }

    public String getOtras() {
        return otras;
    }

    public void setOtras(String otras) {
        this.otras = otras;
    }

    public Boolean getNinguna() {
        return ninguna;
    }

    public void setNinguna(Boolean ninguna) {
        this.ninguna = ninguna;
    }

    public String getServAsistencialMedico() {
        return servAsistencialMedico;
    }

    public void setServAsistencialMedico(String servAsistencialMedico) {
        this.servAsistencialMedico = servAsistencialMedico;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getAccidenteNombre() {
        return accidenteNombre;
    }

    public void setAccidenteNombre(String accidenteNombre) {
        this.accidenteNombre = accidenteNombre;
    }

    public String getAccidenteDireccion() {
        return accidenteDireccion;
    }

    public void setAccidenteDireccion(String accidenteDireccion) {
        this.accidenteDireccion = accidenteDireccion;
    }

    public String getAccidenteTelefono() {
        return accidenteTelefono;
    }

    public void setAccidenteTelefono(String accidenteTelefono) {
        this.accidenteTelefono = accidenteTelefono;
    }

    public TblSolicitudIngreso getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(TblSolicitudIngreso idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @XmlTransient
    public Collection<TblDireccion> getTblDireccionCollection() {
        return tblDireccionCollection;
    }

    public void setTblDireccionCollection(Collection<TblDireccion> tblDireccionCollection) {
        this.tblDireccionCollection = tblDireccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDatosMedicos != null ? idDatosMedicos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblDatosMedicos)) {
            return false;
        }
        TblDatosMedicos other = (TblDatosMedicos) object;
        if ((this.idDatosMedicos == null && other.idDatosMedicos != null) || (this.idDatosMedicos != null && !this.idDatosMedicos.equals(other.idDatosMedicos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblDatosMedicos[ idDatosMedicos=" + idDatosMedicos + " ]";
    }
    
}
