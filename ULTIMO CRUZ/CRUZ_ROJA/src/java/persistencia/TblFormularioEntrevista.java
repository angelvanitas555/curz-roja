/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_formulario_entrevista")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblFormularioEntrevista.findAll", query = "SELECT t FROM TblFormularioEntrevista t")
    , @NamedQuery(name = "TblFormularioEntrevista.findByIdformularioEntrevista", query = "SELECT t FROM TblFormularioEntrevista t WHERE t.idformularioEntrevista = :idformularioEntrevista")
    , @NamedQuery(name = "TblFormularioEntrevista.findByPasatiempo", query = "SELECT t FROM TblFormularioEntrevista t WHERE t.pasatiempo = :pasatiempo")
    , @NamedQuery(name = "TblFormularioEntrevista.findBySociable", query = "SELECT t FROM TblFormularioEntrevista t WHERE t.sociable = :sociable")
    , @NamedQuery(name = "TblFormularioEntrevista.findByConoceDeCruzRoja", query = "SELECT t FROM TblFormularioEntrevista t WHERE t.conoceDeCruzRoja = :conoceDeCruzRoja")
    , @NamedQuery(name = "TblFormularioEntrevista.findByTrabajaEnEquipo", query = "SELECT t FROM TblFormularioEntrevista t WHERE t.trabajaEnEquipo = :trabajaEnEquipo")
    , @NamedQuery(name = "TblFormularioEntrevista.findByDiponibilidad", query = "SELECT t FROM TblFormularioEntrevista t WHERE t.diponibilidad = :diponibilidad")})
public class TblFormularioEntrevista implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_formularioEntrevista")
    private Integer idformularioEntrevista;
    @Basic(optional = false)
    @Lob
    @Column(name = "fortalezas")
    private String fortalezas;
    @Basic(optional = false)
    @Lob
    @Column(name = "oportunidades")
    private String oportunidades;
    @Basic(optional = false)
    @Lob
    @Column(name = "devilidades")
    private String devilidades;
    @Basic(optional = false)
    @Lob
    @Column(name = "amenazas")
    private String amenazas;
    @Basic(optional = false)
    @Lob
    @Column(name = "metas_corto_plazo")
    private String metasCortoPlazo;
    @Basic(optional = false)
    @Column(name = "pasatiempo")
    private String pasatiempo;
    @Basic(optional = false)
    @Column(name = "sociable")
    private boolean sociable;
    @Basic(optional = false)
    @Lob
    @Column(name = "exp_es_sociable")
    private String expEsSociable;
    @Basic(optional = false)
    @Column(name = "conoce_de_cruz_roja")
    private boolean conoceDeCruzRoja;
    @Lob
    @Column(name = "explique")
    private String explique;
    @Basic(optional = false)
    @Lob
    @Column(name = "motivo_ingreso")
    private String motivoIngreso;
    @Basic(optional = false)
    @Lob
    @Column(name = "expectativas")
    private String expectativas;
    @Lob
    @Column(name = "que_ofrece_a_institucion")
    private String queOfreceAInstitucion;
    @Basic(optional = false)
    @Lob
    @Column(name = "voluntariado")
    private String voluntariado;
    @Basic(optional = false)
    @Lob
    @Column(name = "veneficios_a_comunidad")
    private String veneficiosAComunidad;
    @Basic(optional = false)
    @Lob
    @Column(name = "areas_mayor_interes")
    private String areasMayorInteres;
    @Basic(optional = false)
    @Column(name = "trabaja_en_equipo")
    private boolean trabajaEnEquipo;
    @Basic(optional = false)
    @Column(name = "diponibilidad")
    private boolean diponibilidad;
    @JoinColumn(name = "id_solicitud", referencedColumnName = "id_solicitud")
    @ManyToOne(optional = false)
    private TblSolicitudIngreso idSolicitud;

    public TblFormularioEntrevista() {
    }

    public TblFormularioEntrevista(Integer idformularioEntrevista) {
        this.idformularioEntrevista = idformularioEntrevista;
    }

    public TblFormularioEntrevista(Integer idformularioEntrevista, String fortalezas, String oportunidades, String devilidades, String amenazas, String metasCortoPlazo, String pasatiempo, boolean sociable, String expEsSociable, boolean conoceDeCruzRoja, String motivoIngreso, String expectativas, String voluntariado, String veneficiosAComunidad, String areasMayorInteres, boolean trabajaEnEquipo, boolean diponibilidad) {
        this.idformularioEntrevista = idformularioEntrevista;
        this.fortalezas = fortalezas;
        this.oportunidades = oportunidades;
        this.devilidades = devilidades;
        this.amenazas = amenazas;
        this.metasCortoPlazo = metasCortoPlazo;
        this.pasatiempo = pasatiempo;
        this.sociable = sociable;
        this.expEsSociable = expEsSociable;
        this.conoceDeCruzRoja = conoceDeCruzRoja;
        this.motivoIngreso = motivoIngreso;
        this.expectativas = expectativas;
        this.voluntariado = voluntariado;
        this.veneficiosAComunidad = veneficiosAComunidad;
        this.areasMayorInteres = areasMayorInteres;
        this.trabajaEnEquipo = trabajaEnEquipo;
        this.diponibilidad = diponibilidad;
    }

    public Integer getIdformularioEntrevista() {
        return idformularioEntrevista;
    }

    public void setIdformularioEntrevista(Integer idformularioEntrevista) {
        this.idformularioEntrevista = idformularioEntrevista;
    }

    public String getFortalezas() {
        return fortalezas;
    }

    public void setFortalezas(String fortalezas) {
        this.fortalezas = fortalezas;
    }

    public String getOportunidades() {
        return oportunidades;
    }

    public void setOportunidades(String oportunidades) {
        this.oportunidades = oportunidades;
    }

    public String getDevilidades() {
        return devilidades;
    }

    public void setDevilidades(String devilidades) {
        this.devilidades = devilidades;
    }

    public String getAmenazas() {
        return amenazas;
    }

    public void setAmenazas(String amenazas) {
        this.amenazas = amenazas;
    }

    public String getMetasCortoPlazo() {
        return metasCortoPlazo;
    }

    public void setMetasCortoPlazo(String metasCortoPlazo) {
        this.metasCortoPlazo = metasCortoPlazo;
    }

    public String getPasatiempo() {
        return pasatiempo;
    }

    public void setPasatiempo(String pasatiempo) {
        this.pasatiempo = pasatiempo;
    }

    public boolean getSociable() {
        return sociable;
    }

    public void setSociable(boolean sociable) {
        this.sociable = sociable;
    }

    public String getExpEsSociable() {
        return expEsSociable;
    }

    public void setExpEsSociable(String expEsSociable) {
        this.expEsSociable = expEsSociable;
    }

    public boolean getConoceDeCruzRoja() {
        return conoceDeCruzRoja;
    }

    public void setConoceDeCruzRoja(boolean conoceDeCruzRoja) {
        this.conoceDeCruzRoja = conoceDeCruzRoja;
    }

    public String getExplique() {
        return explique;
    }

    public void setExplique(String explique) {
        this.explique = explique;
    }

    public String getMotivoIngreso() {
        return motivoIngreso;
    }

    public void setMotivoIngreso(String motivoIngreso) {
        this.motivoIngreso = motivoIngreso;
    }

    public String getExpectativas() {
        return expectativas;
    }

    public void setExpectativas(String expectativas) {
        this.expectativas = expectativas;
    }

    public String getQueOfreceAInstitucion() {
        return queOfreceAInstitucion;
    }

    public void setQueOfreceAInstitucion(String queOfreceAInstitucion) {
        this.queOfreceAInstitucion = queOfreceAInstitucion;
    }

    public String getVoluntariado() {
        return voluntariado;
    }

    public void setVoluntariado(String voluntariado) {
        this.voluntariado = voluntariado;
    }

    public String getVeneficiosAComunidad() {
        return veneficiosAComunidad;
    }

    public void setVeneficiosAComunidad(String veneficiosAComunidad) {
        this.veneficiosAComunidad = veneficiosAComunidad;
    }

    public String getAreasMayorInteres() {
        return areasMayorInteres;
    }

    public void setAreasMayorInteres(String areasMayorInteres) {
        this.areasMayorInteres = areasMayorInteres;
    }

    public boolean getTrabajaEnEquipo() {
        return trabajaEnEquipo;
    }

    public void setTrabajaEnEquipo(boolean trabajaEnEquipo) {
        this.trabajaEnEquipo = trabajaEnEquipo;
    }

    public boolean getDiponibilidad() {
        return diponibilidad;
    }

    public void setDiponibilidad(boolean diponibilidad) {
        this.diponibilidad = diponibilidad;
    }

    public TblSolicitudIngreso getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(TblSolicitudIngreso idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idformularioEntrevista != null ? idformularioEntrevista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblFormularioEntrevista)) {
            return false;
        }
        TblFormularioEntrevista other = (TblFormularioEntrevista) object;
        if ((this.idformularioEntrevista == null && other.idformularioEntrevista != null) || (this.idformularioEntrevista != null && !this.idformularioEntrevista.equals(other.idformularioEntrevista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblFormularioEntrevista[ idformularioEntrevista=" + idformularioEntrevista + " ]";
    }
    
}
