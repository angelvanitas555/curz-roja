/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_datos_academicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblDatosAcademicos.findAll", query = "SELECT t FROM TblDatosAcademicos t")
    , @NamedQuery(name = "TblDatosAcademicos.findByIdDatosAcademicos", query = "SELECT t FROM TblDatosAcademicos t WHERE t.idDatosAcademicos = :idDatosAcademicos")
    , @NamedQuery(name = "TblDatosAcademicos.findByEstudios", query = "SELECT t FROM TblDatosAcademicos t WHERE t.estudios = :estudios")
    , @NamedQuery(name = "TblDatosAcademicos.findByLugar", query = "SELECT t FROM TblDatosAcademicos t WHERE t.lugar = :lugar")
    , @NamedQuery(name = "TblDatosAcademicos.findByAnio", query = "SELECT t FROM TblDatosAcademicos t WHERE t.anio = :anio")})
public class TblDatosAcademicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_datos_academicos")
    private Integer idDatosAcademicos;
    @Basic(optional = false)
    @Column(name = "estudios")
    private String estudios;
    @Basic(optional = false)
    @Column(name = "lugar")
    private String lugar;
    @Basic(optional = false)
    @Column(name = "anio")
    private int anio;
    @JoinColumn(name = "id_dat_person", referencedColumnName = "id_dat_person")
    @ManyToOne(optional = false)
    private TblDatosPersonales idDatPerson;

    public TblDatosAcademicos() {
    }

    public TblDatosAcademicos(Integer idDatosAcademicos) {
        this.idDatosAcademicos = idDatosAcademicos;
    }

    public TblDatosAcademicos(Integer idDatosAcademicos, String estudios, String lugar, int anio) {
        this.idDatosAcademicos = idDatosAcademicos;
        this.estudios = estudios;
        this.lugar = lugar;
        this.anio = anio;
    }

    public Integer getIdDatosAcademicos() {
        return idDatosAcademicos;
    }

    public void setIdDatosAcademicos(Integer idDatosAcademicos) {
        this.idDatosAcademicos = idDatosAcademicos;
    }

    public String getEstudios() {
        return estudios;
    }

    public void setEstudios(String estudios) {
        this.estudios = estudios;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public TblDatosPersonales getIdDatPerson() {
        return idDatPerson;
    }

    public void setIdDatPerson(TblDatosPersonales idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDatosAcademicos != null ? idDatosAcademicos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblDatosAcademicos)) {
            return false;
        }
        TblDatosAcademicos other = (TblDatosAcademicos) object;
        if ((this.idDatosAcademicos == null && other.idDatosAcademicos != null) || (this.idDatosAcademicos != null && !this.idDatosAcademicos.equals(other.idDatosAcademicos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblDatosAcademicos[ idDatosAcademicos=" + idDatosAcademicos + " ]";
    }
    
}
