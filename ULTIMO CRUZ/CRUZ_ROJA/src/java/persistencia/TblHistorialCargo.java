/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_historial_cargo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblHistorialCargo.findAll", query = "SELECT t FROM TblHistorialCargo t")
    , @NamedQuery(name = "TblHistorialCargo.findByIdHistorialCargo", query = "SELECT t FROM TblHistorialCargo t WHERE t.idHistorialCargo = :idHistorialCargo")
    , @NamedQuery(name = "TblHistorialCargo.findByNombreCargo", query = "SELECT t FROM TblHistorialCargo t WHERE t.nombreCargo = :nombreCargo")
    , @NamedQuery(name = "TblHistorialCargo.findByPeriodoCargo", query = "SELECT t FROM TblHistorialCargo t WHERE t.periodoCargo = :periodoCargo")})
public class TblHistorialCargo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_historial_cargo")
    private Integer idHistorialCargo;
    @Basic(optional = false)
    @Column(name = "nombre_cargo")
    private String nombreCargo;
    @Basic(optional = false)
    @Column(name = "periodo_cargo")
    private String periodoCargo;
    @Lob
    @Column(name = "observaciones")
    private String observaciones;
    @JoinColumn(name = "id_seccion", referencedColumnName = "id_seccion")
    @ManyToOne(optional = false)
    private TblSeccion idSeccion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idHistorialCargo")
    private Collection<TblRegistroHistorial> tblRegistroHistorialCollection;

    public TblHistorialCargo() {
    }

    public TblHistorialCargo(Integer idHistorialCargo) {
        this.idHistorialCargo = idHistorialCargo;
    }

    public TblHistorialCargo(Integer idHistorialCargo, String nombreCargo, String periodoCargo) {
        this.idHistorialCargo = idHistorialCargo;
        this.nombreCargo = nombreCargo;
        this.periodoCargo = periodoCargo;
    }

    public Integer getIdHistorialCargo() {
        return idHistorialCargo;
    }

    public void setIdHistorialCargo(Integer idHistorialCargo) {
        this.idHistorialCargo = idHistorialCargo;
    }

    public String getNombreCargo() {
        return nombreCargo;
    }

    public void setNombreCargo(String nombreCargo) {
        this.nombreCargo = nombreCargo;
    }

    public String getPeriodoCargo() {
        return periodoCargo;
    }

    public void setPeriodoCargo(String periodoCargo) {
        this.periodoCargo = periodoCargo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public TblSeccion getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(TblSeccion idSeccion) {
        this.idSeccion = idSeccion;
    }

    @XmlTransient
    public Collection<TblRegistroHistorial> getTblRegistroHistorialCollection() {
        return tblRegistroHistorialCollection;
    }

    public void setTblRegistroHistorialCollection(Collection<TblRegistroHistorial> tblRegistroHistorialCollection) {
        this.tblRegistroHistorialCollection = tblRegistroHistorialCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHistorialCargo != null ? idHistorialCargo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblHistorialCargo)) {
            return false;
        }
        TblHistorialCargo other = (TblHistorialCargo) object;
        if ((this.idHistorialCargo == null && other.idHistorialCargo != null) || (this.idHistorialCargo != null && !this.idHistorialCargo.equals(other.idHistorialCargo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblHistorialCargo[ idHistorialCargo=" + idHistorialCargo + " ]";
    }
    
}
