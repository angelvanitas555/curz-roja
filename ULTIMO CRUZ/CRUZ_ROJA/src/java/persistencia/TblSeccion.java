/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_seccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblSeccion.findAll", query = "SELECT t FROM TblSeccion t")
    , @NamedQuery(name = "TblSeccion.findByIdSeccion", query = "SELECT t FROM TblSeccion t WHERE t.idSeccion = :idSeccion")
    , @NamedQuery(name = "TblSeccion.findByNombreSeccion", query = "SELECT t FROM TblSeccion t WHERE t.nombreSeccion = :nombreSeccion")})
public class TblSeccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_seccion")
    private Integer idSeccion;
    @Basic(optional = false)
    @Column(name = "nombre_seccion")
    private String nombreSeccion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSeccion")
    private Collection<TblHistorialCargo> tblHistorialCargoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSeccion")
    private Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSeccion")
    private Collection<TblDireccion> tblDireccionCollection;

    public TblSeccion() {
    }

    public TblSeccion(Integer idSeccion) {
        this.idSeccion = idSeccion;
    }

    public TblSeccion(Integer idSeccion, String nombreSeccion) {
        this.idSeccion = idSeccion;
        this.nombreSeccion = nombreSeccion;
    }

    public Integer getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Integer idSeccion) {
        this.idSeccion = idSeccion;
    }

    public String getNombreSeccion() {
        return nombreSeccion;
    }

    public void setNombreSeccion(String nombreSeccion) {
        this.nombreSeccion = nombreSeccion;
    }

    @XmlTransient
    public Collection<TblHistorialCargo> getTblHistorialCargoCollection() {
        return tblHistorialCargoCollection;
    }

    public void setTblHistorialCargoCollection(Collection<TblHistorialCargo> tblHistorialCargoCollection) {
        this.tblHistorialCargoCollection = tblHistorialCargoCollection;
    }

    @XmlTransient
    public Collection<TblSolicitudIngreso> getTblSolicitudIngresoCollection() {
        return tblSolicitudIngresoCollection;
    }

    public void setTblSolicitudIngresoCollection(Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection) {
        this.tblSolicitudIngresoCollection = tblSolicitudIngresoCollection;
    }

    @XmlTransient
    public Collection<TblDireccion> getTblDireccionCollection() {
        return tblDireccionCollection;
    }

    public void setTblDireccionCollection(Collection<TblDireccion> tblDireccionCollection) {
        this.tblDireccionCollection = tblDireccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSeccion != null ? idSeccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblSeccion)) {
            return false;
        }
        TblSeccion other = (TblSeccion) object;
        if ((this.idSeccion == null && other.idSeccion != null) || (this.idSeccion != null && !this.idSeccion.equals(other.idSeccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblSeccion[ idSeccion=" + idSeccion + " ]";
    }
    
}
