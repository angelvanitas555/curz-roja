/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_registro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblRegistro.findAll", query = "SELECT t FROM TblRegistro t")
    , @NamedQuery(name = "TblRegistro.findByIdRegistro", query = "SELECT t FROM TblRegistro t WHERE t.idRegistro = :idRegistro")
    , @NamedQuery(name = "TblRegistro.findByFechaRegistro", query = "SELECT t FROM TblRegistro t WHERE t.fechaRegistro = :fechaRegistro")
    , @NamedQuery(name = "TblRegistro.findByCargo", query = "SELECT t FROM TblRegistro t WHERE t.cargo = :cargo")})
public class TblRegistro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro")
    private Integer idRegistro;
    @Basic(optional = false)
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date fechaRegistro;
    @Basic(optional = false)
    @Column(name = "cargo")
    private String cargo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRegistro")
    private Collection<TblRegistroCapacidad> tblRegistroCapacidadCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRegistro")
    private Collection<TblRegistroHistorial> tblRegistroHistorialCollection;
    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
    @ManyToOne(optional = false)
    private TblArea idArea;
    @JoinColumn(name = "id_solicitud", referencedColumnName = "id_solicitud")
    @ManyToOne(optional = false)
    private TblSolicitudIngreso idSolicitud;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRegistro")
    private Collection<TblSolicitudAfiliacion> tblSolicitudAfiliacionCollection;

    public TblRegistro() {
    }

    public TblRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public TblRegistro(Integer idRegistro, Date fechaRegistro, String cargo) {
        this.idRegistro = idRegistro;
        this.fechaRegistro = fechaRegistro;
        this.cargo = cargo;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @XmlTransient
    public Collection<TblRegistroCapacidad> getTblRegistroCapacidadCollection() {
        return tblRegistroCapacidadCollection;
    }

    public void setTblRegistroCapacidadCollection(Collection<TblRegistroCapacidad> tblRegistroCapacidadCollection) {
        this.tblRegistroCapacidadCollection = tblRegistroCapacidadCollection;
    }

    @XmlTransient
    public Collection<TblRegistroHistorial> getTblRegistroHistorialCollection() {
        return tblRegistroHistorialCollection;
    }

    public void setTblRegistroHistorialCollection(Collection<TblRegistroHistorial> tblRegistroHistorialCollection) {
        this.tblRegistroHistorialCollection = tblRegistroHistorialCollection;
    }

    public TblArea getIdArea() {
        return idArea;
    }

    public void setIdArea(TblArea idArea) {
        this.idArea = idArea;
    }

    public TblSolicitudIngreso getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(TblSolicitudIngreso idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @XmlTransient
    public Collection<TblSolicitudAfiliacion> getTblSolicitudAfiliacionCollection() {
        return tblSolicitudAfiliacionCollection;
    }

    public void setTblSolicitudAfiliacionCollection(Collection<TblSolicitudAfiliacion> tblSolicitudAfiliacionCollection) {
        this.tblSolicitudAfiliacionCollection = tblSolicitudAfiliacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblRegistro)) {
            return false;
        }
        TblRegistro other = (TblRegistro) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblRegistro[ idRegistro=" + idRegistro + " ]";
    }
    
}
