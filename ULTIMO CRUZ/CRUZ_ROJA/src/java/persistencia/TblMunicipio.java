/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_municipio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblMunicipio.findAll", query = "SELECT t FROM TblMunicipio t")
    , @NamedQuery(name = "TblMunicipio.findByIdMunicipio", query = "SELECT t FROM TblMunicipio t WHERE t.idMunicipio = :idMunicipio")
    , @NamedQuery(name = "TblMunicipio.findByNombreMunicipio", query = "SELECT t FROM TblMunicipio t WHERE t.nombreMunicipio = :nombreMunicipio")})
public class TblMunicipio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_municipio")
    private Integer idMunicipio;
    @Basic(optional = false)
    @Column(name = "nombre_municipio")
    private String nombreMunicipio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMunicipio")
    private Collection<TblDepartamento> tblDepartamentoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMunicipio")
    private Collection<TblDireccion> tblDireccionCollection;

    public TblMunicipio() {
    }

    public TblMunicipio(Integer idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public TblMunicipio(Integer idMunicipio, String nombreMunicipio) {
        this.idMunicipio = idMunicipio;
        this.nombreMunicipio = nombreMunicipio;
    }

    public Integer getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(Integer idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    @XmlTransient
    public Collection<TblDepartamento> getTblDepartamentoCollection() {
        return tblDepartamentoCollection;
    }

    public void setTblDepartamentoCollection(Collection<TblDepartamento> tblDepartamentoCollection) {
        this.tblDepartamentoCollection = tblDepartamentoCollection;
    }

    @XmlTransient
    public Collection<TblDireccion> getTblDireccionCollection() {
        return tblDireccionCollection;
    }

    public void setTblDireccionCollection(Collection<TblDireccion> tblDireccionCollection) {
        this.tblDireccionCollection = tblDireccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMunicipio != null ? idMunicipio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblMunicipio)) {
            return false;
        }
        TblMunicipio other = (TblMunicipio) object;
        if ((this.idMunicipio == null && other.idMunicipio != null) || (this.idMunicipio != null && !this.idMunicipio.equals(other.idMunicipio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblMunicipio[ idMunicipio=" + idMunicipio + " ]";
    }
    
}
