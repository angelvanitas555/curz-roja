/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_area")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblArea.findAll", query = "SELECT t FROM TblArea t")
    , @NamedQuery(name = "TblArea.findByIdArea", query = "SELECT t FROM TblArea t WHERE t.idArea = :idArea")
    , @NamedQuery(name = "TblArea.findByNombreArea", query = "SELECT t FROM TblArea t WHERE t.nombreArea = :nombreArea")})
public class TblArea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_area")
    private Integer idArea;
    @Basic(optional = false)
    @Column(name = "nombre_area")
    private String nombreArea;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idArea")
    private Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idArea")
    private Collection<TblRegistro> tblRegistroCollection;

    public TblArea() {
    }

    public TblArea(Integer idArea) {
        this.idArea = idArea;
    }

    public TblArea(Integer idArea, String nombreArea) {
        this.idArea = idArea;
        this.nombreArea = nombreArea;
    }

    public Integer getIdArea() {
        return idArea;
    }

    public void setIdArea(Integer idArea) {
        this.idArea = idArea;
    }

    public String getNombreArea() {
        return nombreArea;
    }

    public void setNombreArea(String nombreArea) {
        this.nombreArea = nombreArea;
    }

    @XmlTransient
    public Collection<TblSolicitudIngreso> getTblSolicitudIngresoCollection() {
        return tblSolicitudIngresoCollection;
    }

    public void setTblSolicitudIngresoCollection(Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection) {
        this.tblSolicitudIngresoCollection = tblSolicitudIngresoCollection;
    }

    @XmlTransient
    public Collection<TblRegistro> getTblRegistroCollection() {
        return tblRegistroCollection;
    }

    public void setTblRegistroCollection(Collection<TblRegistro> tblRegistroCollection) {
        this.tblRegistroCollection = tblRegistroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArea != null ? idArea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblArea)) {
            return false;
        }
        TblArea other = (TblArea) object;
        if ((this.idArea == null && other.idArea != null) || (this.idArea != null && !this.idArea.equals(other.idArea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblArea[ idArea=" + idArea + " ]";
    }
    
}
