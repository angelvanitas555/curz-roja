/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_solicitud_ingreso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblSolicitudIngreso.findAll", query = "SELECT t FROM TblSolicitudIngreso t")
    , @NamedQuery(name = "TblSolicitudIngreso.findByIdSolicitud", query = "SELECT t FROM TblSolicitudIngreso t WHERE t.idSolicitud = :idSolicitud")})
public class TblSolicitudIngreso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_solicitud")
    private Integer idSolicitud;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSolicitud")
    private Collection<TblDatosMedicos> tblDatosMedicosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSolicitud")
    private Collection<TblFormularioEntrevista> tblFormularioEntrevistaCollection;
    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
    @ManyToOne(optional = false)
    private TblArea idArea;
    @JoinColumn(name = "id_seccion", referencedColumnName = "id_seccion")
    @ManyToOne(optional = false)
    private TblSeccion idSeccion;
    @JoinColumn(name = "id_dat_person", referencedColumnName = "id_dat_person")
    @ManyToOne(optional = false)
    private TblDatosPersonales idDatPerson;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private TblUsuario idUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSolicitud")
    private Collection<TblRegistro> tblRegistroCollection;

    public TblSolicitudIngreso() {
    }

    public TblSolicitudIngreso(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @XmlTransient
    public Collection<TblDatosMedicos> getTblDatosMedicosCollection() {
        return tblDatosMedicosCollection;
    }

    public void setTblDatosMedicosCollection(Collection<TblDatosMedicos> tblDatosMedicosCollection) {
        this.tblDatosMedicosCollection = tblDatosMedicosCollection;
    }

    @XmlTransient
    public Collection<TblFormularioEntrevista> getTblFormularioEntrevistaCollection() {
        return tblFormularioEntrevistaCollection;
    }

    public void setTblFormularioEntrevistaCollection(Collection<TblFormularioEntrevista> tblFormularioEntrevistaCollection) {
        this.tblFormularioEntrevistaCollection = tblFormularioEntrevistaCollection;
    }

    public TblArea getIdArea() {
        return idArea;
    }

    public void setIdArea(TblArea idArea) {
        this.idArea = idArea;
    }

    public TblSeccion getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(TblSeccion idSeccion) {
        this.idSeccion = idSeccion;
    }

    public TblDatosPersonales getIdDatPerson() {
        return idDatPerson;
    }

    public void setIdDatPerson(TblDatosPersonales idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    public TblUsuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(TblUsuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @XmlTransient
    public Collection<TblRegistro> getTblRegistroCollection() {
        return tblRegistroCollection;
    }

    public void setTblRegistroCollection(Collection<TblRegistro> tblRegistroCollection) {
        this.tblRegistroCollection = tblRegistroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSolicitud != null ? idSolicitud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblSolicitudIngreso)) {
            return false;
        }
        TblSolicitudIngreso other = (TblSolicitudIngreso) object;
        if ((this.idSolicitud == null && other.idSolicitud != null) || (this.idSolicitud != null && !this.idSolicitud.equals(other.idSolicitud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblSolicitudIngreso[ idSolicitud=" + idSolicitud + " ]";
    }
    
}
