/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_datos_empleo_estudio_actual")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblDatosEmpleoEstudioActual.findAll", query = "SELECT t FROM TblDatosEmpleoEstudioActual t")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByIdEmpleoEstudio", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.idEmpleoEstudio = :idEmpleoEstudio")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByEmpresa", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.empresa = :empresa")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByDireccion", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.direccion = :direccion")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByTipoTrabajo", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.tipoTrabajo = :tipoTrabajo")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByTelefono", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.telefono = :telefono")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByEstudiante", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.estudiante = :estudiante")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByTipoEstudio", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.tipoEstudio = :tipoEstudio")
    , @NamedQuery(name = "TblDatosEmpleoEstudioActual.findByInstitucion", query = "SELECT t FROM TblDatosEmpleoEstudioActual t WHERE t.institucion = :institucion")})
public class TblDatosEmpleoEstudioActual implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_empleo_estudio")
    private Integer idEmpleoEstudio;
    @Column(name = "empresa")
    private String empresa;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "tipo_trabajo")
    private String tipoTrabajo;
    @Column(name = "telefono")
    private Integer telefono;
    @Basic(optional = false)
    @Column(name = "estudiante")
    private boolean estudiante;
    @Column(name = "tipo_estudio")
    private String tipoEstudio;
    @Column(name = "institucion")
    private String institucion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpleoEstudio")
    private Collection<TblDireccion> tblDireccionCollection;
    @JoinColumn(name = "id_datos_personales", referencedColumnName = "id_dat_person")
    @ManyToOne(optional = false)
    private TblDatosPersonales idDatosPersonales;

    public TblDatosEmpleoEstudioActual() {
    }

    public TblDatosEmpleoEstudioActual(Integer idEmpleoEstudio) {
        this.idEmpleoEstudio = idEmpleoEstudio;
    }

    public TblDatosEmpleoEstudioActual(Integer idEmpleoEstudio, String direccion, boolean estudiante) {
        this.idEmpleoEstudio = idEmpleoEstudio;
        this.direccion = direccion;
        this.estudiante = estudiante;
    }

    public Integer getIdEmpleoEstudio() {
        return idEmpleoEstudio;
    }

    public void setIdEmpleoEstudio(Integer idEmpleoEstudio) {
        this.idEmpleoEstudio = idEmpleoEstudio;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipoTrabajo() {
        return tipoTrabajo;
    }

    public void setTipoTrabajo(String tipoTrabajo) {
        this.tipoTrabajo = tipoTrabajo;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public boolean getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(boolean estudiante) {
        this.estudiante = estudiante;
    }

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    @XmlTransient
    public Collection<TblDireccion> getTblDireccionCollection() {
        return tblDireccionCollection;
    }

    public void setTblDireccionCollection(Collection<TblDireccion> tblDireccionCollection) {
        this.tblDireccionCollection = tblDireccionCollection;
    }

    public TblDatosPersonales getIdDatosPersonales() {
        return idDatosPersonales;
    }

    public void setIdDatosPersonales(TblDatosPersonales idDatosPersonales) {
        this.idDatosPersonales = idDatosPersonales;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpleoEstudio != null ? idEmpleoEstudio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblDatosEmpleoEstudioActual)) {
            return false;
        }
        TblDatosEmpleoEstudioActual other = (TblDatosEmpleoEstudioActual) object;
        if ((this.idEmpleoEstudio == null && other.idEmpleoEstudio != null) || (this.idEmpleoEstudio != null && !this.idEmpleoEstudio.equals(other.idEmpleoEstudio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TblDatosEmpleoEstudioActual[ idEmpleoEstudio=" + idEmpleoEstudio + " ]";
    }
    
}
