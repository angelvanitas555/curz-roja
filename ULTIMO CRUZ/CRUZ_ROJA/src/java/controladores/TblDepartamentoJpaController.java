/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TblDepartamento;
import persistencia.TblMunicipio;

/**
 *
 * @author Jose.azucenaUSAM
 */
public class TblDepartamentoJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblDepartamentoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZ_ROJAPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblDepartamento tblDepartamento) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblMunicipio idMunicipio = tblDepartamento.getIdMunicipio();
            if (idMunicipio != null) {
                idMunicipio = em.getReference(idMunicipio.getClass(), idMunicipio.getIdMunicipio());
                tblDepartamento.setIdMunicipio(idMunicipio);
            }
            em.persist(tblDepartamento);
            if (idMunicipio != null) {
                idMunicipio.getTblDepartamentoCollection().add(tblDepartamento);
                idMunicipio = em.merge(idMunicipio);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblDepartamento tblDepartamento) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDepartamento persistentTblDepartamento = em.find(TblDepartamento.class, tblDepartamento.getIdDepto());
            TblMunicipio idMunicipioOld = persistentTblDepartamento.getIdMunicipio();
            TblMunicipio idMunicipioNew = tblDepartamento.getIdMunicipio();
            if (idMunicipioNew != null) {
                idMunicipioNew = em.getReference(idMunicipioNew.getClass(), idMunicipioNew.getIdMunicipio());
                tblDepartamento.setIdMunicipio(idMunicipioNew);
            }
            tblDepartamento = em.merge(tblDepartamento);
            if (idMunicipioOld != null && !idMunicipioOld.equals(idMunicipioNew)) {
                idMunicipioOld.getTblDepartamentoCollection().remove(tblDepartamento);
                idMunicipioOld = em.merge(idMunicipioOld);
            }
            if (idMunicipioNew != null && !idMunicipioNew.equals(idMunicipioOld)) {
                idMunicipioNew.getTblDepartamentoCollection().add(tblDepartamento);
                idMunicipioNew = em.merge(idMunicipioNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblDepartamento.getIdDepto();
                if (findTblDepartamento(id) == null) {
                    throw new NonexistentEntityException("The tblDepartamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDepartamento tblDepartamento;
            try {
                tblDepartamento = em.getReference(TblDepartamento.class, id);
                tblDepartamento.getIdDepto();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblDepartamento with id " + id + " no longer exists.", enfe);
            }
            TblMunicipio idMunicipio = tblDepartamento.getIdMunicipio();
            if (idMunicipio != null) {
                idMunicipio.getTblDepartamentoCollection().remove(tblDepartamento);
                idMunicipio = em.merge(idMunicipio);
            }
            em.remove(tblDepartamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblDepartamento> findTblDepartamentoEntities() {
        return findTblDepartamentoEntities(true, -1, -1);
    }

    public List<TblDepartamento> findTblDepartamentoEntities(int maxResults, int firstResult) {
        return findTblDepartamentoEntities(false, maxResults, firstResult);
    }

    private List<TblDepartamento> findTblDepartamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblDepartamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblDepartamento findTblDepartamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblDepartamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblDepartamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblDepartamento> rt = cq.from(TblDepartamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
