/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.exceptions.IllegalOrphanException;
import controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TblRegistroCapacidad;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.TblCapacitaciones;

/**
 *
 * @author Jose.azucenaUSAM
 */
public class TblCapacitacionesJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblCapacitacionesJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZ_ROJAPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblCapacitaciones tblCapacitaciones) {
        if (tblCapacitaciones.getTblRegistroCapacidadCollection() == null) {
            tblCapacitaciones.setTblRegistroCapacidadCollection(new ArrayList<TblRegistroCapacidad>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<TblRegistroCapacidad> attachedTblRegistroCapacidadCollection = new ArrayList<TblRegistroCapacidad>();
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach : tblCapacitaciones.getTblRegistroCapacidadCollection()) {
                tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach = em.getReference(tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach.getClass(), tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach.getIdRegistroCapacidad());
                attachedTblRegistroCapacidadCollection.add(tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach);
            }
            tblCapacitaciones.setTblRegistroCapacidadCollection(attachedTblRegistroCapacidadCollection);
            em.persist(tblCapacitaciones);
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionTblRegistroCapacidad : tblCapacitaciones.getTblRegistroCapacidadCollection()) {
                TblCapacitaciones oldIdCapacitacionOfTblRegistroCapacidadCollectionTblRegistroCapacidad = tblRegistroCapacidadCollectionTblRegistroCapacidad.getIdCapacitacion();
                tblRegistroCapacidadCollectionTblRegistroCapacidad.setIdCapacitacion(tblCapacitaciones);
                tblRegistroCapacidadCollectionTblRegistroCapacidad = em.merge(tblRegistroCapacidadCollectionTblRegistroCapacidad);
                if (oldIdCapacitacionOfTblRegistroCapacidadCollectionTblRegistroCapacidad != null) {
                    oldIdCapacitacionOfTblRegistroCapacidadCollectionTblRegistroCapacidad.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidadCollectionTblRegistroCapacidad);
                    oldIdCapacitacionOfTblRegistroCapacidadCollectionTblRegistroCapacidad = em.merge(oldIdCapacitacionOfTblRegistroCapacidadCollectionTblRegistroCapacidad);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblCapacitaciones tblCapacitaciones) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblCapacitaciones persistentTblCapacitaciones = em.find(TblCapacitaciones.class, tblCapacitaciones.getIdCapacitaciones());
            Collection<TblRegistroCapacidad> tblRegistroCapacidadCollectionOld = persistentTblCapacitaciones.getTblRegistroCapacidadCollection();
            Collection<TblRegistroCapacidad> tblRegistroCapacidadCollectionNew = tblCapacitaciones.getTblRegistroCapacidadCollection();
            List<String> illegalOrphanMessages = null;
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionOldTblRegistroCapacidad : tblRegistroCapacidadCollectionOld) {
                if (!tblRegistroCapacidadCollectionNew.contains(tblRegistroCapacidadCollectionOldTblRegistroCapacidad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblRegistroCapacidad " + tblRegistroCapacidadCollectionOldTblRegistroCapacidad + " since its idCapacitacion field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<TblRegistroCapacidad> attachedTblRegistroCapacidadCollectionNew = new ArrayList<TblRegistroCapacidad>();
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach : tblRegistroCapacidadCollectionNew) {
                tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach = em.getReference(tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach.getClass(), tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach.getIdRegistroCapacidad());
                attachedTblRegistroCapacidadCollectionNew.add(tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach);
            }
            tblRegistroCapacidadCollectionNew = attachedTblRegistroCapacidadCollectionNew;
            tblCapacitaciones.setTblRegistroCapacidadCollection(tblRegistroCapacidadCollectionNew);
            tblCapacitaciones = em.merge(tblCapacitaciones);
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionNewTblRegistroCapacidad : tblRegistroCapacidadCollectionNew) {
                if (!tblRegistroCapacidadCollectionOld.contains(tblRegistroCapacidadCollectionNewTblRegistroCapacidad)) {
                    TblCapacitaciones oldIdCapacitacionOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad = tblRegistroCapacidadCollectionNewTblRegistroCapacidad.getIdCapacitacion();
                    tblRegistroCapacidadCollectionNewTblRegistroCapacidad.setIdCapacitacion(tblCapacitaciones);
                    tblRegistroCapacidadCollectionNewTblRegistroCapacidad = em.merge(tblRegistroCapacidadCollectionNewTblRegistroCapacidad);
                    if (oldIdCapacitacionOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad != null && !oldIdCapacitacionOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad.equals(tblCapacitaciones)) {
                        oldIdCapacitacionOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidadCollectionNewTblRegistroCapacidad);
                        oldIdCapacitacionOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad = em.merge(oldIdCapacitacionOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblCapacitaciones.getIdCapacitaciones();
                if (findTblCapacitaciones(id) == null) {
                    throw new NonexistentEntityException("The tblCapacitaciones with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblCapacitaciones tblCapacitaciones;
            try {
                tblCapacitaciones = em.getReference(TblCapacitaciones.class, id);
                tblCapacitaciones.getIdCapacitaciones();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblCapacitaciones with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblRegistroCapacidad> tblRegistroCapacidadCollectionOrphanCheck = tblCapacitaciones.getTblRegistroCapacidadCollection();
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionOrphanCheckTblRegistroCapacidad : tblRegistroCapacidadCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblCapacitaciones (" + tblCapacitaciones + ") cannot be destroyed since the TblRegistroCapacidad " + tblRegistroCapacidadCollectionOrphanCheckTblRegistroCapacidad + " in its tblRegistroCapacidadCollection field has a non-nullable idCapacitacion field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tblCapacitaciones);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblCapacitaciones> findTblCapacitacionesEntities() {
        return findTblCapacitacionesEntities(true, -1, -1);
    }

    public List<TblCapacitaciones> findTblCapacitacionesEntities(int maxResults, int firstResult) {
        return findTblCapacitacionesEntities(false, maxResults, firstResult);
    }

    private List<TblCapacitaciones> findTblCapacitacionesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblCapacitaciones.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblCapacitaciones findTblCapacitaciones(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblCapacitaciones.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblCapacitacionesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblCapacitaciones> rt = cq.from(TblCapacitaciones.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
