/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.exceptions.IllegalOrphanException;
import controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TblDatosPersonales;
import persistencia.TblDireccion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.TblDatosEmpleoEstudioActual;

/**
 *
 * @author Jose.azucenaUSAM
 */
public class TblDatosEmpleoEstudioActualJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblDatosEmpleoEstudioActualJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZ_ROJAPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActual) {
        if (tblDatosEmpleoEstudioActual.getTblDireccionCollection() == null) {
            tblDatosEmpleoEstudioActual.setTblDireccionCollection(new ArrayList<TblDireccion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosPersonales idDatosPersonales = tblDatosEmpleoEstudioActual.getIdDatosPersonales();
            if (idDatosPersonales != null) {
                idDatosPersonales = em.getReference(idDatosPersonales.getClass(), idDatosPersonales.getIdDatPerson());
                tblDatosEmpleoEstudioActual.setIdDatosPersonales(idDatosPersonales);
            }
            Collection<TblDireccion> attachedTblDireccionCollection = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionTblDireccionToAttach : tblDatosEmpleoEstudioActual.getTblDireccionCollection()) {
                tblDireccionCollectionTblDireccionToAttach = em.getReference(tblDireccionCollectionTblDireccionToAttach.getClass(), tblDireccionCollectionTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollection.add(tblDireccionCollectionTblDireccionToAttach);
            }
            tblDatosEmpleoEstudioActual.setTblDireccionCollection(attachedTblDireccionCollection);
            em.persist(tblDatosEmpleoEstudioActual);
            if (idDatosPersonales != null) {
                idDatosPersonales.getTblDatosEmpleoEstudioActualCollection().add(tblDatosEmpleoEstudioActual);
                idDatosPersonales = em.merge(idDatosPersonales);
            }
            for (TblDireccion tblDireccionCollectionTblDireccion : tblDatosEmpleoEstudioActual.getTblDireccionCollection()) {
                TblDatosEmpleoEstudioActual oldIdEmpleoEstudioOfTblDireccionCollectionTblDireccion = tblDireccionCollectionTblDireccion.getIdEmpleoEstudio();
                tblDireccionCollectionTblDireccion.setIdEmpleoEstudio(tblDatosEmpleoEstudioActual);
                tblDireccionCollectionTblDireccion = em.merge(tblDireccionCollectionTblDireccion);
                if (oldIdEmpleoEstudioOfTblDireccionCollectionTblDireccion != null) {
                    oldIdEmpleoEstudioOfTblDireccionCollectionTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionTblDireccion);
                    oldIdEmpleoEstudioOfTblDireccionCollectionTblDireccion = em.merge(oldIdEmpleoEstudioOfTblDireccionCollectionTblDireccion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActual) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosEmpleoEstudioActual persistentTblDatosEmpleoEstudioActual = em.find(TblDatosEmpleoEstudioActual.class, tblDatosEmpleoEstudioActual.getIdEmpleoEstudio());
            TblDatosPersonales idDatosPersonalesOld = persistentTblDatosEmpleoEstudioActual.getIdDatosPersonales();
            TblDatosPersonales idDatosPersonalesNew = tblDatosEmpleoEstudioActual.getIdDatosPersonales();
            Collection<TblDireccion> tblDireccionCollectionOld = persistentTblDatosEmpleoEstudioActual.getTblDireccionCollection();
            Collection<TblDireccion> tblDireccionCollectionNew = tblDatosEmpleoEstudioActual.getTblDireccionCollection();
            List<String> illegalOrphanMessages = null;
            for (TblDireccion tblDireccionCollectionOldTblDireccion : tblDireccionCollectionOld) {
                if (!tblDireccionCollectionNew.contains(tblDireccionCollectionOldTblDireccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDireccion " + tblDireccionCollectionOldTblDireccion + " since its idEmpleoEstudio field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idDatosPersonalesNew != null) {
                idDatosPersonalesNew = em.getReference(idDatosPersonalesNew.getClass(), idDatosPersonalesNew.getIdDatPerson());
                tblDatosEmpleoEstudioActual.setIdDatosPersonales(idDatosPersonalesNew);
            }
            Collection<TblDireccion> attachedTblDireccionCollectionNew = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionNewTblDireccionToAttach : tblDireccionCollectionNew) {
                tblDireccionCollectionNewTblDireccionToAttach = em.getReference(tblDireccionCollectionNewTblDireccionToAttach.getClass(), tblDireccionCollectionNewTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollectionNew.add(tblDireccionCollectionNewTblDireccionToAttach);
            }
            tblDireccionCollectionNew = attachedTblDireccionCollectionNew;
            tblDatosEmpleoEstudioActual.setTblDireccionCollection(tblDireccionCollectionNew);
            tblDatosEmpleoEstudioActual = em.merge(tblDatosEmpleoEstudioActual);
            if (idDatosPersonalesOld != null && !idDatosPersonalesOld.equals(idDatosPersonalesNew)) {
                idDatosPersonalesOld.getTblDatosEmpleoEstudioActualCollection().remove(tblDatosEmpleoEstudioActual);
                idDatosPersonalesOld = em.merge(idDatosPersonalesOld);
            }
            if (idDatosPersonalesNew != null && !idDatosPersonalesNew.equals(idDatosPersonalesOld)) {
                idDatosPersonalesNew.getTblDatosEmpleoEstudioActualCollection().add(tblDatosEmpleoEstudioActual);
                idDatosPersonalesNew = em.merge(idDatosPersonalesNew);
            }
            for (TblDireccion tblDireccionCollectionNewTblDireccion : tblDireccionCollectionNew) {
                if (!tblDireccionCollectionOld.contains(tblDireccionCollectionNewTblDireccion)) {
                    TblDatosEmpleoEstudioActual oldIdEmpleoEstudioOfTblDireccionCollectionNewTblDireccion = tblDireccionCollectionNewTblDireccion.getIdEmpleoEstudio();
                    tblDireccionCollectionNewTblDireccion.setIdEmpleoEstudio(tblDatosEmpleoEstudioActual);
                    tblDireccionCollectionNewTblDireccion = em.merge(tblDireccionCollectionNewTblDireccion);
                    if (oldIdEmpleoEstudioOfTblDireccionCollectionNewTblDireccion != null && !oldIdEmpleoEstudioOfTblDireccionCollectionNewTblDireccion.equals(tblDatosEmpleoEstudioActual)) {
                        oldIdEmpleoEstudioOfTblDireccionCollectionNewTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionNewTblDireccion);
                        oldIdEmpleoEstudioOfTblDireccionCollectionNewTblDireccion = em.merge(oldIdEmpleoEstudioOfTblDireccionCollectionNewTblDireccion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblDatosEmpleoEstudioActual.getIdEmpleoEstudio();
                if (findTblDatosEmpleoEstudioActual(id) == null) {
                    throw new NonexistentEntityException("The tblDatosEmpleoEstudioActual with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActual;
            try {
                tblDatosEmpleoEstudioActual = em.getReference(TblDatosEmpleoEstudioActual.class, id);
                tblDatosEmpleoEstudioActual.getIdEmpleoEstudio();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblDatosEmpleoEstudioActual with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblDireccion> tblDireccionCollectionOrphanCheck = tblDatosEmpleoEstudioActual.getTblDireccionCollection();
            for (TblDireccion tblDireccionCollectionOrphanCheckTblDireccion : tblDireccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosEmpleoEstudioActual (" + tblDatosEmpleoEstudioActual + ") cannot be destroyed since the TblDireccion " + tblDireccionCollectionOrphanCheckTblDireccion + " in its tblDireccionCollection field has a non-nullable idEmpleoEstudio field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TblDatosPersonales idDatosPersonales = tblDatosEmpleoEstudioActual.getIdDatosPersonales();
            if (idDatosPersonales != null) {
                idDatosPersonales.getTblDatosEmpleoEstudioActualCollection().remove(tblDatosEmpleoEstudioActual);
                idDatosPersonales = em.merge(idDatosPersonales);
            }
            em.remove(tblDatosEmpleoEstudioActual);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblDatosEmpleoEstudioActual> findTblDatosEmpleoEstudioActualEntities() {
        return findTblDatosEmpleoEstudioActualEntities(true, -1, -1);
    }

    public List<TblDatosEmpleoEstudioActual> findTblDatosEmpleoEstudioActualEntities(int maxResults, int firstResult) {
        return findTblDatosEmpleoEstudioActualEntities(false, maxResults, firstResult);
    }

    private List<TblDatosEmpleoEstudioActual> findTblDatosEmpleoEstudioActualEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblDatosEmpleoEstudioActual.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblDatosEmpleoEstudioActual findTblDatosEmpleoEstudioActual(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblDatosEmpleoEstudioActual.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblDatosEmpleoEstudioActualCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblDatosEmpleoEstudioActual> rt = cq.from(TblDatosEmpleoEstudioActual.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
