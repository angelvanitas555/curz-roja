/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.exceptions.IllegalOrphanException;
import controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TblDatosAcademicos;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.TblPadres;
import persistencia.TblReferencias;
import persistencia.TblSolicitudIngreso;
import persistencia.TblDireccion;
import persistencia.TblDatosEmpleoEstudioActual;
import persistencia.TblDatosPersonales;
import persistencia.TblTelefonos;

/**
 *
 * @author Jose.azucenaUSAM
 */
public class TblDatosPersonalesJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblDatosPersonalesJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZ_ROJAPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblDatosPersonales tblDatosPersonales) {
        if (tblDatosPersonales.getTblDatosAcademicosCollection() == null) {
            tblDatosPersonales.setTblDatosAcademicosCollection(new ArrayList<TblDatosAcademicos>());
        }
        if (tblDatosPersonales.getTblPadresCollection() == null) {
            tblDatosPersonales.setTblPadresCollection(new ArrayList<TblPadres>());
        }
        if (tblDatosPersonales.getTblReferenciasCollection() == null) {
            tblDatosPersonales.setTblReferenciasCollection(new ArrayList<TblReferencias>());
        }
        if (tblDatosPersonales.getTblSolicitudIngresoCollection() == null) {
            tblDatosPersonales.setTblSolicitudIngresoCollection(new ArrayList<TblSolicitudIngreso>());
        }
        if (tblDatosPersonales.getTblDireccionCollection() == null) {
            tblDatosPersonales.setTblDireccionCollection(new ArrayList<TblDireccion>());
        }
        if (tblDatosPersonales.getTblDatosEmpleoEstudioActualCollection() == null) {
            tblDatosPersonales.setTblDatosEmpleoEstudioActualCollection(new ArrayList<TblDatosEmpleoEstudioActual>());
        }
        if (tblDatosPersonales.getTblTelefonosCollection() == null) {
            tblDatosPersonales.setTblTelefonosCollection(new ArrayList<TblTelefonos>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<TblDatosAcademicos> attachedTblDatosAcademicosCollection = new ArrayList<TblDatosAcademicos>();
            for (TblDatosAcademicos tblDatosAcademicosCollectionTblDatosAcademicosToAttach : tblDatosPersonales.getTblDatosAcademicosCollection()) {
                tblDatosAcademicosCollectionTblDatosAcademicosToAttach = em.getReference(tblDatosAcademicosCollectionTblDatosAcademicosToAttach.getClass(), tblDatosAcademicosCollectionTblDatosAcademicosToAttach.getIdDatosAcademicos());
                attachedTblDatosAcademicosCollection.add(tblDatosAcademicosCollectionTblDatosAcademicosToAttach);
            }
            tblDatosPersonales.setTblDatosAcademicosCollection(attachedTblDatosAcademicosCollection);
            Collection<TblPadres> attachedTblPadresCollection = new ArrayList<TblPadres>();
            for (TblPadres tblPadresCollectionTblPadresToAttach : tblDatosPersonales.getTblPadresCollection()) {
                tblPadresCollectionTblPadresToAttach = em.getReference(tblPadresCollectionTblPadresToAttach.getClass(), tblPadresCollectionTblPadresToAttach.getIdPadres());
                attachedTblPadresCollection.add(tblPadresCollectionTblPadresToAttach);
            }
            tblDatosPersonales.setTblPadresCollection(attachedTblPadresCollection);
            Collection<TblReferencias> attachedTblReferenciasCollection = new ArrayList<TblReferencias>();
            for (TblReferencias tblReferenciasCollectionTblReferenciasToAttach : tblDatosPersonales.getTblReferenciasCollection()) {
                tblReferenciasCollectionTblReferenciasToAttach = em.getReference(tblReferenciasCollectionTblReferenciasToAttach.getClass(), tblReferenciasCollectionTblReferenciasToAttach.getIdReferencia());
                attachedTblReferenciasCollection.add(tblReferenciasCollectionTblReferenciasToAttach);
            }
            tblDatosPersonales.setTblReferenciasCollection(attachedTblReferenciasCollection);
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollection = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach : tblDatosPersonales.getTblSolicitudIngresoCollection()) {
                tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollection.add(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach);
            }
            tblDatosPersonales.setTblSolicitudIngresoCollection(attachedTblSolicitudIngresoCollection);
            Collection<TblDireccion> attachedTblDireccionCollection = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionTblDireccionToAttach : tblDatosPersonales.getTblDireccionCollection()) {
                tblDireccionCollectionTblDireccionToAttach = em.getReference(tblDireccionCollectionTblDireccionToAttach.getClass(), tblDireccionCollectionTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollection.add(tblDireccionCollectionTblDireccionToAttach);
            }
            tblDatosPersonales.setTblDireccionCollection(attachedTblDireccionCollection);
            Collection<TblDatosEmpleoEstudioActual> attachedTblDatosEmpleoEstudioActualCollection = new ArrayList<TblDatosEmpleoEstudioActual>();
            for (TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActualToAttach : tblDatosPersonales.getTblDatosEmpleoEstudioActualCollection()) {
                tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActualToAttach = em.getReference(tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActualToAttach.getClass(), tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActualToAttach.getIdEmpleoEstudio());
                attachedTblDatosEmpleoEstudioActualCollection.add(tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActualToAttach);
            }
            tblDatosPersonales.setTblDatosEmpleoEstudioActualCollection(attachedTblDatosEmpleoEstudioActualCollection);
            Collection<TblTelefonos> attachedTblTelefonosCollection = new ArrayList<TblTelefonos>();
            for (TblTelefonos tblTelefonosCollectionTblTelefonosToAttach : tblDatosPersonales.getTblTelefonosCollection()) {
                tblTelefonosCollectionTblTelefonosToAttach = em.getReference(tblTelefonosCollectionTblTelefonosToAttach.getClass(), tblTelefonosCollectionTblTelefonosToAttach.getIdTelefono());
                attachedTblTelefonosCollection.add(tblTelefonosCollectionTblTelefonosToAttach);
            }
            tblDatosPersonales.setTblTelefonosCollection(attachedTblTelefonosCollection);
            em.persist(tblDatosPersonales);
            for (TblDatosAcademicos tblDatosAcademicosCollectionTblDatosAcademicos : tblDatosPersonales.getTblDatosAcademicosCollection()) {
                TblDatosPersonales oldIdDatPersonOfTblDatosAcademicosCollectionTblDatosAcademicos = tblDatosAcademicosCollectionTblDatosAcademicos.getIdDatPerson();
                tblDatosAcademicosCollectionTblDatosAcademicos.setIdDatPerson(tblDatosPersonales);
                tblDatosAcademicosCollectionTblDatosAcademicos = em.merge(tblDatosAcademicosCollectionTblDatosAcademicos);
                if (oldIdDatPersonOfTblDatosAcademicosCollectionTblDatosAcademicos != null) {
                    oldIdDatPersonOfTblDatosAcademicosCollectionTblDatosAcademicos.getTblDatosAcademicosCollection().remove(tblDatosAcademicosCollectionTblDatosAcademicos);
                    oldIdDatPersonOfTblDatosAcademicosCollectionTblDatosAcademicos = em.merge(oldIdDatPersonOfTblDatosAcademicosCollectionTblDatosAcademicos);
                }
            }
            for (TblPadres tblPadresCollectionTblPadres : tblDatosPersonales.getTblPadresCollection()) {
                TblDatosPersonales oldIdDatPersonOfTblPadresCollectionTblPadres = tblPadresCollectionTblPadres.getIdDatPerson();
                tblPadresCollectionTblPadres.setIdDatPerson(tblDatosPersonales);
                tblPadresCollectionTblPadres = em.merge(tblPadresCollectionTblPadres);
                if (oldIdDatPersonOfTblPadresCollectionTblPadres != null) {
                    oldIdDatPersonOfTblPadresCollectionTblPadres.getTblPadresCollection().remove(tblPadresCollectionTblPadres);
                    oldIdDatPersonOfTblPadresCollectionTblPadres = em.merge(oldIdDatPersonOfTblPadresCollectionTblPadres);
                }
            }
            for (TblReferencias tblReferenciasCollectionTblReferencias : tblDatosPersonales.getTblReferenciasCollection()) {
                TblDatosPersonales oldIdDatPersonOfTblReferenciasCollectionTblReferencias = tblReferenciasCollectionTblReferencias.getIdDatPerson();
                tblReferenciasCollectionTblReferencias.setIdDatPerson(tblDatosPersonales);
                tblReferenciasCollectionTblReferencias = em.merge(tblReferenciasCollectionTblReferencias);
                if (oldIdDatPersonOfTblReferenciasCollectionTblReferencias != null) {
                    oldIdDatPersonOfTblReferenciasCollectionTblReferencias.getTblReferenciasCollection().remove(tblReferenciasCollectionTblReferencias);
                    oldIdDatPersonOfTblReferenciasCollectionTblReferencias = em.merge(oldIdDatPersonOfTblReferenciasCollectionTblReferencias);
                }
            }
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngreso : tblDatosPersonales.getTblSolicitudIngresoCollection()) {
                TblDatosPersonales oldIdDatPersonOfTblSolicitudIngresoCollectionTblSolicitudIngreso = tblSolicitudIngresoCollectionTblSolicitudIngreso.getIdDatPerson();
                tblSolicitudIngresoCollectionTblSolicitudIngreso.setIdDatPerson(tblDatosPersonales);
                tblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                if (oldIdDatPersonOfTblSolicitudIngresoCollectionTblSolicitudIngreso != null) {
                    oldIdDatPersonOfTblSolicitudIngresoCollectionTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                    oldIdDatPersonOfTblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(oldIdDatPersonOfTblSolicitudIngresoCollectionTblSolicitudIngreso);
                }
            }
            for (TblDireccion tblDireccionCollectionTblDireccion : tblDatosPersonales.getTblDireccionCollection()) {
                TblDatosPersonales oldIdDatPersonOfTblDireccionCollectionTblDireccion = tblDireccionCollectionTblDireccion.getIdDatPerson();
                tblDireccionCollectionTblDireccion.setIdDatPerson(tblDatosPersonales);
                tblDireccionCollectionTblDireccion = em.merge(tblDireccionCollectionTblDireccion);
                if (oldIdDatPersonOfTblDireccionCollectionTblDireccion != null) {
                    oldIdDatPersonOfTblDireccionCollectionTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionTblDireccion);
                    oldIdDatPersonOfTblDireccionCollectionTblDireccion = em.merge(oldIdDatPersonOfTblDireccionCollectionTblDireccion);
                }
            }
            for (TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual : tblDatosPersonales.getTblDatosEmpleoEstudioActualCollection()) {
                TblDatosPersonales oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual = tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual.getIdDatosPersonales();
                tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual.setIdDatosPersonales(tblDatosPersonales);
                tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual = em.merge(tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual);
                if (oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual != null) {
                    oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual.getTblDatosEmpleoEstudioActualCollection().remove(tblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual);
                    oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual = em.merge(oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionTblDatosEmpleoEstudioActual);
                }
            }
            for (TblTelefonos tblTelefonosCollectionTblTelefonos : tblDatosPersonales.getTblTelefonosCollection()) {
                TblDatosPersonales oldIdDatPersonOfTblTelefonosCollectionTblTelefonos = tblTelefonosCollectionTblTelefonos.getIdDatPerson();
                tblTelefonosCollectionTblTelefonos.setIdDatPerson(tblDatosPersonales);
                tblTelefonosCollectionTblTelefonos = em.merge(tblTelefonosCollectionTblTelefonos);
                if (oldIdDatPersonOfTblTelefonosCollectionTblTelefonos != null) {
                    oldIdDatPersonOfTblTelefonosCollectionTblTelefonos.getTblTelefonosCollection().remove(tblTelefonosCollectionTblTelefonos);
                    oldIdDatPersonOfTblTelefonosCollectionTblTelefonos = em.merge(oldIdDatPersonOfTblTelefonosCollectionTblTelefonos);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblDatosPersonales tblDatosPersonales) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosPersonales persistentTblDatosPersonales = em.find(TblDatosPersonales.class, tblDatosPersonales.getIdDatPerson());
            Collection<TblDatosAcademicos> tblDatosAcademicosCollectionOld = persistentTblDatosPersonales.getTblDatosAcademicosCollection();
            Collection<TblDatosAcademicos> tblDatosAcademicosCollectionNew = tblDatosPersonales.getTblDatosAcademicosCollection();
            Collection<TblPadres> tblPadresCollectionOld = persistentTblDatosPersonales.getTblPadresCollection();
            Collection<TblPadres> tblPadresCollectionNew = tblDatosPersonales.getTblPadresCollection();
            Collection<TblReferencias> tblReferenciasCollectionOld = persistentTblDatosPersonales.getTblReferenciasCollection();
            Collection<TblReferencias> tblReferenciasCollectionNew = tblDatosPersonales.getTblReferenciasCollection();
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOld = persistentTblDatosPersonales.getTblSolicitudIngresoCollection();
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionNew = tblDatosPersonales.getTblSolicitudIngresoCollection();
            Collection<TblDireccion> tblDireccionCollectionOld = persistentTblDatosPersonales.getTblDireccionCollection();
            Collection<TblDireccion> tblDireccionCollectionNew = tblDatosPersonales.getTblDireccionCollection();
            Collection<TblDatosEmpleoEstudioActual> tblDatosEmpleoEstudioActualCollectionOld = persistentTblDatosPersonales.getTblDatosEmpleoEstudioActualCollection();
            Collection<TblDatosEmpleoEstudioActual> tblDatosEmpleoEstudioActualCollectionNew = tblDatosPersonales.getTblDatosEmpleoEstudioActualCollection();
            Collection<TblTelefonos> tblTelefonosCollectionOld = persistentTblDatosPersonales.getTblTelefonosCollection();
            Collection<TblTelefonos> tblTelefonosCollectionNew = tblDatosPersonales.getTblTelefonosCollection();
            List<String> illegalOrphanMessages = null;
            for (TblDatosAcademicos tblDatosAcademicosCollectionOldTblDatosAcademicos : tblDatosAcademicosCollectionOld) {
                if (!tblDatosAcademicosCollectionNew.contains(tblDatosAcademicosCollectionOldTblDatosAcademicos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDatosAcademicos " + tblDatosAcademicosCollectionOldTblDatosAcademicos + " since its idDatPerson field is not nullable.");
                }
            }
            for (TblPadres tblPadresCollectionOldTblPadres : tblPadresCollectionOld) {
                if (!tblPadresCollectionNew.contains(tblPadresCollectionOldTblPadres)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblPadres " + tblPadresCollectionOldTblPadres + " since its idDatPerson field is not nullable.");
                }
            }
            for (TblReferencias tblReferenciasCollectionOldTblReferencias : tblReferenciasCollectionOld) {
                if (!tblReferenciasCollectionNew.contains(tblReferenciasCollectionOldTblReferencias)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblReferencias " + tblReferenciasCollectionOldTblReferencias + " since its idDatPerson field is not nullable.");
                }
            }
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOldTblSolicitudIngreso : tblSolicitudIngresoCollectionOld) {
                if (!tblSolicitudIngresoCollectionNew.contains(tblSolicitudIngresoCollectionOldTblSolicitudIngreso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblSolicitudIngreso " + tblSolicitudIngresoCollectionOldTblSolicitudIngreso + " since its idDatPerson field is not nullable.");
                }
            }
            for (TblDireccion tblDireccionCollectionOldTblDireccion : tblDireccionCollectionOld) {
                if (!tblDireccionCollectionNew.contains(tblDireccionCollectionOldTblDireccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDireccion " + tblDireccionCollectionOldTblDireccion + " since its idDatPerson field is not nullable.");
                }
            }
            for (TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActualCollectionOldTblDatosEmpleoEstudioActual : tblDatosEmpleoEstudioActualCollectionOld) {
                if (!tblDatosEmpleoEstudioActualCollectionNew.contains(tblDatosEmpleoEstudioActualCollectionOldTblDatosEmpleoEstudioActual)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDatosEmpleoEstudioActual " + tblDatosEmpleoEstudioActualCollectionOldTblDatosEmpleoEstudioActual + " since its idDatosPersonales field is not nullable.");
                }
            }
            for (TblTelefonos tblTelefonosCollectionOldTblTelefonos : tblTelefonosCollectionOld) {
                if (!tblTelefonosCollectionNew.contains(tblTelefonosCollectionOldTblTelefonos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblTelefonos " + tblTelefonosCollectionOldTblTelefonos + " since its idDatPerson field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<TblDatosAcademicos> attachedTblDatosAcademicosCollectionNew = new ArrayList<TblDatosAcademicos>();
            for (TblDatosAcademicos tblDatosAcademicosCollectionNewTblDatosAcademicosToAttach : tblDatosAcademicosCollectionNew) {
                tblDatosAcademicosCollectionNewTblDatosAcademicosToAttach = em.getReference(tblDatosAcademicosCollectionNewTblDatosAcademicosToAttach.getClass(), tblDatosAcademicosCollectionNewTblDatosAcademicosToAttach.getIdDatosAcademicos());
                attachedTblDatosAcademicosCollectionNew.add(tblDatosAcademicosCollectionNewTblDatosAcademicosToAttach);
            }
            tblDatosAcademicosCollectionNew = attachedTblDatosAcademicosCollectionNew;
            tblDatosPersonales.setTblDatosAcademicosCollection(tblDatosAcademicosCollectionNew);
            Collection<TblPadres> attachedTblPadresCollectionNew = new ArrayList<TblPadres>();
            for (TblPadres tblPadresCollectionNewTblPadresToAttach : tblPadresCollectionNew) {
                tblPadresCollectionNewTblPadresToAttach = em.getReference(tblPadresCollectionNewTblPadresToAttach.getClass(), tblPadresCollectionNewTblPadresToAttach.getIdPadres());
                attachedTblPadresCollectionNew.add(tblPadresCollectionNewTblPadresToAttach);
            }
            tblPadresCollectionNew = attachedTblPadresCollectionNew;
            tblDatosPersonales.setTblPadresCollection(tblPadresCollectionNew);
            Collection<TblReferencias> attachedTblReferenciasCollectionNew = new ArrayList<TblReferencias>();
            for (TblReferencias tblReferenciasCollectionNewTblReferenciasToAttach : tblReferenciasCollectionNew) {
                tblReferenciasCollectionNewTblReferenciasToAttach = em.getReference(tblReferenciasCollectionNewTblReferenciasToAttach.getClass(), tblReferenciasCollectionNewTblReferenciasToAttach.getIdReferencia());
                attachedTblReferenciasCollectionNew.add(tblReferenciasCollectionNewTblReferenciasToAttach);
            }
            tblReferenciasCollectionNew = attachedTblReferenciasCollectionNew;
            tblDatosPersonales.setTblReferenciasCollection(tblReferenciasCollectionNew);
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollectionNew = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach : tblSolicitudIngresoCollectionNew) {
                tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollectionNew.add(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach);
            }
            tblSolicitudIngresoCollectionNew = attachedTblSolicitudIngresoCollectionNew;
            tblDatosPersonales.setTblSolicitudIngresoCollection(tblSolicitudIngresoCollectionNew);
            Collection<TblDireccion> attachedTblDireccionCollectionNew = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionNewTblDireccionToAttach : tblDireccionCollectionNew) {
                tblDireccionCollectionNewTblDireccionToAttach = em.getReference(tblDireccionCollectionNewTblDireccionToAttach.getClass(), tblDireccionCollectionNewTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollectionNew.add(tblDireccionCollectionNewTblDireccionToAttach);
            }
            tblDireccionCollectionNew = attachedTblDireccionCollectionNew;
            tblDatosPersonales.setTblDireccionCollection(tblDireccionCollectionNew);
            Collection<TblDatosEmpleoEstudioActual> attachedTblDatosEmpleoEstudioActualCollectionNew = new ArrayList<TblDatosEmpleoEstudioActual>();
            for (TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActualToAttach : tblDatosEmpleoEstudioActualCollectionNew) {
                tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActualToAttach = em.getReference(tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActualToAttach.getClass(), tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActualToAttach.getIdEmpleoEstudio());
                attachedTblDatosEmpleoEstudioActualCollectionNew.add(tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActualToAttach);
            }
            tblDatosEmpleoEstudioActualCollectionNew = attachedTblDatosEmpleoEstudioActualCollectionNew;
            tblDatosPersonales.setTblDatosEmpleoEstudioActualCollection(tblDatosEmpleoEstudioActualCollectionNew);
            Collection<TblTelefonos> attachedTblTelefonosCollectionNew = new ArrayList<TblTelefonos>();
            for (TblTelefonos tblTelefonosCollectionNewTblTelefonosToAttach : tblTelefonosCollectionNew) {
                tblTelefonosCollectionNewTblTelefonosToAttach = em.getReference(tblTelefonosCollectionNewTblTelefonosToAttach.getClass(), tblTelefonosCollectionNewTblTelefonosToAttach.getIdTelefono());
                attachedTblTelefonosCollectionNew.add(tblTelefonosCollectionNewTblTelefonosToAttach);
            }
            tblTelefonosCollectionNew = attachedTblTelefonosCollectionNew;
            tblDatosPersonales.setTblTelefonosCollection(tblTelefonosCollectionNew);
            tblDatosPersonales = em.merge(tblDatosPersonales);
            for (TblDatosAcademicos tblDatosAcademicosCollectionNewTblDatosAcademicos : tblDatosAcademicosCollectionNew) {
                if (!tblDatosAcademicosCollectionOld.contains(tblDatosAcademicosCollectionNewTblDatosAcademicos)) {
                    TblDatosPersonales oldIdDatPersonOfTblDatosAcademicosCollectionNewTblDatosAcademicos = tblDatosAcademicosCollectionNewTblDatosAcademicos.getIdDatPerson();
                    tblDatosAcademicosCollectionNewTblDatosAcademicos.setIdDatPerson(tblDatosPersonales);
                    tblDatosAcademicosCollectionNewTblDatosAcademicos = em.merge(tblDatosAcademicosCollectionNewTblDatosAcademicos);
                    if (oldIdDatPersonOfTblDatosAcademicosCollectionNewTblDatosAcademicos != null && !oldIdDatPersonOfTblDatosAcademicosCollectionNewTblDatosAcademicos.equals(tblDatosPersonales)) {
                        oldIdDatPersonOfTblDatosAcademicosCollectionNewTblDatosAcademicos.getTblDatosAcademicosCollection().remove(tblDatosAcademicosCollectionNewTblDatosAcademicos);
                        oldIdDatPersonOfTblDatosAcademicosCollectionNewTblDatosAcademicos = em.merge(oldIdDatPersonOfTblDatosAcademicosCollectionNewTblDatosAcademicos);
                    }
                }
            }
            for (TblPadres tblPadresCollectionNewTblPadres : tblPadresCollectionNew) {
                if (!tblPadresCollectionOld.contains(tblPadresCollectionNewTblPadres)) {
                    TblDatosPersonales oldIdDatPersonOfTblPadresCollectionNewTblPadres = tblPadresCollectionNewTblPadres.getIdDatPerson();
                    tblPadresCollectionNewTblPadres.setIdDatPerson(tblDatosPersonales);
                    tblPadresCollectionNewTblPadres = em.merge(tblPadresCollectionNewTblPadres);
                    if (oldIdDatPersonOfTblPadresCollectionNewTblPadres != null && !oldIdDatPersonOfTblPadresCollectionNewTblPadres.equals(tblDatosPersonales)) {
                        oldIdDatPersonOfTblPadresCollectionNewTblPadres.getTblPadresCollection().remove(tblPadresCollectionNewTblPadres);
                        oldIdDatPersonOfTblPadresCollectionNewTblPadres = em.merge(oldIdDatPersonOfTblPadresCollectionNewTblPadres);
                    }
                }
            }
            for (TblReferencias tblReferenciasCollectionNewTblReferencias : tblReferenciasCollectionNew) {
                if (!tblReferenciasCollectionOld.contains(tblReferenciasCollectionNewTblReferencias)) {
                    TblDatosPersonales oldIdDatPersonOfTblReferenciasCollectionNewTblReferencias = tblReferenciasCollectionNewTblReferencias.getIdDatPerson();
                    tblReferenciasCollectionNewTblReferencias.setIdDatPerson(tblDatosPersonales);
                    tblReferenciasCollectionNewTblReferencias = em.merge(tblReferenciasCollectionNewTblReferencias);
                    if (oldIdDatPersonOfTblReferenciasCollectionNewTblReferencias != null && !oldIdDatPersonOfTblReferenciasCollectionNewTblReferencias.equals(tblDatosPersonales)) {
                        oldIdDatPersonOfTblReferenciasCollectionNewTblReferencias.getTblReferenciasCollection().remove(tblReferenciasCollectionNewTblReferencias);
                        oldIdDatPersonOfTblReferenciasCollectionNewTblReferencias = em.merge(oldIdDatPersonOfTblReferenciasCollectionNewTblReferencias);
                    }
                }
            }
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngreso : tblSolicitudIngresoCollectionNew) {
                if (!tblSolicitudIngresoCollectionOld.contains(tblSolicitudIngresoCollectionNewTblSolicitudIngreso)) {
                    TblDatosPersonales oldIdDatPersonOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = tblSolicitudIngresoCollectionNewTblSolicitudIngreso.getIdDatPerson();
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso.setIdDatPerson(tblDatosPersonales);
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    if (oldIdDatPersonOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso != null && !oldIdDatPersonOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.equals(tblDatosPersonales)) {
                        oldIdDatPersonOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                        oldIdDatPersonOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(oldIdDatPersonOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    }
                }
            }
            for (TblDireccion tblDireccionCollectionNewTblDireccion : tblDireccionCollectionNew) {
                if (!tblDireccionCollectionOld.contains(tblDireccionCollectionNewTblDireccion)) {
                    TblDatosPersonales oldIdDatPersonOfTblDireccionCollectionNewTblDireccion = tblDireccionCollectionNewTblDireccion.getIdDatPerson();
                    tblDireccionCollectionNewTblDireccion.setIdDatPerson(tblDatosPersonales);
                    tblDireccionCollectionNewTblDireccion = em.merge(tblDireccionCollectionNewTblDireccion);
                    if (oldIdDatPersonOfTblDireccionCollectionNewTblDireccion != null && !oldIdDatPersonOfTblDireccionCollectionNewTblDireccion.equals(tblDatosPersonales)) {
                        oldIdDatPersonOfTblDireccionCollectionNewTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionNewTblDireccion);
                        oldIdDatPersonOfTblDireccionCollectionNewTblDireccion = em.merge(oldIdDatPersonOfTblDireccionCollectionNewTblDireccion);
                    }
                }
            }
            for (TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual : tblDatosEmpleoEstudioActualCollectionNew) {
                if (!tblDatosEmpleoEstudioActualCollectionOld.contains(tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual)) {
                    TblDatosPersonales oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual = tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual.getIdDatosPersonales();
                    tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual.setIdDatosPersonales(tblDatosPersonales);
                    tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual = em.merge(tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual);
                    if (oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual != null && !oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual.equals(tblDatosPersonales)) {
                        oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual.getTblDatosEmpleoEstudioActualCollection().remove(tblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual);
                        oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual = em.merge(oldIdDatosPersonalesOfTblDatosEmpleoEstudioActualCollectionNewTblDatosEmpleoEstudioActual);
                    }
                }
            }
            for (TblTelefonos tblTelefonosCollectionNewTblTelefonos : tblTelefonosCollectionNew) {
                if (!tblTelefonosCollectionOld.contains(tblTelefonosCollectionNewTblTelefonos)) {
                    TblDatosPersonales oldIdDatPersonOfTblTelefonosCollectionNewTblTelefonos = tblTelefonosCollectionNewTblTelefonos.getIdDatPerson();
                    tblTelefonosCollectionNewTblTelefonos.setIdDatPerson(tblDatosPersonales);
                    tblTelefonosCollectionNewTblTelefonos = em.merge(tblTelefonosCollectionNewTblTelefonos);
                    if (oldIdDatPersonOfTblTelefonosCollectionNewTblTelefonos != null && !oldIdDatPersonOfTblTelefonosCollectionNewTblTelefonos.equals(tblDatosPersonales)) {
                        oldIdDatPersonOfTblTelefonosCollectionNewTblTelefonos.getTblTelefonosCollection().remove(tblTelefonosCollectionNewTblTelefonos);
                        oldIdDatPersonOfTblTelefonosCollectionNewTblTelefonos = em.merge(oldIdDatPersonOfTblTelefonosCollectionNewTblTelefonos);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblDatosPersonales.getIdDatPerson();
                if (findTblDatosPersonales(id) == null) {
                    throw new NonexistentEntityException("The tblDatosPersonales with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosPersonales tblDatosPersonales;
            try {
                tblDatosPersonales = em.getReference(TblDatosPersonales.class, id);
                tblDatosPersonales.getIdDatPerson();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblDatosPersonales with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblDatosAcademicos> tblDatosAcademicosCollectionOrphanCheck = tblDatosPersonales.getTblDatosAcademicosCollection();
            for (TblDatosAcademicos tblDatosAcademicosCollectionOrphanCheckTblDatosAcademicos : tblDatosAcademicosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosPersonales (" + tblDatosPersonales + ") cannot be destroyed since the TblDatosAcademicos " + tblDatosAcademicosCollectionOrphanCheckTblDatosAcademicos + " in its tblDatosAcademicosCollection field has a non-nullable idDatPerson field.");
            }
            Collection<TblPadres> tblPadresCollectionOrphanCheck = tblDatosPersonales.getTblPadresCollection();
            for (TblPadres tblPadresCollectionOrphanCheckTblPadres : tblPadresCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosPersonales (" + tblDatosPersonales + ") cannot be destroyed since the TblPadres " + tblPadresCollectionOrphanCheckTblPadres + " in its tblPadresCollection field has a non-nullable idDatPerson field.");
            }
            Collection<TblReferencias> tblReferenciasCollectionOrphanCheck = tblDatosPersonales.getTblReferenciasCollection();
            for (TblReferencias tblReferenciasCollectionOrphanCheckTblReferencias : tblReferenciasCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosPersonales (" + tblDatosPersonales + ") cannot be destroyed since the TblReferencias " + tblReferenciasCollectionOrphanCheckTblReferencias + " in its tblReferenciasCollection field has a non-nullable idDatPerson field.");
            }
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOrphanCheck = tblDatosPersonales.getTblSolicitudIngresoCollection();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso : tblSolicitudIngresoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosPersonales (" + tblDatosPersonales + ") cannot be destroyed since the TblSolicitudIngreso " + tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso + " in its tblSolicitudIngresoCollection field has a non-nullable idDatPerson field.");
            }
            Collection<TblDireccion> tblDireccionCollectionOrphanCheck = tblDatosPersonales.getTblDireccionCollection();
            for (TblDireccion tblDireccionCollectionOrphanCheckTblDireccion : tblDireccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosPersonales (" + tblDatosPersonales + ") cannot be destroyed since the TblDireccion " + tblDireccionCollectionOrphanCheckTblDireccion + " in its tblDireccionCollection field has a non-nullable idDatPerson field.");
            }
            Collection<TblDatosEmpleoEstudioActual> tblDatosEmpleoEstudioActualCollectionOrphanCheck = tblDatosPersonales.getTblDatosEmpleoEstudioActualCollection();
            for (TblDatosEmpleoEstudioActual tblDatosEmpleoEstudioActualCollectionOrphanCheckTblDatosEmpleoEstudioActual : tblDatosEmpleoEstudioActualCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosPersonales (" + tblDatosPersonales + ") cannot be destroyed since the TblDatosEmpleoEstudioActual " + tblDatosEmpleoEstudioActualCollectionOrphanCheckTblDatosEmpleoEstudioActual + " in its tblDatosEmpleoEstudioActualCollection field has a non-nullable idDatosPersonales field.");
            }
            Collection<TblTelefonos> tblTelefonosCollectionOrphanCheck = tblDatosPersonales.getTblTelefonosCollection();
            for (TblTelefonos tblTelefonosCollectionOrphanCheckTblTelefonos : tblTelefonosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosPersonales (" + tblDatosPersonales + ") cannot be destroyed since the TblTelefonos " + tblTelefonosCollectionOrphanCheckTblTelefonos + " in its tblTelefonosCollection field has a non-nullable idDatPerson field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tblDatosPersonales);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblDatosPersonales> findTblDatosPersonalesEntities() {
        return findTblDatosPersonalesEntities(true, -1, -1);
    }

    public List<TblDatosPersonales> findTblDatosPersonalesEntities(int maxResults, int firstResult) {
        return findTblDatosPersonalesEntities(false, maxResults, firstResult);
    }

    private List<TblDatosPersonales> findTblDatosPersonalesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblDatosPersonales.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblDatosPersonales findTblDatosPersonales(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblDatosPersonales.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblDatosPersonalesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblDatosPersonales> rt = cq.from(TblDatosPersonales.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
