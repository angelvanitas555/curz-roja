/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.exceptions.IllegalOrphanException;
import controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TblDatosPersonales;
import persistencia.TblDireccion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.TblReferencias;

/**
 *
 * @author Jose.azucenaUSAM
 */
public class TblReferenciasJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblReferenciasJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZ_ROJAPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblReferencias tblReferencias) {
        if (tblReferencias.getTblDireccionCollection() == null) {
            tblReferencias.setTblDireccionCollection(new ArrayList<TblDireccion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosPersonales idDatPerson = tblReferencias.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson = em.getReference(idDatPerson.getClass(), idDatPerson.getIdDatPerson());
                tblReferencias.setIdDatPerson(idDatPerson);
            }
            Collection<TblDireccion> attachedTblDireccionCollection = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionTblDireccionToAttach : tblReferencias.getTblDireccionCollection()) {
                tblDireccionCollectionTblDireccionToAttach = em.getReference(tblDireccionCollectionTblDireccionToAttach.getClass(), tblDireccionCollectionTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollection.add(tblDireccionCollectionTblDireccionToAttach);
            }
            tblReferencias.setTblDireccionCollection(attachedTblDireccionCollection);
            em.persist(tblReferencias);
            if (idDatPerson != null) {
                idDatPerson.getTblReferenciasCollection().add(tblReferencias);
                idDatPerson = em.merge(idDatPerson);
            }
            for (TblDireccion tblDireccionCollectionTblDireccion : tblReferencias.getTblDireccionCollection()) {
                TblReferencias oldIdReferenciasOfTblDireccionCollectionTblDireccion = tblDireccionCollectionTblDireccion.getIdReferencias();
                tblDireccionCollectionTblDireccion.setIdReferencias(tblReferencias);
                tblDireccionCollectionTblDireccion = em.merge(tblDireccionCollectionTblDireccion);
                if (oldIdReferenciasOfTblDireccionCollectionTblDireccion != null) {
                    oldIdReferenciasOfTblDireccionCollectionTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionTblDireccion);
                    oldIdReferenciasOfTblDireccionCollectionTblDireccion = em.merge(oldIdReferenciasOfTblDireccionCollectionTblDireccion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblReferencias tblReferencias) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblReferencias persistentTblReferencias = em.find(TblReferencias.class, tblReferencias.getIdReferencia());
            TblDatosPersonales idDatPersonOld = persistentTblReferencias.getIdDatPerson();
            TblDatosPersonales idDatPersonNew = tblReferencias.getIdDatPerson();
            Collection<TblDireccion> tblDireccionCollectionOld = persistentTblReferencias.getTblDireccionCollection();
            Collection<TblDireccion> tblDireccionCollectionNew = tblReferencias.getTblDireccionCollection();
            List<String> illegalOrphanMessages = null;
            for (TblDireccion tblDireccionCollectionOldTblDireccion : tblDireccionCollectionOld) {
                if (!tblDireccionCollectionNew.contains(tblDireccionCollectionOldTblDireccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDireccion " + tblDireccionCollectionOldTblDireccion + " since its idReferencias field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idDatPersonNew != null) {
                idDatPersonNew = em.getReference(idDatPersonNew.getClass(), idDatPersonNew.getIdDatPerson());
                tblReferencias.setIdDatPerson(idDatPersonNew);
            }
            Collection<TblDireccion> attachedTblDireccionCollectionNew = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionNewTblDireccionToAttach : tblDireccionCollectionNew) {
                tblDireccionCollectionNewTblDireccionToAttach = em.getReference(tblDireccionCollectionNewTblDireccionToAttach.getClass(), tblDireccionCollectionNewTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollectionNew.add(tblDireccionCollectionNewTblDireccionToAttach);
            }
            tblDireccionCollectionNew = attachedTblDireccionCollectionNew;
            tblReferencias.setTblDireccionCollection(tblDireccionCollectionNew);
            tblReferencias = em.merge(tblReferencias);
            if (idDatPersonOld != null && !idDatPersonOld.equals(idDatPersonNew)) {
                idDatPersonOld.getTblReferenciasCollection().remove(tblReferencias);
                idDatPersonOld = em.merge(idDatPersonOld);
            }
            if (idDatPersonNew != null && !idDatPersonNew.equals(idDatPersonOld)) {
                idDatPersonNew.getTblReferenciasCollection().add(tblReferencias);
                idDatPersonNew = em.merge(idDatPersonNew);
            }
            for (TblDireccion tblDireccionCollectionNewTblDireccion : tblDireccionCollectionNew) {
                if (!tblDireccionCollectionOld.contains(tblDireccionCollectionNewTblDireccion)) {
                    TblReferencias oldIdReferenciasOfTblDireccionCollectionNewTblDireccion = tblDireccionCollectionNewTblDireccion.getIdReferencias();
                    tblDireccionCollectionNewTblDireccion.setIdReferencias(tblReferencias);
                    tblDireccionCollectionNewTblDireccion = em.merge(tblDireccionCollectionNewTblDireccion);
                    if (oldIdReferenciasOfTblDireccionCollectionNewTblDireccion != null && !oldIdReferenciasOfTblDireccionCollectionNewTblDireccion.equals(tblReferencias)) {
                        oldIdReferenciasOfTblDireccionCollectionNewTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionNewTblDireccion);
                        oldIdReferenciasOfTblDireccionCollectionNewTblDireccion = em.merge(oldIdReferenciasOfTblDireccionCollectionNewTblDireccion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblReferencias.getIdReferencia();
                if (findTblReferencias(id) == null) {
                    throw new NonexistentEntityException("The tblReferencias with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblReferencias tblReferencias;
            try {
                tblReferencias = em.getReference(TblReferencias.class, id);
                tblReferencias.getIdReferencia();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblReferencias with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblDireccion> tblDireccionCollectionOrphanCheck = tblReferencias.getTblDireccionCollection();
            for (TblDireccion tblDireccionCollectionOrphanCheckTblDireccion : tblDireccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblReferencias (" + tblReferencias + ") cannot be destroyed since the TblDireccion " + tblDireccionCollectionOrphanCheckTblDireccion + " in its tblDireccionCollection field has a non-nullable idReferencias field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TblDatosPersonales idDatPerson = tblReferencias.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson.getTblReferenciasCollection().remove(tblReferencias);
                idDatPerson = em.merge(idDatPerson);
            }
            em.remove(tblReferencias);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblReferencias> findTblReferenciasEntities() {
        return findTblReferenciasEntities(true, -1, -1);
    }

    public List<TblReferencias> findTblReferenciasEntities(int maxResults, int firstResult) {
        return findTblReferenciasEntities(false, maxResults, firstResult);
    }

    private List<TblReferencias> findTblReferenciasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblReferencias.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblReferencias findTblReferencias(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblReferencias.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblReferenciasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblReferencias> rt = cq.from(TblReferencias.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
