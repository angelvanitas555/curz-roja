/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TblDatosPersonales;
import persistencia.TblPadres;

/**
 *
 * @author Jose.azucenaUSAM
 */
public class TblPadresJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblPadresJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZ_ROJAPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblPadres tblPadres) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosPersonales idDatPerson = tblPadres.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson = em.getReference(idDatPerson.getClass(), idDatPerson.getIdDatPerson());
                tblPadres.setIdDatPerson(idDatPerson);
            }
            em.persist(tblPadres);
            if (idDatPerson != null) {
                idDatPerson.getTblPadresCollection().add(tblPadres);
                idDatPerson = em.merge(idDatPerson);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblPadres tblPadres) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblPadres persistentTblPadres = em.find(TblPadres.class, tblPadres.getIdPadres());
            TblDatosPersonales idDatPersonOld = persistentTblPadres.getIdDatPerson();
            TblDatosPersonales idDatPersonNew = tblPadres.getIdDatPerson();
            if (idDatPersonNew != null) {
                idDatPersonNew = em.getReference(idDatPersonNew.getClass(), idDatPersonNew.getIdDatPerson());
                tblPadres.setIdDatPerson(idDatPersonNew);
            }
            tblPadres = em.merge(tblPadres);
            if (idDatPersonOld != null && !idDatPersonOld.equals(idDatPersonNew)) {
                idDatPersonOld.getTblPadresCollection().remove(tblPadres);
                idDatPersonOld = em.merge(idDatPersonOld);
            }
            if (idDatPersonNew != null && !idDatPersonNew.equals(idDatPersonOld)) {
                idDatPersonNew.getTblPadresCollection().add(tblPadres);
                idDatPersonNew = em.merge(idDatPersonNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblPadres.getIdPadres();
                if (findTblPadres(id) == null) {
                    throw new NonexistentEntityException("The tblPadres with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblPadres tblPadres;
            try {
                tblPadres = em.getReference(TblPadres.class, id);
                tblPadres.getIdPadres();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblPadres with id " + id + " no longer exists.", enfe);
            }
            TblDatosPersonales idDatPerson = tblPadres.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson.getTblPadresCollection().remove(tblPadres);
                idDatPerson = em.merge(idDatPerson);
            }
            em.remove(tblPadres);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblPadres> findTblPadresEntities() {
        return findTblPadresEntities(true, -1, -1);
    }

    public List<TblPadres> findTblPadresEntities(int maxResults, int firstResult) {
        return findTblPadresEntities(false, maxResults, firstResult);
    }

    private List<TblPadres> findTblPadresEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblPadres.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblPadres findTblPadres(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblPadres.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblPadresCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblPadres> rt = cq.from(TblPadres.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
