/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TblFormularioEntrevista;
import persistencia.TblSolicitudIngreso;

/**
 *
 * @author Jose.azucenaUSAM
 */
public class TblFormularioEntrevistaJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblFormularioEntrevistaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZ_ROJAPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblFormularioEntrevista tblFormularioEntrevista) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSolicitudIngreso idSolicitud = tblFormularioEntrevista.getIdSolicitud();
            if (idSolicitud != null) {
                idSolicitud = em.getReference(idSolicitud.getClass(), idSolicitud.getIdSolicitud());
                tblFormularioEntrevista.setIdSolicitud(idSolicitud);
            }
            em.persist(tblFormularioEntrevista);
            if (idSolicitud != null) {
                idSolicitud.getTblFormularioEntrevistaCollection().add(tblFormularioEntrevista);
                idSolicitud = em.merge(idSolicitud);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblFormularioEntrevista tblFormularioEntrevista) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblFormularioEntrevista persistentTblFormularioEntrevista = em.find(TblFormularioEntrevista.class, tblFormularioEntrevista.getIdformularioEntrevista());
            TblSolicitudIngreso idSolicitudOld = persistentTblFormularioEntrevista.getIdSolicitud();
            TblSolicitudIngreso idSolicitudNew = tblFormularioEntrevista.getIdSolicitud();
            if (idSolicitudNew != null) {
                idSolicitudNew = em.getReference(idSolicitudNew.getClass(), idSolicitudNew.getIdSolicitud());
                tblFormularioEntrevista.setIdSolicitud(idSolicitudNew);
            }
            tblFormularioEntrevista = em.merge(tblFormularioEntrevista);
            if (idSolicitudOld != null && !idSolicitudOld.equals(idSolicitudNew)) {
                idSolicitudOld.getTblFormularioEntrevistaCollection().remove(tblFormularioEntrevista);
                idSolicitudOld = em.merge(idSolicitudOld);
            }
            if (idSolicitudNew != null && !idSolicitudNew.equals(idSolicitudOld)) {
                idSolicitudNew.getTblFormularioEntrevistaCollection().add(tblFormularioEntrevista);
                idSolicitudNew = em.merge(idSolicitudNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblFormularioEntrevista.getIdformularioEntrevista();
                if (findTblFormularioEntrevista(id) == null) {
                    throw new NonexistentEntityException("The tblFormularioEntrevista with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblFormularioEntrevista tblFormularioEntrevista;
            try {
                tblFormularioEntrevista = em.getReference(TblFormularioEntrevista.class, id);
                tblFormularioEntrevista.getIdformularioEntrevista();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblFormularioEntrevista with id " + id + " no longer exists.", enfe);
            }
            TblSolicitudIngreso idSolicitud = tblFormularioEntrevista.getIdSolicitud();
            if (idSolicitud != null) {
                idSolicitud.getTblFormularioEntrevistaCollection().remove(tblFormularioEntrevista);
                idSolicitud = em.merge(idSolicitud);
            }
            em.remove(tblFormularioEntrevista);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblFormularioEntrevista> findTblFormularioEntrevistaEntities() {
        return findTblFormularioEntrevistaEntities(true, -1, -1);
    }

    public List<TblFormularioEntrevista> findTblFormularioEntrevistaEntities(int maxResults, int firstResult) {
        return findTblFormularioEntrevistaEntities(false, maxResults, firstResult);
    }

    private List<TblFormularioEntrevista> findTblFormularioEntrevistaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblFormularioEntrevista.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblFormularioEntrevista findTblFormularioEntrevista(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblFormularioEntrevista.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblFormularioEntrevistaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblFormularioEntrevista> rt = cq.from(TblFormularioEntrevista.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
