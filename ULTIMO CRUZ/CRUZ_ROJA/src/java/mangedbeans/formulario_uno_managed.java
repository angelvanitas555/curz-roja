/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedbeans;

import controladores.TblDatosEmpleoEstudioActualJpaController;
import controladores.TblDatosMedicosJpaController;
import controladores.TblDatosPersonalesJpaController;
import controladores.TblReferenciasJpaController;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import persistencia.TblDatosEmpleoEstudioActual;
import persistencia.TblDatosMedicos;
import persistencia.TblDatosPersonales;
import persistencia.TblReferencias;

/**
 *
 * @author Jose.azucenaUSAM
 */
@ManagedBean(name = "formulario_uno")
@SessionScoped
public class formulario_uno_managed {
    
   private TblDatosPersonalesJpaController control_uno = new TblDatosPersonalesJpaController();
   private  TblDatosPersonales personales_uno = new TblDatosPersonales();
    
    //REFERENCIAS PERSONALES
   private  TblReferenciasJpaController control_referencia_rel = new TblReferenciasJpaController();
   private  TblReferencias referencias_rel = new TblReferencias();
    
    //DSTOS MEDICOS
   private  TblDatosMedicosJpaController contro_d_medicos = new TblDatosMedicosJpaController();
   private  TblDatosMedicos d_medicos = new TblDatosMedicos();
    
    //DATOS EMPLEO ACTUAL ESTUDIO ACTUAL
   private  TblDatosEmpleoEstudioActualJpaController control_empleo_rel = new TblDatosEmpleoEstudioActualJpaController();
   private  TblDatosEmpleoEstudioActual empleo_actual_rel = new TblDatosEmpleoEstudioActual();
   
   //AQUI ESTAN LAS VARIABLES PARA INICIALIZAR LOS ID'S
   
     private Integer id_rel_datpersonal;
     private Integer id_rel_dat_medicos;
     private Integer id_rel_empleo_estudio;
     private Integer id_rel_referencias_rel;
  

   

    public TblDatosPersonalesJpaController getControl_uno() {
        return control_uno;
    }

    public void setControl_uno(TblDatosPersonalesJpaController control_uno) {
        this.control_uno = control_uno;
    }

    public TblDatosPersonales getPersonales_uno() {
        return personales_uno;
    }

    public void setPersonales_uno(TblDatosPersonales personales_uno) {
        this.personales_uno = personales_uno;
    }

    public TblReferenciasJpaController getControl_referencia_rel() {
        return control_referencia_rel;
    }

    public void setControl_referencia_rel(TblReferenciasJpaController control_referencia_rel) {
        this.control_referencia_rel = control_referencia_rel;
    }

    public TblReferencias getReferencias_rel() {
        return referencias_rel;
    }

    public void setReferencias_rel(TblReferencias referencias_rel) {
        this.referencias_rel = referencias_rel;
    }

    public TblDatosMedicosJpaController getContro_d_medicos() {
        return contro_d_medicos;
    }

    public void setContro_d_medicos(TblDatosMedicosJpaController contro_d_medicos) {
        this.contro_d_medicos = contro_d_medicos;
    }

    public TblDatosMedicos getD_medicos() {
        return d_medicos;
    }

    public void setD_medicos(TblDatosMedicos d_medicos) {
        this.d_medicos = d_medicos;
    }

    public TblDatosEmpleoEstudioActualJpaController getControl_empleo_rel() {
        return control_empleo_rel;
    }

    public void setControl_empleo_rel(TblDatosEmpleoEstudioActualJpaController control_empleo_rel) {
        this.control_empleo_rel = control_empleo_rel;
    }

    public TblDatosEmpleoEstudioActual getEmpleo_actual_rel() {
        return empleo_actual_rel;
    }

    public void setEmpleo_actual_rel(TblDatosEmpleoEstudioActual empleo_actual_rel) {
        this.empleo_actual_rel = empleo_actual_rel;
    }

    //METODOS PARA GUARDAR
     
     //guardar 
     public void insertar_grupo(){
     TblDatosPersonales pnal = new TblDatosPersonales();
     TblReferencias  reff = new TblReferencias();
     TblDatosMedicos d_medi = new TblDatosMedicos();
     TblDatosEmpleoEstudioActual empleo = new TblDatosEmpleoEstudioActual();
     
     pnal.setIdDatPerson(id_rel_datpersonal);
     reff.setIdReferencia(id_rel_referencias_rel);
     d_medi.setIdDatosMedicos(id_rel_dat_medicos);
     empleo.setIdEmpleoEstudio(id_rel_empleo_estudio);
     control_uno.create(personales_uno);//persistencia principal
     }
    

    //CREAMOS LA ENCAPSULACION PARA TODO
   

    public formulario_uno_managed() {
    }
    
}
