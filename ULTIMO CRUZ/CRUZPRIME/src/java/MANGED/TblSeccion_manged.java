/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MANGED;

import CONTROLLERS.TblSeccionJpaController;
import PERSISTENCIA.TblSeccion;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author jose.azucenaUSAM
 */
@ManagedBean(name = "seccionales")
@RequestScoped
public class TblSeccion_manged {

    TblSeccionJpaController control = new TblSeccionJpaController();
    TblSeccion seccion = new TblSeccion();

    public TblSeccionJpaController getControl() {
        return control;
    }

    public void setControl(TblSeccionJpaController control) {
        this.control = control;
    }

    public TblSeccion getSeccion() {
        return seccion;
    }

    public void setSeccion(TblSeccion seccion) {
        this.seccion = seccion;
    }
    
    public TblSeccion_manged() {
    }
    
}
