/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MANGED;

import CONTROLLERS.TblTelefonosJpaController;
import PERSISTENCIA.TblTelefonos;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author jose.azucenaUSAM
 */
@ManagedBean(name = "phone")
@RequestScoped
public class TblTelefonos_managed {
    
    TblTelefonosJpaController control = new TblTelefonosJpaController();
    TblTelefonos phones = new TblTelefonos();

    public TblTelefonosJpaController getControl() {
        return control;
    }

    public void setControl(TblTelefonosJpaController control) {
        this.control = control;
    }

    public TblTelefonos getPhones() {
        return phones;
    }

    public void setPhones(TblTelefonos phones) {
        this.phones = phones;
    }
    public TblTelefonos_managed() {
    }
    
}
