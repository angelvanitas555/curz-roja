/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MANGED;

import CONTROLLERS.TblMunicipioJpaController;
import PERSISTENCIA.TblDatosPersonales;
import PERSISTENCIA.TblMunicipio;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author jose.azucenaUSAM
 */
@ManagedBean(name = "municipio")
@RequestScoped
public class TblMunicipio_manged {

    TblMunicipioJpaController control = new TblMunicipioJpaController();
    TblMunicipio municipio = new TblMunicipio();
    
   private TblDatosPersonales_manged man_personal = new TblDatosPersonales_manged();

    public TblDatosPersonales_manged getMan_personal() {
        return man_personal;
    }

    public void setMan_personal(TblDatosPersonales_manged man_personal) {
        this.man_personal = man_personal;
    }

    public TblDatosPersonales getPersispersonal() {
        return persispersonal;
    }

    public void setPersispersonal(TblDatosPersonales persispersonal) {
        this.persispersonal = persispersonal;
    }
    
    TblDatosPersonales persispersonal = new TblDatosPersonales();

    public TblMunicipioJpaController getControl() {
        return control;
    }

    public void setControl(TblMunicipioJpaController control) {
        this.control = control;
    }

    public TblMunicipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(TblMunicipio municipio) {
        this.municipio = municipio;
    }
   
    
    
}
