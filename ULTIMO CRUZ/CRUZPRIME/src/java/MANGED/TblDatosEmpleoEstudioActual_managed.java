/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MANGED;

import CONTROLLERS.TblDatosEmpleoEstudioActualJpaController;
import CONTROLLERS.TblDatosPersonalesJpaController;
import PERSISTENCIA.TblDatosEmpleoEstudioActual;
import PERSISTENCIA.TblDatosPersonales;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author jose.azucenaUSAM
 */
@ManagedBean(name = "estudia")
@SessionScoped
public class TblDatosEmpleoEstudioActual_managed {

    TblDatosEmpleoEstudioActual estu_traba = new TblDatosEmpleoEstudioActual();
    TblDatosEmpleoEstudioActualJpaController control = new TblDatosEmpleoEstudioActualJpaController();
    TblDatosPersonales personales = new TblDatosPersonales();
    private Integer id_personales;
    
    TblDatosPersonalesJpaController control_personal = new TblDatosPersonalesJpaController();

    public TblDatosPersonalesJpaController getControl_personal() {
        return control_personal;
    }

    public void setControl_personal(TblDatosPersonalesJpaController control_personal) {
        this.control_personal = control_personal;
    }

    public TblDatosEmpleoEstudioActual getEstu_traba() {
        return estu_traba;
    }

    public void setEstu_traba(TblDatosEmpleoEstudioActual estu_traba) {
        this.estu_traba = estu_traba;
    }

    public TblDatosEmpleoEstudioActualJpaController getControl() {
        return control;
    }

    public void setControl(TblDatosEmpleoEstudioActualJpaController control) {
        this.control = control;
    }

    public TblDatosPersonales getPersonales() {
        return personales;
    }

    public void setPersonales(TblDatosPersonales personales) {
        this.personales = personales;
    }

    public Integer getId_personales() {
        return id_personales;
    }

    public void setId_personales(Integer id_personales) {
        this.id_personales = id_personales;
    }
    
    public TblDatosEmpleoEstudioActual_managed() {
    }
    
    public void crear_nexo(){
    personales.setIdDatPerson(id_personales);
    estu_traba.setIdDatosPersonales(personales);
    
    control.create(estu_traba);
    }
    
    

    public Integer getId_p() {
        return id_p;
    }

    public void setId_p(Integer id_p) {
        this.id_p = id_p;
    }
    
    private Integer id_p;
    
    
}
