/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MANGED;

import CONTROLLERS.TblDatosPersonalesJpaController;
import CONTROLLERS.TblReferenciasJpaController;
import PERSISTENCIA.TblDatosPersonales;
import PERSISTENCIA.TblReferencias;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author jose.azucenaUSAM
 */
@ManagedBean(name = "referencias")
@SessionScoped
public class TblReferencias_managed {
    
    TblReferencias reff = new TblReferencias();
    TblReferenciasJpaController control = new TblReferenciasJpaController();
    TblDatosPersonalesJpaController control_personales = new TblDatosPersonalesJpaController();

    public TblDatosPersonalesJpaController getControl_personales() {
        return control_personales;
    }

    public void setControl_personales(TblDatosPersonalesJpaController control_personales) {
        this.control_personales = control_personales;
    }
    
    public TblReferencias getReff() {
        return reff;
    }
    
    public void setReff(TblReferencias reff) {
        this.reff = reff;
    }
    
    public TblReferenciasJpaController getControl() {
        return control;
    }
    
    public void setControl(TblReferenciasJpaController control) {
        this.control = control;
    }
    
    public TblReferencias_managed() {
    }
    
    TblDatosPersonales personales = new TblDatosPersonales();
    
    public TblDatosPersonales getPersonales() {
        return personales;
    }
    
    public void setPersonales(TblDatosPersonales personales) {
        this.personales = personales;
    }
    
    private Integer data_persona;
    
    public Integer getData_persona() {
        return data_persona;
    }
    
    public void setData_persona(Integer data_persona) {
        this.data_persona = data_persona;
    }

    //metodo para insertar el id_data_person
    public void insertar_data_persona() {
        
        TblReferenciasJpaController control_dos = new TblReferenciasJpaController();
        TblDatosPersonales DATOS = new TblDatosPersonales();
        
        DATOS.setIdDatPerson(data_persona);
        reff.setIdDatPerson(DATOS);
        control_dos.create(reff);
        
    }
    
}
