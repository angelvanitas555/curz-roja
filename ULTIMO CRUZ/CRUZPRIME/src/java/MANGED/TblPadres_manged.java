/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MANGED;

import CONTROLLERS.TblDatosPersonalesJpaController;
import CONTROLLERS.TblPadresJpaController;
import PERSISTENCIA.TblDatosPersonales;
import PERSISTENCIA.TblPadres;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author jose.azucenaUSAM
 */
@ManagedBean(name = "padres")
@SessionScoped
public class TblPadres_manged {

    TblPadresJpaController control = new TblPadresJpaController();
    TblPadres padres = new TblPadres();

    public Integer getId_datapersona() {
        return id_datapersona;
    }

    public void setId_datapersona(Integer id_datapersona) {
        this.id_datapersona = id_datapersona;
    }

    private Integer id_datapersona;
    
    public TblPadresJpaController getControl() {
        return control;
    }

    public void setControl(TblPadresJpaController control) {
        this.control = control;
    }

    public TblPadres getPadres() {
        return padres;
    }

    public void setPadres(TblPadres padres) {
        this.padres = padres;
    }

    public TblPadres_manged() {
    }

   

    TblDatosPersonales tabla_d_personales = new TblDatosPersonales();

    public TblDatosPersonalesJpaController getControl_personales() {
        return control_personales;
    }

    public void setControl_personales(TblDatosPersonalesJpaController control_personales) {
        this.control_personales = control_personales;
    }
    TblDatosPersonalesJpaController control_personales = new TblDatosPersonalesJpaController();

    public TblDatosPersonales getTabla_d_personales() {
        return tabla_d_personales;
    }

    public void setTabla_d_personales(TblDatosPersonales tabla_d_personales) {
        this.tabla_d_personales = tabla_d_personales;
    }

   
    // con este metodo inserto valores en el campo de id_data_person de la tabla tbl_datos_personales
    public void crear_padres(){
    //instanciamos el objeto controlador    
    TblPadresJpaController padre_control_dos = new TblPadresJpaController();
    // instanciamos el objeto persistencia en donde setteareamos (persistiremos)
    TblDatosPersonales DATOS = new TblDatosPersonales();
    
    DATOS.setIdDatPerson(id_datapersona);
    padres.setIdDatPerson(DATOS);
    padre_control_dos.create(padres);
    }
}
