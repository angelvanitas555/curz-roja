/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MANGED;

import CONTROLLERS.TblDatosPersonalesJpaController;
import PERSISTENCIA.TblDatosPersonales;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author jose.azucenaUSAM
 */
@ManagedBean(name = "data_personal")
@RequestScoped
public class TblDatosPersonales_manged {

    TblDatosPersonalesJpaController control = new TblDatosPersonalesJpaController();
    TblDatosPersonales datapersonal = new TblDatosPersonales();

    public TblDatosPersonalesJpaController getControl() {
        return control;
    }

    public void setControl(TblDatosPersonalesJpaController control) {
        this.control = control;
    }

    public TblDatosPersonales getDatapersonal() {
        return datapersonal;
    }

    public void setDatapersonal(TblDatosPersonales datapersonal) {
        this.datapersonal = datapersonal;
    }
    
    public TblDatosPersonales_manged() {
    }
    
}
