/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_padres")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblPadres.findAll", query = "SELECT t FROM TblPadres t")
    , @NamedQuery(name = "TblPadres.findByIdPadres", query = "SELECT t FROM TblPadres t WHERE t.idPadres = :idPadres")
    , @NamedQuery(name = "TblPadres.findByNombre", query = "SELECT t FROM TblPadres t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TblPadres.findBySexo", query = "SELECT t FROM TblPadres t WHERE t.sexo = :sexo")
    , @NamedQuery(name = "TblPadres.findByEstado", query = "SELECT t FROM TblPadres t WHERE t.estado = :estado")})
public class TblPadres implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_padres")
    private Integer idPadres;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "sexo")
    private String sexo;
    @Basic(optional = false)
    @Column(name = "estado")
    private boolean estado;
    @JoinColumn(name = "id_dat_person", referencedColumnName = "id_dat_person")
    @ManyToOne(optional = false)
    private TblDatosPersonales idDatPerson;

    public TblPadres() {
    }

    public TblPadres(Integer idPadres) {
        this.idPadres = idPadres;
    }

    public TblPadres(Integer idPadres, String nombre, String sexo, boolean estado) {
        this.idPadres = idPadres;
        this.nombre = nombre;
        this.sexo = sexo;
        this.estado = estado;
    }

    public Integer getIdPadres() {
        return idPadres;
    }

    public void setIdPadres(Integer idPadres) {
        this.idPadres = idPadres;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public TblDatosPersonales getIdDatPerson() {
        return idDatPerson;
    }

    public void setIdDatPerson(TblDatosPersonales idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPadres != null ? idPadres.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblPadres)) {
            return false;
        }
        TblPadres other = (TblPadres) object;
        if ((this.idPadres == null && other.idPadres != null) || (this.idPadres != null && !this.idPadres.equals(other.idPadres))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblPadres[ idPadres=" + idPadres + " ]";
    }
    
}
