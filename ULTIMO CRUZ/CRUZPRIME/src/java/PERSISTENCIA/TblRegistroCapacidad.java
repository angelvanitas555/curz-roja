/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_registro_capacidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblRegistroCapacidad.findAll", query = "SELECT t FROM TblRegistroCapacidad t")
    , @NamedQuery(name = "TblRegistroCapacidad.findByIdRegistroCapacidad", query = "SELECT t FROM TblRegistroCapacidad t WHERE t.idRegistroCapacidad = :idRegistroCapacidad")})
public class TblRegistroCapacidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro_capacidad")
    private Integer idRegistroCapacidad;
    @JoinColumn(name = "id_registro", referencedColumnName = "id_registro")
    @ManyToOne(optional = false)
    private TblRegistro idRegistro;
    @JoinColumn(name = "id_capacitacion", referencedColumnName = "id_capacitaciones")
    @ManyToOne(optional = false)
    private TblCapacitaciones idCapacitacion;

    public TblRegistroCapacidad() {
    }

    public TblRegistroCapacidad(Integer idRegistroCapacidad) {
        this.idRegistroCapacidad = idRegistroCapacidad;
    }

    public Integer getIdRegistroCapacidad() {
        return idRegistroCapacidad;
    }

    public void setIdRegistroCapacidad(Integer idRegistroCapacidad) {
        this.idRegistroCapacidad = idRegistroCapacidad;
    }

    public TblRegistro getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(TblRegistro idRegistro) {
        this.idRegistro = idRegistro;
    }

    public TblCapacitaciones getIdCapacitacion() {
        return idCapacitacion;
    }

    public void setIdCapacitacion(TblCapacitaciones idCapacitacion) {
        this.idCapacitacion = idCapacitacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistroCapacidad != null ? idRegistroCapacidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblRegistroCapacidad)) {
            return false;
        }
        TblRegistroCapacidad other = (TblRegistroCapacidad) object;
        if ((this.idRegistroCapacidad == null && other.idRegistroCapacidad != null) || (this.idRegistroCapacidad != null && !this.idRegistroCapacidad.equals(other.idRegistroCapacidad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblRegistroCapacidad[ idRegistroCapacidad=" + idRegistroCapacidad + " ]";
    }
    
}
