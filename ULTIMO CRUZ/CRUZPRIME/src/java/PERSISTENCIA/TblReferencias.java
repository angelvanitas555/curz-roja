/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_referencias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblReferencias.findAll", query = "SELECT t FROM TblReferencias t")
    , @NamedQuery(name = "TblReferencias.findByIdReferencia", query = "SELECT t FROM TblReferencias t WHERE t.idReferencia = :idReferencia")
    , @NamedQuery(name = "TblReferencias.findByNombreReferencia", query = "SELECT t FROM TblReferencias t WHERE t.nombreReferencia = :nombreReferencia")
    , @NamedQuery(name = "TblReferencias.findByTelefono", query = "SELECT t FROM TblReferencias t WHERE t.telefono = :telefono")
    , @NamedQuery(name = "TblReferencias.findByTipo", query = "SELECT t FROM TblReferencias t WHERE t.tipo = :tipo")})
public class TblReferencias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_referencia")
    private Integer idReferencia;
    @Basic(optional = false)
    @Column(name = "nombre_referencia")
    private String nombreReferencia;
    @Basic(optional = false)
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @Column(name = "tipo")
    private String tipo;
    @JoinColumn(name = "id_dat_person", referencedColumnName = "id_dat_person")
    @ManyToOne(optional = false)
    private TblDatosPersonales idDatPerson;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReferencias")
    private Collection<TblDireccion> tblDireccionCollection;

    public TblReferencias() {
    }

    public TblReferencias(Integer idReferencia) {
        this.idReferencia = idReferencia;
    }

    public TblReferencias(Integer idReferencia, String nombreReferencia, String telefono, String tipo) {
        this.idReferencia = idReferencia;
        this.nombreReferencia = nombreReferencia;
        this.telefono = telefono;
        this.tipo = tipo;
    }

    public Integer getIdReferencia() {
        return idReferencia;
    }

    public void setIdReferencia(Integer idReferencia) {
        this.idReferencia = idReferencia;
    }

    public String getNombreReferencia() {
        return nombreReferencia;
    }

    public void setNombreReferencia(String nombreReferencia) {
        this.nombreReferencia = nombreReferencia;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public TblDatosPersonales getIdDatPerson() {
        return idDatPerson;
    }

    public void setIdDatPerson(TblDatosPersonales idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    @XmlTransient
    public Collection<TblDireccion> getTblDireccionCollection() {
        return tblDireccionCollection;
    }

    public void setTblDireccionCollection(Collection<TblDireccion> tblDireccionCollection) {
        this.tblDireccionCollection = tblDireccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReferencia != null ? idReferencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblReferencias)) {
            return false;
        }
        TblReferencias other = (TblReferencias) object;
        if ((this.idReferencia == null && other.idReferencia != null) || (this.idReferencia != null && !this.idReferencia.equals(other.idReferencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblReferencias[ idReferencia=" + idReferencia + " ]";
    }
    
}
