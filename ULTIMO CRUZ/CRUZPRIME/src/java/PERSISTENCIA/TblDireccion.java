/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_direccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblDireccion.findAll", query = "SELECT t FROM TblDireccion t")
    , @NamedQuery(name = "TblDireccion.findByIdDireccion", query = "SELECT t FROM TblDireccion t WHERE t.idDireccion = :idDireccion")
    , @NamedQuery(name = "TblDireccion.findByDomicilio", query = "SELECT t FROM TblDireccion t WHERE t.domicilio = :domicilio")})
public class TblDireccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_direccion")
    private Integer idDireccion;
    @Basic(optional = false)
    @Column(name = "domicilio")
    private String domicilio;
    @JoinColumn(name = "id_municipio", referencedColumnName = "id_municipio")
    @ManyToOne(optional = false)
    private TblMunicipio idMunicipio;
    @JoinColumn(name = "id_dat_person", referencedColumnName = "id_dat_person")
    @ManyToOne(optional = false)
    private TblDatosPersonales idDatPerson;
    @JoinColumn(name = "id_empleo_estudio", referencedColumnName = "id_empleo_estudio")
    @ManyToOne(optional = false)
    private TblDatosEmpleoEstudioActual idEmpleoEstudio;
    @JoinColumn(name = "id_referencias", referencedColumnName = "id_referencia")
    @ManyToOne(optional = false)
    private TblReferencias idReferencias;
    @JoinColumn(name = "id_datos_medicos", referencedColumnName = "id_datos_medicos")
    @ManyToOne(optional = false)
    private TblDatosMedicos idDatosMedicos;
    @JoinColumn(name = "id_seccion", referencedColumnName = "id_seccion")
    @ManyToOne(optional = false)
    private TblSeccion idSeccion;

    public TblDireccion() {
    }

    public TblDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public TblDireccion(Integer idDireccion, String domicilio) {
        this.idDireccion = idDireccion;
        this.domicilio = domicilio;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public TblMunicipio getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(TblMunicipio idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public TblDatosPersonales getIdDatPerson() {
        return idDatPerson;
    }

    public void setIdDatPerson(TblDatosPersonales idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    public TblDatosEmpleoEstudioActual getIdEmpleoEstudio() {
        return idEmpleoEstudio;
    }

    public void setIdEmpleoEstudio(TblDatosEmpleoEstudioActual idEmpleoEstudio) {
        this.idEmpleoEstudio = idEmpleoEstudio;
    }

    public TblReferencias getIdReferencias() {
        return idReferencias;
    }

    public void setIdReferencias(TblReferencias idReferencias) {
        this.idReferencias = idReferencias;
    }

    public TblDatosMedicos getIdDatosMedicos() {
        return idDatosMedicos;
    }

    public void setIdDatosMedicos(TblDatosMedicos idDatosMedicos) {
        this.idDatosMedicos = idDatosMedicos;
    }

    public TblSeccion getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(TblSeccion idSeccion) {
        this.idSeccion = idSeccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDireccion != null ? idDireccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblDireccion)) {
            return false;
        }
        TblDireccion other = (TblDireccion) object;
        if ((this.idDireccion == null && other.idDireccion != null) || (this.idDireccion != null && !this.idDireccion.equals(other.idDireccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblDireccion[ idDireccion=" + idDireccion + " ]";
    }
    
}
