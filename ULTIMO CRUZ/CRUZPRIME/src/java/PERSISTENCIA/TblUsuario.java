/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblUsuario.findAll", query = "SELECT t FROM TblUsuario t")
    , @NamedQuery(name = "TblUsuario.findByIdUsuario", query = "SELECT t FROM TblUsuario t WHERE t.idUsuario = :idUsuario")
    , @NamedQuery(name = "TblUsuario.findByDocumento", query = "SELECT t FROM TblUsuario t WHERE t.documento = :documento")
    , @NamedQuery(name = "TblUsuario.findByUsuario", query = "SELECT t FROM TblUsuario t WHERE t.usuario = :usuario")
    , @NamedQuery(name = "TblUsuario.findByPass", query = "SELECT t FROM TblUsuario t WHERE t.pass = :pass")})
public class TblUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Basic(optional = false)
    @Column(name = "documento")
    private int documento;
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "pass")
    private String pass;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuario")
    private Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection;

    public TblUsuario() {
    }

    public TblUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public TblUsuario(Integer idUsuario, int documento, String usuario, String pass) {
        this.idUsuario = idUsuario;
        this.documento = documento;
        this.usuario = usuario;
        this.pass = pass;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @XmlTransient
    public Collection<TblSolicitudIngreso> getTblSolicitudIngresoCollection() {
        return tblSolicitudIngresoCollection;
    }

    public void setTblSolicitudIngresoCollection(Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection) {
        this.tblSolicitudIngresoCollection = tblSolicitudIngresoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblUsuario)) {
            return false;
        }
        TblUsuario other = (TblUsuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblUsuario[ idUsuario=" + idUsuario + " ]";
    }
    
}
