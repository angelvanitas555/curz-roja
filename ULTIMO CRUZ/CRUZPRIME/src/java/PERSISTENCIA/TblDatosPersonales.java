/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_datos_personales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblDatosPersonales.findAll", query = "SELECT t FROM TblDatosPersonales t")
    , @NamedQuery(name = "TblDatosPersonales.findByIdDatPerson", query = "SELECT t FROM TblDatosPersonales t WHERE t.idDatPerson = :idDatPerson")
    , @NamedQuery(name = "TblDatosPersonales.findByNombres", query = "SELECT t FROM TblDatosPersonales t WHERE t.nombres = :nombres")
    , @NamedQuery(name = "TblDatosPersonales.findByPApellido", query = "SELECT t FROM TblDatosPersonales t WHERE t.pApellido = :pApellido")
    , @NamedQuery(name = "TblDatosPersonales.findBySApellido", query = "SELECT t FROM TblDatosPersonales t WHERE t.sApellido = :sApellido")
    , @NamedQuery(name = "TblDatosPersonales.findByIdiomas", query = "SELECT t FROM TblDatosPersonales t WHERE t.idiomas = :idiomas")
    , @NamedQuery(name = "TblDatosPersonales.findBySexo", query = "SELECT t FROM TblDatosPersonales t WHERE t.sexo = :sexo")
    , @NamedQuery(name = "TblDatosPersonales.findByEdad", query = "SELECT t FROM TblDatosPersonales t WHERE t.edad = :edad")
    , @NamedQuery(name = "TblDatosPersonales.findByNacionalidad", query = "SELECT t FROM TblDatosPersonales t WHERE t.nacionalidad = :nacionalidad")
    , @NamedQuery(name = "TblDatosPersonales.findByIdDocumento", query = "SELECT t FROM TblDatosPersonales t WHERE t.idDocumento = :idDocumento")
    , @NamedQuery(name = "TblDatosPersonales.findByLugarNac", query = "SELECT t FROM TblDatosPersonales t WHERE t.lugarNac = :lugarNac")
    , @NamedQuery(name = "TblDatosPersonales.findByFechaNac", query = "SELECT t FROM TblDatosPersonales t WHERE t.fechaNac = :fechaNac")
    , @NamedQuery(name = "TblDatosPersonales.findByEstadoCivil", query = "SELECT t FROM TblDatosPersonales t WHERE t.estadoCivil = :estadoCivil")
    , @NamedQuery(name = "TblDatosPersonales.findByNumeroHijos", query = "SELECT t FROM TblDatosPersonales t WHERE t.numeroHijos = :numeroHijos")
    , @NamedQuery(name = "TblDatosPersonales.findByLicencia", query = "SELECT t FROM TblDatosPersonales t WHERE t.licencia = :licencia")
    , @NamedQuery(name = "TblDatosPersonales.findByViveCon", query = "SELECT t FROM TblDatosPersonales t WHERE t.viveCon = :viveCon")
    , @NamedQuery(name = "TblDatosPersonales.findByNucleoFamiliar", query = "SELECT t FROM TblDatosPersonales t WHERE t.nucleoFamiliar = :nucleoFamiliar")
    , @NamedQuery(name = "TblDatosPersonales.findByLugarExpedicion", query = "SELECT t FROM TblDatosPersonales t WHERE t.lugarExpedicion = :lugarExpedicion")
    , @NamedQuery(name = "TblDatosPersonales.findByFechaExpedicion", query = "SELECT t FROM TblDatosPersonales t WHERE t.fechaExpedicion = :fechaExpedicion")})
public class TblDatosPersonales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_dat_person")
    private Integer idDatPerson;
    @Basic(optional = false)
    @Column(name = "nombres")
    private String nombres;
    @Basic(optional = false)
    @Column(name = "p_apellido")
    private String pApellido;
    @Basic(optional = false)
    @Column(name = "s_apellido")
    private String sApellido;
    @Column(name = "idiomas")
    private String idiomas;
    @Basic(optional = false)
    @Column(name = "sexo")
    private String sexo;
    @Basic(optional = false)
    @Column(name = "edad")
    private int edad;
    @Basic(optional = false)
    @Column(name = "nacionalidad")
    private String nacionalidad;
    @Basic(optional = false)
    @Column(name = "id_documento")
    private int idDocumento;
    @Basic(optional = false)
    @Column(name = "lugar_nac")
    private String lugarNac;
    @Basic(optional = false)
    @Column(name = "fecha_nac")
    @Temporal(TemporalType.DATE)
    private Date fechaNac;
    @Basic(optional = false)
    @Column(name = "estado_civil")
    private String estadoCivil;
    @Basic(optional = false)
    @Column(name = "numero_hijos")
    private int numeroHijos;
    @Basic(optional = false)
    @Column(name = "licencia")
    private String licencia;
    @Basic(optional = false)
    @Column(name = "vive_con")
    private String viveCon;
    @Basic(optional = false)
    @Column(name = "nucleo_familiar")
    private String nucleoFamiliar;
    @Column(name = "lugar_expedicion")
    private String lugarExpedicion;
    @Column(name = "fecha_expedicion")
    @Temporal(TemporalType.DATE)
    private Date fechaExpedicion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatPerson")
    private Collection<TblDatosAcademicos> tblDatosAcademicosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatPerson")
    private Collection<TblPadres> tblPadresCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatPerson")
    private Collection<TblReferencias> tblReferenciasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatPerson")
    private Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatPerson")
    private Collection<TblDireccion> tblDireccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatosPersonales")
    private Collection<TblDatosEmpleoEstudioActual> tblDatosEmpleoEstudioActualCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatPerson")
    private Collection<TblTelefonos> tblTelefonosCollection;

    public TblDatosPersonales() {
    }

    public TblDatosPersonales(Integer idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    public TblDatosPersonales(Integer idDatPerson, String nombres, String pApellido, String sApellido, String sexo, int edad, String nacionalidad, int idDocumento, String lugarNac, Date fechaNac, String estadoCivil, int numeroHijos, String licencia, String viveCon, String nucleoFamiliar) {
        this.idDatPerson = idDatPerson;
        this.nombres = nombres;
        this.pApellido = pApellido;
        this.sApellido = sApellido;
        this.sexo = sexo;
        this.edad = edad;
        this.nacionalidad = nacionalidad;
        this.idDocumento = idDocumento;
        this.lugarNac = lugarNac;
        this.fechaNac = fechaNac;
        this.estadoCivil = estadoCivil;
        this.numeroHijos = numeroHijos;
        this.licencia = licencia;
        this.viveCon = viveCon;
        this.nucleoFamiliar = nucleoFamiliar;
    }

    public Integer getIdDatPerson() {
        return idDatPerson;
    }

    public void setIdDatPerson(Integer idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPApellido() {
        return pApellido;
    }

    public void setPApellido(String pApellido) {
        this.pApellido = pApellido;
    }

    public String getSApellido() {
        return sApellido;
    }

    public void setSApellido(String sApellido) {
        this.sApellido = sApellido;
    }

    public String getIdiomas() {
        return idiomas;
    }

    public void setIdiomas(String idiomas) {
        this.idiomas = idiomas;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getLugarNac() {
        return lugarNac;
    }

    public void setLugarNac(String lugarNac) {
        this.lugarNac = lugarNac;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public int getNumeroHijos() {
        return numeroHijos;
    }

    public void setNumeroHijos(int numeroHijos) {
        this.numeroHijos = numeroHijos;
    }

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }

    public String getViveCon() {
        return viveCon;
    }

    public void setViveCon(String viveCon) {
        this.viveCon = viveCon;
    }

    public String getNucleoFamiliar() {
        return nucleoFamiliar;
    }

    public void setNucleoFamiliar(String nucleoFamiliar) {
        this.nucleoFamiliar = nucleoFamiliar;
    }

    public String getLugarExpedicion() {
        return lugarExpedicion;
    }

    public void setLugarExpedicion(String lugarExpedicion) {
        this.lugarExpedicion = lugarExpedicion;
    }

    public Date getFechaExpedicion() {
        return fechaExpedicion;
    }

    public void setFechaExpedicion(Date fechaExpedicion) {
        this.fechaExpedicion = fechaExpedicion;
    }

    @XmlTransient
    public Collection<TblDatosAcademicos> getTblDatosAcademicosCollection() {
        return tblDatosAcademicosCollection;
    }

    public void setTblDatosAcademicosCollection(Collection<TblDatosAcademicos> tblDatosAcademicosCollection) {
        this.tblDatosAcademicosCollection = tblDatosAcademicosCollection;
    }

    @XmlTransient
    public Collection<TblPadres> getTblPadresCollection() {
        return tblPadresCollection;
    }

    public void setTblPadresCollection(Collection<TblPadres> tblPadresCollection) {
        this.tblPadresCollection = tblPadresCollection;
    }

    @XmlTransient
    public Collection<TblReferencias> getTblReferenciasCollection() {
        return tblReferenciasCollection;
    }

    public void setTblReferenciasCollection(Collection<TblReferencias> tblReferenciasCollection) {
        this.tblReferenciasCollection = tblReferenciasCollection;
    }

    @XmlTransient
    public Collection<TblSolicitudIngreso> getTblSolicitudIngresoCollection() {
        return tblSolicitudIngresoCollection;
    }

    public void setTblSolicitudIngresoCollection(Collection<TblSolicitudIngreso> tblSolicitudIngresoCollection) {
        this.tblSolicitudIngresoCollection = tblSolicitudIngresoCollection;
    }

    @XmlTransient
    public Collection<TblDireccion> getTblDireccionCollection() {
        return tblDireccionCollection;
    }

    public void setTblDireccionCollection(Collection<TblDireccion> tblDireccionCollection) {
        this.tblDireccionCollection = tblDireccionCollection;
    }

    @XmlTransient
    public Collection<TblDatosEmpleoEstudioActual> getTblDatosEmpleoEstudioActualCollection() {
        return tblDatosEmpleoEstudioActualCollection;
    }

    public void setTblDatosEmpleoEstudioActualCollection(Collection<TblDatosEmpleoEstudioActual> tblDatosEmpleoEstudioActualCollection) {
        this.tblDatosEmpleoEstudioActualCollection = tblDatosEmpleoEstudioActualCollection;
    }

    @XmlTransient
    public Collection<TblTelefonos> getTblTelefonosCollection() {
        return tblTelefonosCollection;
    }

    public void setTblTelefonosCollection(Collection<TblTelefonos> tblTelefonosCollection) {
        this.tblTelefonosCollection = tblTelefonosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDatPerson != null ? idDatPerson.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblDatosPersonales)) {
            return false;
        }
        TblDatosPersonales other = (TblDatosPersonales) object;
        if ((this.idDatPerson == null && other.idDatPerson != null) || (this.idDatPerson != null && !this.idDatPerson.equals(other.idDatPerson))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblDatosPersonales[ idDatPerson=" + idDatPerson + " ]";
    }
    
}
