/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_solicitud_afiliacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblSolicitudAfiliacion.findAll", query = "SELECT t FROM TblSolicitudAfiliacion t")
    , @NamedQuery(name = "TblSolicitudAfiliacion.findByIdSolicitudAfiliacion", query = "SELECT t FROM TblSolicitudAfiliacion t WHERE t.idSolicitudAfiliacion = :idSolicitudAfiliacion")
    , @NamedQuery(name = "TblSolicitudAfiliacion.findByCarrerasAutomovil", query = "SELECT t FROM TblSolicitudAfiliacion t WHERE t.carrerasAutomovil = :carrerasAutomovil")
    , @NamedQuery(name = "TblSolicitudAfiliacion.findByAviacionPrivada", query = "SELECT t FROM TblSolicitudAfiliacion t WHERE t.aviacionPrivada = :aviacionPrivada")
    , @NamedQuery(name = "TblSolicitudAfiliacion.findByMotocicliasmo", query = "SELECT t FROM TblSolicitudAfiliacion t WHERE t.motocicliasmo = :motocicliasmo")})
public class TblSolicitudAfiliacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_solicitud_afiliacion")
    private Integer idSolicitudAfiliacion;
    @Column(name = "carreras_automovil")
    private Boolean carrerasAutomovil;
    @Column(name = "aviacion_privada")
    private Boolean aviacionPrivada;
    @Basic(optional = false)
    @Column(name = "motocicliasmo")
    private boolean motocicliasmo;
    @Lob
    @Column(name = "otros")
    private String otros;
    @JoinColumn(name = "id_registro", referencedColumnName = "id_registro")
    @ManyToOne(optional = false)
    private TblRegistro idRegistro;

    public TblSolicitudAfiliacion() {
    }

    public TblSolicitudAfiliacion(Integer idSolicitudAfiliacion) {
        this.idSolicitudAfiliacion = idSolicitudAfiliacion;
    }

    public TblSolicitudAfiliacion(Integer idSolicitudAfiliacion, boolean motocicliasmo) {
        this.idSolicitudAfiliacion = idSolicitudAfiliacion;
        this.motocicliasmo = motocicliasmo;
    }

    public Integer getIdSolicitudAfiliacion() {
        return idSolicitudAfiliacion;
    }

    public void setIdSolicitudAfiliacion(Integer idSolicitudAfiliacion) {
        this.idSolicitudAfiliacion = idSolicitudAfiliacion;
    }

    public Boolean getCarrerasAutomovil() {
        return carrerasAutomovil;
    }

    public void setCarrerasAutomovil(Boolean carrerasAutomovil) {
        this.carrerasAutomovil = carrerasAutomovil;
    }

    public Boolean getAviacionPrivada() {
        return aviacionPrivada;
    }

    public void setAviacionPrivada(Boolean aviacionPrivada) {
        this.aviacionPrivada = aviacionPrivada;
    }

    public boolean getMotocicliasmo() {
        return motocicliasmo;
    }

    public void setMotocicliasmo(boolean motocicliasmo) {
        this.motocicliasmo = motocicliasmo;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public TblRegistro getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(TblRegistro idRegistro) {
        this.idRegistro = idRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSolicitudAfiliacion != null ? idSolicitudAfiliacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblSolicitudAfiliacion)) {
            return false;
        }
        TblSolicitudAfiliacion other = (TblSolicitudAfiliacion) object;
        if ((this.idSolicitudAfiliacion == null && other.idSolicitudAfiliacion != null) || (this.idSolicitudAfiliacion != null && !this.idSolicitudAfiliacion.equals(other.idSolicitudAfiliacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblSolicitudAfiliacion[ idSolicitudAfiliacion=" + idSolicitudAfiliacion + " ]";
    }
    
}
