/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_departamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblDepartamento.findAll", query = "SELECT t FROM TblDepartamento t")
    , @NamedQuery(name = "TblDepartamento.findByIdDepto", query = "SELECT t FROM TblDepartamento t WHERE t.idDepto = :idDepto")
    , @NamedQuery(name = "TblDepartamento.findByNombreDepto", query = "SELECT t FROM TblDepartamento t WHERE t.nombreDepto = :nombreDepto")})
public class TblDepartamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_depto")
    private Integer idDepto;
    @Basic(optional = false)
    @Column(name = "nombre_depto")
    private String nombreDepto;
    @JoinColumn(name = "id_municipio", referencedColumnName = "id_municipio")
    @ManyToOne(optional = false)
    private TblMunicipio idMunicipio;

    public TblDepartamento() {
    }

    public TblDepartamento(Integer idDepto) {
        this.idDepto = idDepto;
    }

    public TblDepartamento(Integer idDepto, String nombreDepto) {
        this.idDepto = idDepto;
        this.nombreDepto = nombreDepto;
    }

    public Integer getIdDepto() {
        return idDepto;
    }

    public void setIdDepto(Integer idDepto) {
        this.idDepto = idDepto;
    }

    public String getNombreDepto() {
        return nombreDepto;
    }

    public void setNombreDepto(String nombreDepto) {
        this.nombreDepto = nombreDepto;
    }

    public TblMunicipio getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(TblMunicipio idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDepto != null ? idDepto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblDepartamento)) {
            return false;
        }
        TblDepartamento other = (TblDepartamento) object;
        if ((this.idDepto == null && other.idDepto != null) || (this.idDepto != null && !this.idDepto.equals(other.idDepto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblDepartamento[ idDepto=" + idDepto + " ]";
    }
    
}
