/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCIA;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tbl_telefonos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblTelefonos.findAll", query = "SELECT t FROM TblTelefonos t")
    , @NamedQuery(name = "TblTelefonos.findByIdTelefono", query = "SELECT t FROM TblTelefonos t WHERE t.idTelefono = :idTelefono")
    , @NamedQuery(name = "TblTelefonos.findByTipo", query = "SELECT t FROM TblTelefonos t WHERE t.tipo = :tipo")
    , @NamedQuery(name = "TblTelefonos.findByNumeroTelefono", query = "SELECT t FROM TblTelefonos t WHERE t.numeroTelefono = :numeroTelefono")})
public class TblTelefonos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_telefono")
    private Integer idTelefono;
    @Basic(optional = false)
    @Column(name = "tipo")
    private String tipo;
    @Basic(optional = false)
    @Column(name = "numero_telefono")
    private String numeroTelefono;
    @JoinColumn(name = "id_dat_person", referencedColumnName = "id_dat_person")
    @ManyToOne(optional = false)
    private TblDatosPersonales idDatPerson;

    public TblTelefonos() {
    }

    public TblTelefonos(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public TblTelefonos(Integer idTelefono, String tipo, String numeroTelefono) {
        this.idTelefono = idTelefono;
        this.tipo = tipo;
        this.numeroTelefono = numeroTelefono;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public TblDatosPersonales getIdDatPerson() {
        return idDatPerson;
    }

    public void setIdDatPerson(TblDatosPersonales idDatPerson) {
        this.idDatPerson = idDatPerson;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTelefono != null ? idTelefono.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblTelefonos)) {
            return false;
        }
        TblTelefonos other = (TblTelefonos) object;
        if ((this.idTelefono == null && other.idTelefono != null) || (this.idTelefono != null && !this.idTelefono.equals(other.idTelefono))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PERSISTENCIA.TblTelefonos[ idTelefono=" + idTelefono + " ]";
    }
    
}
