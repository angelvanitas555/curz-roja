/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblRegistro;
import PERSISTENCIA.TblHistorialCargo;
import PERSISTENCIA.TblRegistroHistorial;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblRegistroHistorialJpaController implements Serializable {

    public TblRegistroHistorialJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblRegistroHistorial tblRegistroHistorial) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistro idRegistro = tblRegistroHistorial.getIdRegistro();
            if (idRegistro != null) {
                idRegistro = em.getReference(idRegistro.getClass(), idRegistro.getIdRegistro());
                tblRegistroHistorial.setIdRegistro(idRegistro);
            }
            TblHistorialCargo idHistorialCargo = tblRegistroHistorial.getIdHistorialCargo();
            if (idHistorialCargo != null) {
                idHistorialCargo = em.getReference(idHistorialCargo.getClass(), idHistorialCargo.getIdHistorialCargo());
                tblRegistroHistorial.setIdHistorialCargo(idHistorialCargo);
            }
            em.persist(tblRegistroHistorial);
            if (idRegistro != null) {
                idRegistro.getTblRegistroHistorialCollection().add(tblRegistroHistorial);
                idRegistro = em.merge(idRegistro);
            }
            if (idHistorialCargo != null) {
                idHistorialCargo.getTblRegistroHistorialCollection().add(tblRegistroHistorial);
                idHistorialCargo = em.merge(idHistorialCargo);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblRegistroHistorial tblRegistroHistorial) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistroHistorial persistentTblRegistroHistorial = em.find(TblRegistroHistorial.class, tblRegistroHistorial.getIdRegistroHistorial());
            TblRegistro idRegistroOld = persistentTblRegistroHistorial.getIdRegistro();
            TblRegistro idRegistroNew = tblRegistroHistorial.getIdRegistro();
            TblHistorialCargo idHistorialCargoOld = persistentTblRegistroHistorial.getIdHistorialCargo();
            TblHistorialCargo idHistorialCargoNew = tblRegistroHistorial.getIdHistorialCargo();
            if (idRegistroNew != null) {
                idRegistroNew = em.getReference(idRegistroNew.getClass(), idRegistroNew.getIdRegistro());
                tblRegistroHistorial.setIdRegistro(idRegistroNew);
            }
            if (idHistorialCargoNew != null) {
                idHistorialCargoNew = em.getReference(idHistorialCargoNew.getClass(), idHistorialCargoNew.getIdHistorialCargo());
                tblRegistroHistorial.setIdHistorialCargo(idHistorialCargoNew);
            }
            tblRegistroHistorial = em.merge(tblRegistroHistorial);
            if (idRegistroOld != null && !idRegistroOld.equals(idRegistroNew)) {
                idRegistroOld.getTblRegistroHistorialCollection().remove(tblRegistroHistorial);
                idRegistroOld = em.merge(idRegistroOld);
            }
            if (idRegistroNew != null && !idRegistroNew.equals(idRegistroOld)) {
                idRegistroNew.getTblRegistroHistorialCollection().add(tblRegistroHistorial);
                idRegistroNew = em.merge(idRegistroNew);
            }
            if (idHistorialCargoOld != null && !idHistorialCargoOld.equals(idHistorialCargoNew)) {
                idHistorialCargoOld.getTblRegistroHistorialCollection().remove(tblRegistroHistorial);
                idHistorialCargoOld = em.merge(idHistorialCargoOld);
            }
            if (idHistorialCargoNew != null && !idHistorialCargoNew.equals(idHistorialCargoOld)) {
                idHistorialCargoNew.getTblRegistroHistorialCollection().add(tblRegistroHistorial);
                idHistorialCargoNew = em.merge(idHistorialCargoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblRegistroHistorial.getIdRegistroHistorial();
                if (findTblRegistroHistorial(id) == null) {
                    throw new NonexistentEntityException("The tblRegistroHistorial with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistroHistorial tblRegistroHistorial;
            try {
                tblRegistroHistorial = em.getReference(TblRegistroHistorial.class, id);
                tblRegistroHistorial.getIdRegistroHistorial();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblRegistroHistorial with id " + id + " no longer exists.", enfe);
            }
            TblRegistro idRegistro = tblRegistroHistorial.getIdRegistro();
            if (idRegistro != null) {
                idRegistro.getTblRegistroHistorialCollection().remove(tblRegistroHistorial);
                idRegistro = em.merge(idRegistro);
            }
            TblHistorialCargo idHistorialCargo = tblRegistroHistorial.getIdHistorialCargo();
            if (idHistorialCargo != null) {
                idHistorialCargo.getTblRegistroHistorialCollection().remove(tblRegistroHistorial);
                idHistorialCargo = em.merge(idHistorialCargo);
            }
            em.remove(tblRegistroHistorial);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblRegistroHistorial> findTblRegistroHistorialEntities() {
        return findTblRegistroHistorialEntities(true, -1, -1);
    }

    public List<TblRegistroHistorial> findTblRegistroHistorialEntities(int maxResults, int firstResult) {
        return findTblRegistroHistorialEntities(false, maxResults, firstResult);
    }

    private List<TblRegistroHistorial> findTblRegistroHistorialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblRegistroHistorial.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblRegistroHistorial findTblRegistroHistorial(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblRegistroHistorial.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblRegistroHistorialCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblRegistroHistorial> rt = cq.from(TblRegistroHistorial.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
