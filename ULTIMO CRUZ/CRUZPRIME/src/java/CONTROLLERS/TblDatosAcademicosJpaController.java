/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.NonexistentEntityException;
import PERSISTENCIA.TblDatosAcademicos;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblDatosPersonales;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblDatosAcademicosJpaController implements Serializable {

    public TblDatosAcademicosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblDatosAcademicos tblDatosAcademicos) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosPersonales idDatPerson = tblDatosAcademicos.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson = em.getReference(idDatPerson.getClass(), idDatPerson.getIdDatPerson());
                tblDatosAcademicos.setIdDatPerson(idDatPerson);
            }
            em.persist(tblDatosAcademicos);
            if (idDatPerson != null) {
                idDatPerson.getTblDatosAcademicosCollection().add(tblDatosAcademicos);
                idDatPerson = em.merge(idDatPerson);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblDatosAcademicos tblDatosAcademicos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosAcademicos persistentTblDatosAcademicos = em.find(TblDatosAcademicos.class, tblDatosAcademicos.getIdDatosAcademicos());
            TblDatosPersonales idDatPersonOld = persistentTblDatosAcademicos.getIdDatPerson();
            TblDatosPersonales idDatPersonNew = tblDatosAcademicos.getIdDatPerson();
            if (idDatPersonNew != null) {
                idDatPersonNew = em.getReference(idDatPersonNew.getClass(), idDatPersonNew.getIdDatPerson());
                tblDatosAcademicos.setIdDatPerson(idDatPersonNew);
            }
            tblDatosAcademicos = em.merge(tblDatosAcademicos);
            if (idDatPersonOld != null && !idDatPersonOld.equals(idDatPersonNew)) {
                idDatPersonOld.getTblDatosAcademicosCollection().remove(tblDatosAcademicos);
                idDatPersonOld = em.merge(idDatPersonOld);
            }
            if (idDatPersonNew != null && !idDatPersonNew.equals(idDatPersonOld)) {
                idDatPersonNew.getTblDatosAcademicosCollection().add(tblDatosAcademicos);
                idDatPersonNew = em.merge(idDatPersonNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblDatosAcademicos.getIdDatosAcademicos();
                if (findTblDatosAcademicos(id) == null) {
                    throw new NonexistentEntityException("The tblDatosAcademicos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosAcademicos tblDatosAcademicos;
            try {
                tblDatosAcademicos = em.getReference(TblDatosAcademicos.class, id);
                tblDatosAcademicos.getIdDatosAcademicos();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblDatosAcademicos with id " + id + " no longer exists.", enfe);
            }
            TblDatosPersonales idDatPerson = tblDatosAcademicos.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson.getTblDatosAcademicosCollection().remove(tblDatosAcademicos);
                idDatPerson = em.merge(idDatPerson);
            }
            em.remove(tblDatosAcademicos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblDatosAcademicos> findTblDatosAcademicosEntities() {
        return findTblDatosAcademicosEntities(true, -1, -1);
    }

    public List<TblDatosAcademicos> findTblDatosAcademicosEntities(int maxResults, int firstResult) {
        return findTblDatosAcademicosEntities(false, maxResults, firstResult);
    }

    private List<TblDatosAcademicos> findTblDatosAcademicosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblDatosAcademicos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblDatosAcademicos findTblDatosAcademicos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblDatosAcademicos.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblDatosAcademicosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblDatosAcademicos> rt = cq.from(TblDatosAcademicos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
