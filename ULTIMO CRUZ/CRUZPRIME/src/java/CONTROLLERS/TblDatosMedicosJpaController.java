/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import PERSISTENCIA.TblDatosMedicos;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblSolicitudIngreso;
import PERSISTENCIA.TblDireccion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblDatosMedicosJpaController implements Serializable {

    public TblDatosMedicosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblDatosMedicos tblDatosMedicos) {
        if (tblDatosMedicos.getTblDireccionCollection() == null) {
            tblDatosMedicos.setTblDireccionCollection(new ArrayList<TblDireccion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSolicitudIngreso idSolicitud = tblDatosMedicos.getIdSolicitud();
            if (idSolicitud != null) {
                idSolicitud = em.getReference(idSolicitud.getClass(), idSolicitud.getIdSolicitud());
                tblDatosMedicos.setIdSolicitud(idSolicitud);
            }
            Collection<TblDireccion> attachedTblDireccionCollection = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionTblDireccionToAttach : tblDatosMedicos.getTblDireccionCollection()) {
                tblDireccionCollectionTblDireccionToAttach = em.getReference(tblDireccionCollectionTblDireccionToAttach.getClass(), tblDireccionCollectionTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollection.add(tblDireccionCollectionTblDireccionToAttach);
            }
            tblDatosMedicos.setTblDireccionCollection(attachedTblDireccionCollection);
            em.persist(tblDatosMedicos);
            if (idSolicitud != null) {
                idSolicitud.getTblDatosMedicosCollection().add(tblDatosMedicos);
                idSolicitud = em.merge(idSolicitud);
            }
            for (TblDireccion tblDireccionCollectionTblDireccion : tblDatosMedicos.getTblDireccionCollection()) {
                TblDatosMedicos oldIdDatosMedicosOfTblDireccionCollectionTblDireccion = tblDireccionCollectionTblDireccion.getIdDatosMedicos();
                tblDireccionCollectionTblDireccion.setIdDatosMedicos(tblDatosMedicos);
                tblDireccionCollectionTblDireccion = em.merge(tblDireccionCollectionTblDireccion);
                if (oldIdDatosMedicosOfTblDireccionCollectionTblDireccion != null) {
                    oldIdDatosMedicosOfTblDireccionCollectionTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionTblDireccion);
                    oldIdDatosMedicosOfTblDireccionCollectionTblDireccion = em.merge(oldIdDatosMedicosOfTblDireccionCollectionTblDireccion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblDatosMedicos tblDatosMedicos) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosMedicos persistentTblDatosMedicos = em.find(TblDatosMedicos.class, tblDatosMedicos.getIdDatosMedicos());
            TblSolicitudIngreso idSolicitudOld = persistentTblDatosMedicos.getIdSolicitud();
            TblSolicitudIngreso idSolicitudNew = tblDatosMedicos.getIdSolicitud();
            Collection<TblDireccion> tblDireccionCollectionOld = persistentTblDatosMedicos.getTblDireccionCollection();
            Collection<TblDireccion> tblDireccionCollectionNew = tblDatosMedicos.getTblDireccionCollection();
            List<String> illegalOrphanMessages = null;
            for (TblDireccion tblDireccionCollectionOldTblDireccion : tblDireccionCollectionOld) {
                if (!tblDireccionCollectionNew.contains(tblDireccionCollectionOldTblDireccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDireccion " + tblDireccionCollectionOldTblDireccion + " since its idDatosMedicos field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idSolicitudNew != null) {
                idSolicitudNew = em.getReference(idSolicitudNew.getClass(), idSolicitudNew.getIdSolicitud());
                tblDatosMedicos.setIdSolicitud(idSolicitudNew);
            }
            Collection<TblDireccion> attachedTblDireccionCollectionNew = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionNewTblDireccionToAttach : tblDireccionCollectionNew) {
                tblDireccionCollectionNewTblDireccionToAttach = em.getReference(tblDireccionCollectionNewTblDireccionToAttach.getClass(), tblDireccionCollectionNewTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollectionNew.add(tblDireccionCollectionNewTblDireccionToAttach);
            }
            tblDireccionCollectionNew = attachedTblDireccionCollectionNew;
            tblDatosMedicos.setTblDireccionCollection(tblDireccionCollectionNew);
            tblDatosMedicos = em.merge(tblDatosMedicos);
            if (idSolicitudOld != null && !idSolicitudOld.equals(idSolicitudNew)) {
                idSolicitudOld.getTblDatosMedicosCollection().remove(tblDatosMedicos);
                idSolicitudOld = em.merge(idSolicitudOld);
            }
            if (idSolicitudNew != null && !idSolicitudNew.equals(idSolicitudOld)) {
                idSolicitudNew.getTblDatosMedicosCollection().add(tblDatosMedicos);
                idSolicitudNew = em.merge(idSolicitudNew);
            }
            for (TblDireccion tblDireccionCollectionNewTblDireccion : tblDireccionCollectionNew) {
                if (!tblDireccionCollectionOld.contains(tblDireccionCollectionNewTblDireccion)) {
                    TblDatosMedicos oldIdDatosMedicosOfTblDireccionCollectionNewTblDireccion = tblDireccionCollectionNewTblDireccion.getIdDatosMedicos();
                    tblDireccionCollectionNewTblDireccion.setIdDatosMedicos(tblDatosMedicos);
                    tblDireccionCollectionNewTblDireccion = em.merge(tblDireccionCollectionNewTblDireccion);
                    if (oldIdDatosMedicosOfTblDireccionCollectionNewTblDireccion != null && !oldIdDatosMedicosOfTblDireccionCollectionNewTblDireccion.equals(tblDatosMedicos)) {
                        oldIdDatosMedicosOfTblDireccionCollectionNewTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionNewTblDireccion);
                        oldIdDatosMedicosOfTblDireccionCollectionNewTblDireccion = em.merge(oldIdDatosMedicosOfTblDireccionCollectionNewTblDireccion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblDatosMedicos.getIdDatosMedicos();
                if (findTblDatosMedicos(id) == null) {
                    throw new NonexistentEntityException("The tblDatosMedicos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosMedicos tblDatosMedicos;
            try {
                tblDatosMedicos = em.getReference(TblDatosMedicos.class, id);
                tblDatosMedicos.getIdDatosMedicos();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblDatosMedicos with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblDireccion> tblDireccionCollectionOrphanCheck = tblDatosMedicos.getTblDireccionCollection();
            for (TblDireccion tblDireccionCollectionOrphanCheckTblDireccion : tblDireccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblDatosMedicos (" + tblDatosMedicos + ") cannot be destroyed since the TblDireccion " + tblDireccionCollectionOrphanCheckTblDireccion + " in its tblDireccionCollection field has a non-nullable idDatosMedicos field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TblSolicitudIngreso idSolicitud = tblDatosMedicos.getIdSolicitud();
            if (idSolicitud != null) {
                idSolicitud.getTblDatosMedicosCollection().remove(tblDatosMedicos);
                idSolicitud = em.merge(idSolicitud);
            }
            em.remove(tblDatosMedicos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblDatosMedicos> findTblDatosMedicosEntities() {
        return findTblDatosMedicosEntities(true, -1, -1);
    }

    public List<TblDatosMedicos> findTblDatosMedicosEntities(int maxResults, int firstResult) {
        return findTblDatosMedicosEntities(false, maxResults, firstResult);
    }

    private List<TblDatosMedicos> findTblDatosMedicosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblDatosMedicos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblDatosMedicos findTblDatosMedicos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblDatosMedicos.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblDatosMedicosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblDatosMedicos> rt = cq.from(TblDatosMedicos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
