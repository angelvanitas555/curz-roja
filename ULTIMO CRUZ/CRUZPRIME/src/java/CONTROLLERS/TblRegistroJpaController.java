/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblArea;
import PERSISTENCIA.TblRegistro;
import PERSISTENCIA.TblSolicitudIngreso;
import PERSISTENCIA.TblRegistroCapacidad;
import java.util.ArrayList;
import java.util.Collection;
import PERSISTENCIA.TblRegistroHistorial;
import PERSISTENCIA.TblSolicitudAfiliacion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblRegistroJpaController implements Serializable {

    public TblRegistroJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblRegistro tblRegistro) {
        if (tblRegistro.getTblRegistroCapacidadCollection() == null) {
            tblRegistro.setTblRegistroCapacidadCollection(new ArrayList<TblRegistroCapacidad>());
        }
        if (tblRegistro.getTblRegistroHistorialCollection() == null) {
            tblRegistro.setTblRegistroHistorialCollection(new ArrayList<TblRegistroHistorial>());
        }
        if (tblRegistro.getTblSolicitudAfiliacionCollection() == null) {
            tblRegistro.setTblSolicitudAfiliacionCollection(new ArrayList<TblSolicitudAfiliacion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblArea idArea = tblRegistro.getIdArea();
            if (idArea != null) {
                idArea = em.getReference(idArea.getClass(), idArea.getIdArea());
                tblRegistro.setIdArea(idArea);
            }
            TblSolicitudIngreso idSolicitud = tblRegistro.getIdSolicitud();
            if (idSolicitud != null) {
                idSolicitud = em.getReference(idSolicitud.getClass(), idSolicitud.getIdSolicitud());
                tblRegistro.setIdSolicitud(idSolicitud);
            }
            Collection<TblRegistroCapacidad> attachedTblRegistroCapacidadCollection = new ArrayList<TblRegistroCapacidad>();
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach : tblRegistro.getTblRegistroCapacidadCollection()) {
                tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach = em.getReference(tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach.getClass(), tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach.getIdRegistroCapacidad());
                attachedTblRegistroCapacidadCollection.add(tblRegistroCapacidadCollectionTblRegistroCapacidadToAttach);
            }
            tblRegistro.setTblRegistroCapacidadCollection(attachedTblRegistroCapacidadCollection);
            Collection<TblRegistroHistorial> attachedTblRegistroHistorialCollection = new ArrayList<TblRegistroHistorial>();
            for (TblRegistroHistorial tblRegistroHistorialCollectionTblRegistroHistorialToAttach : tblRegistro.getTblRegistroHistorialCollection()) {
                tblRegistroHistorialCollectionTblRegistroHistorialToAttach = em.getReference(tblRegistroHistorialCollectionTblRegistroHistorialToAttach.getClass(), tblRegistroHistorialCollectionTblRegistroHistorialToAttach.getIdRegistroHistorial());
                attachedTblRegistroHistorialCollection.add(tblRegistroHistorialCollectionTblRegistroHistorialToAttach);
            }
            tblRegistro.setTblRegistroHistorialCollection(attachedTblRegistroHistorialCollection);
            Collection<TblSolicitudAfiliacion> attachedTblSolicitudAfiliacionCollection = new ArrayList<TblSolicitudAfiliacion>();
            for (TblSolicitudAfiliacion tblSolicitudAfiliacionCollectionTblSolicitudAfiliacionToAttach : tblRegistro.getTblSolicitudAfiliacionCollection()) {
                tblSolicitudAfiliacionCollectionTblSolicitudAfiliacionToAttach = em.getReference(tblSolicitudAfiliacionCollectionTblSolicitudAfiliacionToAttach.getClass(), tblSolicitudAfiliacionCollectionTblSolicitudAfiliacionToAttach.getIdSolicitudAfiliacion());
                attachedTblSolicitudAfiliacionCollection.add(tblSolicitudAfiliacionCollectionTblSolicitudAfiliacionToAttach);
            }
            tblRegistro.setTblSolicitudAfiliacionCollection(attachedTblSolicitudAfiliacionCollection);
            em.persist(tblRegistro);
            if (idArea != null) {
                idArea.getTblRegistroCollection().add(tblRegistro);
                idArea = em.merge(idArea);
            }
            if (idSolicitud != null) {
                idSolicitud.getTblRegistroCollection().add(tblRegistro);
                idSolicitud = em.merge(idSolicitud);
            }
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionTblRegistroCapacidad : tblRegistro.getTblRegistroCapacidadCollection()) {
                TblRegistro oldIdRegistroOfTblRegistroCapacidadCollectionTblRegistroCapacidad = tblRegistroCapacidadCollectionTblRegistroCapacidad.getIdRegistro();
                tblRegistroCapacidadCollectionTblRegistroCapacidad.setIdRegistro(tblRegistro);
                tblRegistroCapacidadCollectionTblRegistroCapacidad = em.merge(tblRegistroCapacidadCollectionTblRegistroCapacidad);
                if (oldIdRegistroOfTblRegistroCapacidadCollectionTblRegistroCapacidad != null) {
                    oldIdRegistroOfTblRegistroCapacidadCollectionTblRegistroCapacidad.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidadCollectionTblRegistroCapacidad);
                    oldIdRegistroOfTblRegistroCapacidadCollectionTblRegistroCapacidad = em.merge(oldIdRegistroOfTblRegistroCapacidadCollectionTblRegistroCapacidad);
                }
            }
            for (TblRegistroHistorial tblRegistroHistorialCollectionTblRegistroHistorial : tblRegistro.getTblRegistroHistorialCollection()) {
                TblRegistro oldIdRegistroOfTblRegistroHistorialCollectionTblRegistroHistorial = tblRegistroHistorialCollectionTblRegistroHistorial.getIdRegistro();
                tblRegistroHistorialCollectionTblRegistroHistorial.setIdRegistro(tblRegistro);
                tblRegistroHistorialCollectionTblRegistroHistorial = em.merge(tblRegistroHistorialCollectionTblRegistroHistorial);
                if (oldIdRegistroOfTblRegistroHistorialCollectionTblRegistroHistorial != null) {
                    oldIdRegistroOfTblRegistroHistorialCollectionTblRegistroHistorial.getTblRegistroHistorialCollection().remove(tblRegistroHistorialCollectionTblRegistroHistorial);
                    oldIdRegistroOfTblRegistroHistorialCollectionTblRegistroHistorial = em.merge(oldIdRegistroOfTblRegistroHistorialCollectionTblRegistroHistorial);
                }
            }
            for (TblSolicitudAfiliacion tblSolicitudAfiliacionCollectionTblSolicitudAfiliacion : tblRegistro.getTblSolicitudAfiliacionCollection()) {
                TblRegistro oldIdRegistroOfTblSolicitudAfiliacionCollectionTblSolicitudAfiliacion = tblSolicitudAfiliacionCollectionTblSolicitudAfiliacion.getIdRegistro();
                tblSolicitudAfiliacionCollectionTblSolicitudAfiliacion.setIdRegistro(tblRegistro);
                tblSolicitudAfiliacionCollectionTblSolicitudAfiliacion = em.merge(tblSolicitudAfiliacionCollectionTblSolicitudAfiliacion);
                if (oldIdRegistroOfTblSolicitudAfiliacionCollectionTblSolicitudAfiliacion != null) {
                    oldIdRegistroOfTblSolicitudAfiliacionCollectionTblSolicitudAfiliacion.getTblSolicitudAfiliacionCollection().remove(tblSolicitudAfiliacionCollectionTblSolicitudAfiliacion);
                    oldIdRegistroOfTblSolicitudAfiliacionCollectionTblSolicitudAfiliacion = em.merge(oldIdRegistroOfTblSolicitudAfiliacionCollectionTblSolicitudAfiliacion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblRegistro tblRegistro) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistro persistentTblRegistro = em.find(TblRegistro.class, tblRegistro.getIdRegistro());
            TblArea idAreaOld = persistentTblRegistro.getIdArea();
            TblArea idAreaNew = tblRegistro.getIdArea();
            TblSolicitudIngreso idSolicitudOld = persistentTblRegistro.getIdSolicitud();
            TblSolicitudIngreso idSolicitudNew = tblRegistro.getIdSolicitud();
            Collection<TblRegistroCapacidad> tblRegistroCapacidadCollectionOld = persistentTblRegistro.getTblRegistroCapacidadCollection();
            Collection<TblRegistroCapacidad> tblRegistroCapacidadCollectionNew = tblRegistro.getTblRegistroCapacidadCollection();
            Collection<TblRegistroHistorial> tblRegistroHistorialCollectionOld = persistentTblRegistro.getTblRegistroHistorialCollection();
            Collection<TblRegistroHistorial> tblRegistroHistorialCollectionNew = tblRegistro.getTblRegistroHistorialCollection();
            Collection<TblSolicitudAfiliacion> tblSolicitudAfiliacionCollectionOld = persistentTblRegistro.getTblSolicitudAfiliacionCollection();
            Collection<TblSolicitudAfiliacion> tblSolicitudAfiliacionCollectionNew = tblRegistro.getTblSolicitudAfiliacionCollection();
            List<String> illegalOrphanMessages = null;
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionOldTblRegistroCapacidad : tblRegistroCapacidadCollectionOld) {
                if (!tblRegistroCapacidadCollectionNew.contains(tblRegistroCapacidadCollectionOldTblRegistroCapacidad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblRegistroCapacidad " + tblRegistroCapacidadCollectionOldTblRegistroCapacidad + " since its idRegistro field is not nullable.");
                }
            }
            for (TblRegistroHistorial tblRegistroHistorialCollectionOldTblRegistroHistorial : tblRegistroHistorialCollectionOld) {
                if (!tblRegistroHistorialCollectionNew.contains(tblRegistroHistorialCollectionOldTblRegistroHistorial)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblRegistroHistorial " + tblRegistroHistorialCollectionOldTblRegistroHistorial + " since its idRegistro field is not nullable.");
                }
            }
            for (TblSolicitudAfiliacion tblSolicitudAfiliacionCollectionOldTblSolicitudAfiliacion : tblSolicitudAfiliacionCollectionOld) {
                if (!tblSolicitudAfiliacionCollectionNew.contains(tblSolicitudAfiliacionCollectionOldTblSolicitudAfiliacion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblSolicitudAfiliacion " + tblSolicitudAfiliacionCollectionOldTblSolicitudAfiliacion + " since its idRegistro field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idAreaNew != null) {
                idAreaNew = em.getReference(idAreaNew.getClass(), idAreaNew.getIdArea());
                tblRegistro.setIdArea(idAreaNew);
            }
            if (idSolicitudNew != null) {
                idSolicitudNew = em.getReference(idSolicitudNew.getClass(), idSolicitudNew.getIdSolicitud());
                tblRegistro.setIdSolicitud(idSolicitudNew);
            }
            Collection<TblRegistroCapacidad> attachedTblRegistroCapacidadCollectionNew = new ArrayList<TblRegistroCapacidad>();
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach : tblRegistroCapacidadCollectionNew) {
                tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach = em.getReference(tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach.getClass(), tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach.getIdRegistroCapacidad());
                attachedTblRegistroCapacidadCollectionNew.add(tblRegistroCapacidadCollectionNewTblRegistroCapacidadToAttach);
            }
            tblRegistroCapacidadCollectionNew = attachedTblRegistroCapacidadCollectionNew;
            tblRegistro.setTblRegistroCapacidadCollection(tblRegistroCapacidadCollectionNew);
            Collection<TblRegistroHistorial> attachedTblRegistroHistorialCollectionNew = new ArrayList<TblRegistroHistorial>();
            for (TblRegistroHistorial tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach : tblRegistroHistorialCollectionNew) {
                tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach = em.getReference(tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach.getClass(), tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach.getIdRegistroHistorial());
                attachedTblRegistroHistorialCollectionNew.add(tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach);
            }
            tblRegistroHistorialCollectionNew = attachedTblRegistroHistorialCollectionNew;
            tblRegistro.setTblRegistroHistorialCollection(tblRegistroHistorialCollectionNew);
            Collection<TblSolicitudAfiliacion> attachedTblSolicitudAfiliacionCollectionNew = new ArrayList<TblSolicitudAfiliacion>();
            for (TblSolicitudAfiliacion tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacionToAttach : tblSolicitudAfiliacionCollectionNew) {
                tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacionToAttach = em.getReference(tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacionToAttach.getClass(), tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacionToAttach.getIdSolicitudAfiliacion());
                attachedTblSolicitudAfiliacionCollectionNew.add(tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacionToAttach);
            }
            tblSolicitudAfiliacionCollectionNew = attachedTblSolicitudAfiliacionCollectionNew;
            tblRegistro.setTblSolicitudAfiliacionCollection(tblSolicitudAfiliacionCollectionNew);
            tblRegistro = em.merge(tblRegistro);
            if (idAreaOld != null && !idAreaOld.equals(idAreaNew)) {
                idAreaOld.getTblRegistroCollection().remove(tblRegistro);
                idAreaOld = em.merge(idAreaOld);
            }
            if (idAreaNew != null && !idAreaNew.equals(idAreaOld)) {
                idAreaNew.getTblRegistroCollection().add(tblRegistro);
                idAreaNew = em.merge(idAreaNew);
            }
            if (idSolicitudOld != null && !idSolicitudOld.equals(idSolicitudNew)) {
                idSolicitudOld.getTblRegistroCollection().remove(tblRegistro);
                idSolicitudOld = em.merge(idSolicitudOld);
            }
            if (idSolicitudNew != null && !idSolicitudNew.equals(idSolicitudOld)) {
                idSolicitudNew.getTblRegistroCollection().add(tblRegistro);
                idSolicitudNew = em.merge(idSolicitudNew);
            }
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionNewTblRegistroCapacidad : tblRegistroCapacidadCollectionNew) {
                if (!tblRegistroCapacidadCollectionOld.contains(tblRegistroCapacidadCollectionNewTblRegistroCapacidad)) {
                    TblRegistro oldIdRegistroOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad = tblRegistroCapacidadCollectionNewTblRegistroCapacidad.getIdRegistro();
                    tblRegistroCapacidadCollectionNewTblRegistroCapacidad.setIdRegistro(tblRegistro);
                    tblRegistroCapacidadCollectionNewTblRegistroCapacidad = em.merge(tblRegistroCapacidadCollectionNewTblRegistroCapacidad);
                    if (oldIdRegistroOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad != null && !oldIdRegistroOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad.equals(tblRegistro)) {
                        oldIdRegistroOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidadCollectionNewTblRegistroCapacidad);
                        oldIdRegistroOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad = em.merge(oldIdRegistroOfTblRegistroCapacidadCollectionNewTblRegistroCapacidad);
                    }
                }
            }
            for (TblRegistroHistorial tblRegistroHistorialCollectionNewTblRegistroHistorial : tblRegistroHistorialCollectionNew) {
                if (!tblRegistroHistorialCollectionOld.contains(tblRegistroHistorialCollectionNewTblRegistroHistorial)) {
                    TblRegistro oldIdRegistroOfTblRegistroHistorialCollectionNewTblRegistroHistorial = tblRegistroHistorialCollectionNewTblRegistroHistorial.getIdRegistro();
                    tblRegistroHistorialCollectionNewTblRegistroHistorial.setIdRegistro(tblRegistro);
                    tblRegistroHistorialCollectionNewTblRegistroHistorial = em.merge(tblRegistroHistorialCollectionNewTblRegistroHistorial);
                    if (oldIdRegistroOfTblRegistroHistorialCollectionNewTblRegistroHistorial != null && !oldIdRegistroOfTblRegistroHistorialCollectionNewTblRegistroHistorial.equals(tblRegistro)) {
                        oldIdRegistroOfTblRegistroHistorialCollectionNewTblRegistroHistorial.getTblRegistroHistorialCollection().remove(tblRegistroHistorialCollectionNewTblRegistroHistorial);
                        oldIdRegistroOfTblRegistroHistorialCollectionNewTblRegistroHistorial = em.merge(oldIdRegistroOfTblRegistroHistorialCollectionNewTblRegistroHistorial);
                    }
                }
            }
            for (TblSolicitudAfiliacion tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion : tblSolicitudAfiliacionCollectionNew) {
                if (!tblSolicitudAfiliacionCollectionOld.contains(tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion)) {
                    TblRegistro oldIdRegistroOfTblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion = tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion.getIdRegistro();
                    tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion.setIdRegistro(tblRegistro);
                    tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion = em.merge(tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion);
                    if (oldIdRegistroOfTblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion != null && !oldIdRegistroOfTblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion.equals(tblRegistro)) {
                        oldIdRegistroOfTblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion.getTblSolicitudAfiliacionCollection().remove(tblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion);
                        oldIdRegistroOfTblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion = em.merge(oldIdRegistroOfTblSolicitudAfiliacionCollectionNewTblSolicitudAfiliacion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblRegistro.getIdRegistro();
                if (findTblRegistro(id) == null) {
                    throw new NonexistentEntityException("The tblRegistro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistro tblRegistro;
            try {
                tblRegistro = em.getReference(TblRegistro.class, id);
                tblRegistro.getIdRegistro();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblRegistro with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblRegistroCapacidad> tblRegistroCapacidadCollectionOrphanCheck = tblRegistro.getTblRegistroCapacidadCollection();
            for (TblRegistroCapacidad tblRegistroCapacidadCollectionOrphanCheckTblRegistroCapacidad : tblRegistroCapacidadCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblRegistro (" + tblRegistro + ") cannot be destroyed since the TblRegistroCapacidad " + tblRegistroCapacidadCollectionOrphanCheckTblRegistroCapacidad + " in its tblRegistroCapacidadCollection field has a non-nullable idRegistro field.");
            }
            Collection<TblRegistroHistorial> tblRegistroHistorialCollectionOrphanCheck = tblRegistro.getTblRegistroHistorialCollection();
            for (TblRegistroHistorial tblRegistroHistorialCollectionOrphanCheckTblRegistroHistorial : tblRegistroHistorialCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblRegistro (" + tblRegistro + ") cannot be destroyed since the TblRegistroHistorial " + tblRegistroHistorialCollectionOrphanCheckTblRegistroHistorial + " in its tblRegistroHistorialCollection field has a non-nullable idRegistro field.");
            }
            Collection<TblSolicitudAfiliacion> tblSolicitudAfiliacionCollectionOrphanCheck = tblRegistro.getTblSolicitudAfiliacionCollection();
            for (TblSolicitudAfiliacion tblSolicitudAfiliacionCollectionOrphanCheckTblSolicitudAfiliacion : tblSolicitudAfiliacionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblRegistro (" + tblRegistro + ") cannot be destroyed since the TblSolicitudAfiliacion " + tblSolicitudAfiliacionCollectionOrphanCheckTblSolicitudAfiliacion + " in its tblSolicitudAfiliacionCollection field has a non-nullable idRegistro field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TblArea idArea = tblRegistro.getIdArea();
            if (idArea != null) {
                idArea.getTblRegistroCollection().remove(tblRegistro);
                idArea = em.merge(idArea);
            }
            TblSolicitudIngreso idSolicitud = tblRegistro.getIdSolicitud();
            if (idSolicitud != null) {
                idSolicitud.getTblRegistroCollection().remove(tblRegistro);
                idSolicitud = em.merge(idSolicitud);
            }
            em.remove(tblRegistro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblRegistro> findTblRegistroEntities() {
        return findTblRegistroEntities(true, -1, -1);
    }

    public List<TblRegistro> findTblRegistroEntities(int maxResults, int firstResult) {
        return findTblRegistroEntities(false, maxResults, firstResult);
    }

    private List<TblRegistro> findTblRegistroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblRegistro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblRegistro findTblRegistro(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblRegistro.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblRegistroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblRegistro> rt = cq.from(TblRegistro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
