/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblDatosPersonales;
import PERSISTENCIA.TblTelefonos;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblTelefonosJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblTelefonosJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZPRIMEPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblTelefonos tblTelefonos) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDatosPersonales idDatPerson = tblTelefonos.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson = em.getReference(idDatPerson.getClass(), idDatPerson.getIdDatPerson());
                tblTelefonos.setIdDatPerson(idDatPerson);
            }
            em.persist(tblTelefonos);
            if (idDatPerson != null) {
                idDatPerson.getTblTelefonosCollection().add(tblTelefonos);
                idDatPerson = em.merge(idDatPerson);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblTelefonos tblTelefonos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblTelefonos persistentTblTelefonos = em.find(TblTelefonos.class, tblTelefonos.getIdTelefono());
            TblDatosPersonales idDatPersonOld = persistentTblTelefonos.getIdDatPerson();
            TblDatosPersonales idDatPersonNew = tblTelefonos.getIdDatPerson();
            if (idDatPersonNew != null) {
                idDatPersonNew = em.getReference(idDatPersonNew.getClass(), idDatPersonNew.getIdDatPerson());
                tblTelefonos.setIdDatPerson(idDatPersonNew);
            }
            tblTelefonos = em.merge(tblTelefonos);
            if (idDatPersonOld != null && !idDatPersonOld.equals(idDatPersonNew)) {
                idDatPersonOld.getTblTelefonosCollection().remove(tblTelefonos);
                idDatPersonOld = em.merge(idDatPersonOld);
            }
            if (idDatPersonNew != null && !idDatPersonNew.equals(idDatPersonOld)) {
                idDatPersonNew.getTblTelefonosCollection().add(tblTelefonos);
                idDatPersonNew = em.merge(idDatPersonNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblTelefonos.getIdTelefono();
                if (findTblTelefonos(id) == null) {
                    throw new NonexistentEntityException("The tblTelefonos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblTelefonos tblTelefonos;
            try {
                tblTelefonos = em.getReference(TblTelefonos.class, id);
                tblTelefonos.getIdTelefono();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblTelefonos with id " + id + " no longer exists.", enfe);
            }
            TblDatosPersonales idDatPerson = tblTelefonos.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson.getTblTelefonosCollection().remove(tblTelefonos);
                idDatPerson = em.merge(idDatPerson);
            }
            em.remove(tblTelefonos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblTelefonos> findTblTelefonosEntities() {
        return findTblTelefonosEntities(true, -1, -1);
    }

    public List<TblTelefonos> findTblTelefonosEntities(int maxResults, int firstResult) {
        return findTblTelefonosEntities(false, maxResults, firstResult);
    }

    private List<TblTelefonos> findTblTelefonosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblTelefonos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblTelefonos findTblTelefonos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblTelefonos.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblTelefonosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblTelefonos> rt = cq.from(TblTelefonos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
