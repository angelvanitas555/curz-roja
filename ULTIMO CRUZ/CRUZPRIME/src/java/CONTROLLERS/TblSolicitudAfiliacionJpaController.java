/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblRegistro;
import PERSISTENCIA.TblSolicitudAfiliacion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblSolicitudAfiliacionJpaController implements Serializable {

    public TblSolicitudAfiliacionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblSolicitudAfiliacion tblSolicitudAfiliacion) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistro idRegistro = tblSolicitudAfiliacion.getIdRegistro();
            if (idRegistro != null) {
                idRegistro = em.getReference(idRegistro.getClass(), idRegistro.getIdRegistro());
                tblSolicitudAfiliacion.setIdRegistro(idRegistro);
            }
            em.persist(tblSolicitudAfiliacion);
            if (idRegistro != null) {
                idRegistro.getTblSolicitudAfiliacionCollection().add(tblSolicitudAfiliacion);
                idRegistro = em.merge(idRegistro);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblSolicitudAfiliacion tblSolicitudAfiliacion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSolicitudAfiliacion persistentTblSolicitudAfiliacion = em.find(TblSolicitudAfiliacion.class, tblSolicitudAfiliacion.getIdSolicitudAfiliacion());
            TblRegistro idRegistroOld = persistentTblSolicitudAfiliacion.getIdRegistro();
            TblRegistro idRegistroNew = tblSolicitudAfiliacion.getIdRegistro();
            if (idRegistroNew != null) {
                idRegistroNew = em.getReference(idRegistroNew.getClass(), idRegistroNew.getIdRegistro());
                tblSolicitudAfiliacion.setIdRegistro(idRegistroNew);
            }
            tblSolicitudAfiliacion = em.merge(tblSolicitudAfiliacion);
            if (idRegistroOld != null && !idRegistroOld.equals(idRegistroNew)) {
                idRegistroOld.getTblSolicitudAfiliacionCollection().remove(tblSolicitudAfiliacion);
                idRegistroOld = em.merge(idRegistroOld);
            }
            if (idRegistroNew != null && !idRegistroNew.equals(idRegistroOld)) {
                idRegistroNew.getTblSolicitudAfiliacionCollection().add(tblSolicitudAfiliacion);
                idRegistroNew = em.merge(idRegistroNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblSolicitudAfiliacion.getIdSolicitudAfiliacion();
                if (findTblSolicitudAfiliacion(id) == null) {
                    throw new NonexistentEntityException("The tblSolicitudAfiliacion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSolicitudAfiliacion tblSolicitudAfiliacion;
            try {
                tblSolicitudAfiliacion = em.getReference(TblSolicitudAfiliacion.class, id);
                tblSolicitudAfiliacion.getIdSolicitudAfiliacion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblSolicitudAfiliacion with id " + id + " no longer exists.", enfe);
            }
            TblRegistro idRegistro = tblSolicitudAfiliacion.getIdRegistro();
            if (idRegistro != null) {
                idRegistro.getTblSolicitudAfiliacionCollection().remove(tblSolicitudAfiliacion);
                idRegistro = em.merge(idRegistro);
            }
            em.remove(tblSolicitudAfiliacion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblSolicitudAfiliacion> findTblSolicitudAfiliacionEntities() {
        return findTblSolicitudAfiliacionEntities(true, -1, -1);
    }

    public List<TblSolicitudAfiliacion> findTblSolicitudAfiliacionEntities(int maxResults, int firstResult) {
        return findTblSolicitudAfiliacionEntities(false, maxResults, firstResult);
    }

    private List<TblSolicitudAfiliacion> findTblSolicitudAfiliacionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblSolicitudAfiliacion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblSolicitudAfiliacion findTblSolicitudAfiliacion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblSolicitudAfiliacion.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblSolicitudAfiliacionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblSolicitudAfiliacion> rt = cq.from(TblSolicitudAfiliacion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
