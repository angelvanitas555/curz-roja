/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblHistorialCargo;
import java.util.ArrayList;
import java.util.Collection;
import PERSISTENCIA.TblSolicitudIngreso;
import PERSISTENCIA.TblDireccion;
import PERSISTENCIA.TblSeccion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblSeccionJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblSeccionJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZPRIMEPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblSeccion tblSeccion) {
        if (tblSeccion.getTblHistorialCargoCollection() == null) {
            tblSeccion.setTblHistorialCargoCollection(new ArrayList<TblHistorialCargo>());
        }
        if (tblSeccion.getTblSolicitudIngresoCollection() == null) {
            tblSeccion.setTblSolicitudIngresoCollection(new ArrayList<TblSolicitudIngreso>());
        }
        if (tblSeccion.getTblDireccionCollection() == null) {
            tblSeccion.setTblDireccionCollection(new ArrayList<TblDireccion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<TblHistorialCargo> attachedTblHistorialCargoCollection = new ArrayList<TblHistorialCargo>();
            for (TblHistorialCargo tblHistorialCargoCollectionTblHistorialCargoToAttach : tblSeccion.getTblHistorialCargoCollection()) {
                tblHistorialCargoCollectionTblHistorialCargoToAttach = em.getReference(tblHistorialCargoCollectionTblHistorialCargoToAttach.getClass(), tblHistorialCargoCollectionTblHistorialCargoToAttach.getIdHistorialCargo());
                attachedTblHistorialCargoCollection.add(tblHistorialCargoCollectionTblHistorialCargoToAttach);
            }
            tblSeccion.setTblHistorialCargoCollection(attachedTblHistorialCargoCollection);
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollection = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach : tblSeccion.getTblSolicitudIngresoCollection()) {
                tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollection.add(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach);
            }
            tblSeccion.setTblSolicitudIngresoCollection(attachedTblSolicitudIngresoCollection);
            Collection<TblDireccion> attachedTblDireccionCollection = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionTblDireccionToAttach : tblSeccion.getTblDireccionCollection()) {
                tblDireccionCollectionTblDireccionToAttach = em.getReference(tblDireccionCollectionTblDireccionToAttach.getClass(), tblDireccionCollectionTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollection.add(tblDireccionCollectionTblDireccionToAttach);
            }
            tblSeccion.setTblDireccionCollection(attachedTblDireccionCollection);
            em.persist(tblSeccion);
            for (TblHistorialCargo tblHistorialCargoCollectionTblHistorialCargo : tblSeccion.getTblHistorialCargoCollection()) {
                TblSeccion oldIdSeccionOfTblHistorialCargoCollectionTblHistorialCargo = tblHistorialCargoCollectionTblHistorialCargo.getIdSeccion();
                tblHistorialCargoCollectionTblHistorialCargo.setIdSeccion(tblSeccion);
                tblHistorialCargoCollectionTblHistorialCargo = em.merge(tblHistorialCargoCollectionTblHistorialCargo);
                if (oldIdSeccionOfTblHistorialCargoCollectionTblHistorialCargo != null) {
                    oldIdSeccionOfTblHistorialCargoCollectionTblHistorialCargo.getTblHistorialCargoCollection().remove(tblHistorialCargoCollectionTblHistorialCargo);
                    oldIdSeccionOfTblHistorialCargoCollectionTblHistorialCargo = em.merge(oldIdSeccionOfTblHistorialCargoCollectionTblHistorialCargo);
                }
            }
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngreso : tblSeccion.getTblSolicitudIngresoCollection()) {
                TblSeccion oldIdSeccionOfTblSolicitudIngresoCollectionTblSolicitudIngreso = tblSolicitudIngresoCollectionTblSolicitudIngreso.getIdSeccion();
                tblSolicitudIngresoCollectionTblSolicitudIngreso.setIdSeccion(tblSeccion);
                tblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                if (oldIdSeccionOfTblSolicitudIngresoCollectionTblSolicitudIngreso != null) {
                    oldIdSeccionOfTblSolicitudIngresoCollectionTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                    oldIdSeccionOfTblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(oldIdSeccionOfTblSolicitudIngresoCollectionTblSolicitudIngreso);
                }
            }
            for (TblDireccion tblDireccionCollectionTblDireccion : tblSeccion.getTblDireccionCollection()) {
                TblSeccion oldIdSeccionOfTblDireccionCollectionTblDireccion = tblDireccionCollectionTblDireccion.getIdSeccion();
                tblDireccionCollectionTblDireccion.setIdSeccion(tblSeccion);
                tblDireccionCollectionTblDireccion = em.merge(tblDireccionCollectionTblDireccion);
                if (oldIdSeccionOfTblDireccionCollectionTblDireccion != null) {
                    oldIdSeccionOfTblDireccionCollectionTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionTblDireccion);
                    oldIdSeccionOfTblDireccionCollectionTblDireccion = em.merge(oldIdSeccionOfTblDireccionCollectionTblDireccion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblSeccion tblSeccion) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSeccion persistentTblSeccion = em.find(TblSeccion.class, tblSeccion.getIdSeccion());
            Collection<TblHistorialCargo> tblHistorialCargoCollectionOld = persistentTblSeccion.getTblHistorialCargoCollection();
            Collection<TblHistorialCargo> tblHistorialCargoCollectionNew = tblSeccion.getTblHistorialCargoCollection();
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOld = persistentTblSeccion.getTblSolicitudIngresoCollection();
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionNew = tblSeccion.getTblSolicitudIngresoCollection();
            Collection<TblDireccion> tblDireccionCollectionOld = persistentTblSeccion.getTblDireccionCollection();
            Collection<TblDireccion> tblDireccionCollectionNew = tblSeccion.getTblDireccionCollection();
            List<String> illegalOrphanMessages = null;
            for (TblHistorialCargo tblHistorialCargoCollectionOldTblHistorialCargo : tblHistorialCargoCollectionOld) {
                if (!tblHistorialCargoCollectionNew.contains(tblHistorialCargoCollectionOldTblHistorialCargo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblHistorialCargo " + tblHistorialCargoCollectionOldTblHistorialCargo + " since its idSeccion field is not nullable.");
                }
            }
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOldTblSolicitudIngreso : tblSolicitudIngresoCollectionOld) {
                if (!tblSolicitudIngresoCollectionNew.contains(tblSolicitudIngresoCollectionOldTblSolicitudIngreso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblSolicitudIngreso " + tblSolicitudIngresoCollectionOldTblSolicitudIngreso + " since its idSeccion field is not nullable.");
                }
            }
            for (TblDireccion tblDireccionCollectionOldTblDireccion : tblDireccionCollectionOld) {
                if (!tblDireccionCollectionNew.contains(tblDireccionCollectionOldTblDireccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDireccion " + tblDireccionCollectionOldTblDireccion + " since its idSeccion field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<TblHistorialCargo> attachedTblHistorialCargoCollectionNew = new ArrayList<TblHistorialCargo>();
            for (TblHistorialCargo tblHistorialCargoCollectionNewTblHistorialCargoToAttach : tblHistorialCargoCollectionNew) {
                tblHistorialCargoCollectionNewTblHistorialCargoToAttach = em.getReference(tblHistorialCargoCollectionNewTblHistorialCargoToAttach.getClass(), tblHistorialCargoCollectionNewTblHistorialCargoToAttach.getIdHistorialCargo());
                attachedTblHistorialCargoCollectionNew.add(tblHistorialCargoCollectionNewTblHistorialCargoToAttach);
            }
            tblHistorialCargoCollectionNew = attachedTblHistorialCargoCollectionNew;
            tblSeccion.setTblHistorialCargoCollection(tblHistorialCargoCollectionNew);
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollectionNew = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach : tblSolicitudIngresoCollectionNew) {
                tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollectionNew.add(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach);
            }
            tblSolicitudIngresoCollectionNew = attachedTblSolicitudIngresoCollectionNew;
            tblSeccion.setTblSolicitudIngresoCollection(tblSolicitudIngresoCollectionNew);
            Collection<TblDireccion> attachedTblDireccionCollectionNew = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionNewTblDireccionToAttach : tblDireccionCollectionNew) {
                tblDireccionCollectionNewTblDireccionToAttach = em.getReference(tblDireccionCollectionNewTblDireccionToAttach.getClass(), tblDireccionCollectionNewTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollectionNew.add(tblDireccionCollectionNewTblDireccionToAttach);
            }
            tblDireccionCollectionNew = attachedTblDireccionCollectionNew;
            tblSeccion.setTblDireccionCollection(tblDireccionCollectionNew);
            tblSeccion = em.merge(tblSeccion);
            for (TblHistorialCargo tblHistorialCargoCollectionNewTblHistorialCargo : tblHistorialCargoCollectionNew) {
                if (!tblHistorialCargoCollectionOld.contains(tblHistorialCargoCollectionNewTblHistorialCargo)) {
                    TblSeccion oldIdSeccionOfTblHistorialCargoCollectionNewTblHistorialCargo = tblHistorialCargoCollectionNewTblHistorialCargo.getIdSeccion();
                    tblHistorialCargoCollectionNewTblHistorialCargo.setIdSeccion(tblSeccion);
                    tblHistorialCargoCollectionNewTblHistorialCargo = em.merge(tblHistorialCargoCollectionNewTblHistorialCargo);
                    if (oldIdSeccionOfTblHistorialCargoCollectionNewTblHistorialCargo != null && !oldIdSeccionOfTblHistorialCargoCollectionNewTblHistorialCargo.equals(tblSeccion)) {
                        oldIdSeccionOfTblHistorialCargoCollectionNewTblHistorialCargo.getTblHistorialCargoCollection().remove(tblHistorialCargoCollectionNewTblHistorialCargo);
                        oldIdSeccionOfTblHistorialCargoCollectionNewTblHistorialCargo = em.merge(oldIdSeccionOfTblHistorialCargoCollectionNewTblHistorialCargo);
                    }
                }
            }
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngreso : tblSolicitudIngresoCollectionNew) {
                if (!tblSolicitudIngresoCollectionOld.contains(tblSolicitudIngresoCollectionNewTblSolicitudIngreso)) {
                    TblSeccion oldIdSeccionOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = tblSolicitudIngresoCollectionNewTblSolicitudIngreso.getIdSeccion();
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso.setIdSeccion(tblSeccion);
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    if (oldIdSeccionOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso != null && !oldIdSeccionOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.equals(tblSeccion)) {
                        oldIdSeccionOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                        oldIdSeccionOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(oldIdSeccionOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    }
                }
            }
            for (TblDireccion tblDireccionCollectionNewTblDireccion : tblDireccionCollectionNew) {
                if (!tblDireccionCollectionOld.contains(tblDireccionCollectionNewTblDireccion)) {
                    TblSeccion oldIdSeccionOfTblDireccionCollectionNewTblDireccion = tblDireccionCollectionNewTblDireccion.getIdSeccion();
                    tblDireccionCollectionNewTblDireccion.setIdSeccion(tblSeccion);
                    tblDireccionCollectionNewTblDireccion = em.merge(tblDireccionCollectionNewTblDireccion);
                    if (oldIdSeccionOfTblDireccionCollectionNewTblDireccion != null && !oldIdSeccionOfTblDireccionCollectionNewTblDireccion.equals(tblSeccion)) {
                        oldIdSeccionOfTblDireccionCollectionNewTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionNewTblDireccion);
                        oldIdSeccionOfTblDireccionCollectionNewTblDireccion = em.merge(oldIdSeccionOfTblDireccionCollectionNewTblDireccion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblSeccion.getIdSeccion();
                if (findTblSeccion(id) == null) {
                    throw new NonexistentEntityException("The tblSeccion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSeccion tblSeccion;
            try {
                tblSeccion = em.getReference(TblSeccion.class, id);
                tblSeccion.getIdSeccion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblSeccion with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblHistorialCargo> tblHistorialCargoCollectionOrphanCheck = tblSeccion.getTblHistorialCargoCollection();
            for (TblHistorialCargo tblHistorialCargoCollectionOrphanCheckTblHistorialCargo : tblHistorialCargoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblSeccion (" + tblSeccion + ") cannot be destroyed since the TblHistorialCargo " + tblHistorialCargoCollectionOrphanCheckTblHistorialCargo + " in its tblHistorialCargoCollection field has a non-nullable idSeccion field.");
            }
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOrphanCheck = tblSeccion.getTblSolicitudIngresoCollection();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso : tblSolicitudIngresoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblSeccion (" + tblSeccion + ") cannot be destroyed since the TblSolicitudIngreso " + tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso + " in its tblSolicitudIngresoCollection field has a non-nullable idSeccion field.");
            }
            Collection<TblDireccion> tblDireccionCollectionOrphanCheck = tblSeccion.getTblDireccionCollection();
            for (TblDireccion tblDireccionCollectionOrphanCheckTblDireccion : tblDireccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblSeccion (" + tblSeccion + ") cannot be destroyed since the TblDireccion " + tblDireccionCollectionOrphanCheckTblDireccion + " in its tblDireccionCollection field has a non-nullable idSeccion field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tblSeccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblSeccion> findTblSeccionEntities() {
        return findTblSeccionEntities(true, -1, -1);
    }

    public List<TblSeccion> findTblSeccionEntities(int maxResults, int firstResult) {
        return findTblSeccionEntities(false, maxResults, firstResult);
    }

    private List<TblSeccion> findTblSeccionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblSeccion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblSeccion findTblSeccion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblSeccion.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblSeccionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblSeccion> rt = cq.from(TblSeccion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
