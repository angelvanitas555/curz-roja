/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblMunicipio;
import PERSISTENCIA.TblDatosPersonales;
import PERSISTENCIA.TblDatosEmpleoEstudioActual;
import PERSISTENCIA.TblReferencias;
import PERSISTENCIA.TblDatosMedicos;
import PERSISTENCIA.TblDireccion;
import PERSISTENCIA.TblSeccion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblDireccionJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblDireccionJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZPRIMEPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblDireccion tblDireccion) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblMunicipio idMunicipio = tblDireccion.getIdMunicipio();
            if (idMunicipio != null) {
                idMunicipio = em.getReference(idMunicipio.getClass(), idMunicipio.getIdMunicipio());
                tblDireccion.setIdMunicipio(idMunicipio);
            }
            TblDatosPersonales idDatPerson = tblDireccion.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson = em.getReference(idDatPerson.getClass(), idDatPerson.getIdDatPerson());
                tblDireccion.setIdDatPerson(idDatPerson);
            }
            TblDatosEmpleoEstudioActual idEmpleoEstudio = tblDireccion.getIdEmpleoEstudio();
            if (idEmpleoEstudio != null) {
                idEmpleoEstudio = em.getReference(idEmpleoEstudio.getClass(), idEmpleoEstudio.getIdEmpleoEstudio());
                tblDireccion.setIdEmpleoEstudio(idEmpleoEstudio);
            }
            TblReferencias idReferencias = tblDireccion.getIdReferencias();
            if (idReferencias != null) {
                idReferencias = em.getReference(idReferencias.getClass(), idReferencias.getIdReferencia());
                tblDireccion.setIdReferencias(idReferencias);
            }
            TblDatosMedicos idDatosMedicos = tblDireccion.getIdDatosMedicos();
            if (idDatosMedicos != null) {
                idDatosMedicos = em.getReference(idDatosMedicos.getClass(), idDatosMedicos.getIdDatosMedicos());
                tblDireccion.setIdDatosMedicos(idDatosMedicos);
            }
            TblSeccion idSeccion = tblDireccion.getIdSeccion();
            if (idSeccion != null) {
                idSeccion = em.getReference(idSeccion.getClass(), idSeccion.getIdSeccion());
                tblDireccion.setIdSeccion(idSeccion);
            }
            em.persist(tblDireccion);
            if (idMunicipio != null) {
                idMunicipio.getTblDireccionCollection().add(tblDireccion);
                idMunicipio = em.merge(idMunicipio);
            }
            if (idDatPerson != null) {
                idDatPerson.getTblDireccionCollection().add(tblDireccion);
                idDatPerson = em.merge(idDatPerson);
            }
            if (idEmpleoEstudio != null) {
                idEmpleoEstudio.getTblDireccionCollection().add(tblDireccion);
                idEmpleoEstudio = em.merge(idEmpleoEstudio);
            }
            if (idReferencias != null) {
                idReferencias.getTblDireccionCollection().add(tblDireccion);
                idReferencias = em.merge(idReferencias);
            }
            if (idDatosMedicos != null) {
                idDatosMedicos.getTblDireccionCollection().add(tblDireccion);
                idDatosMedicos = em.merge(idDatosMedicos);
            }
            if (idSeccion != null) {
                idSeccion.getTblDireccionCollection().add(tblDireccion);
                idSeccion = em.merge(idSeccion);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblDireccion tblDireccion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDireccion persistentTblDireccion = em.find(TblDireccion.class, tblDireccion.getIdDireccion());
            TblMunicipio idMunicipioOld = persistentTblDireccion.getIdMunicipio();
            TblMunicipio idMunicipioNew = tblDireccion.getIdMunicipio();
            TblDatosPersonales idDatPersonOld = persistentTblDireccion.getIdDatPerson();
            TblDatosPersonales idDatPersonNew = tblDireccion.getIdDatPerson();
            TblDatosEmpleoEstudioActual idEmpleoEstudioOld = persistentTblDireccion.getIdEmpleoEstudio();
            TblDatosEmpleoEstudioActual idEmpleoEstudioNew = tblDireccion.getIdEmpleoEstudio();
            TblReferencias idReferenciasOld = persistentTblDireccion.getIdReferencias();
            TblReferencias idReferenciasNew = tblDireccion.getIdReferencias();
            TblDatosMedicos idDatosMedicosOld = persistentTblDireccion.getIdDatosMedicos();
            TblDatosMedicos idDatosMedicosNew = tblDireccion.getIdDatosMedicos();
            TblSeccion idSeccionOld = persistentTblDireccion.getIdSeccion();
            TblSeccion idSeccionNew = tblDireccion.getIdSeccion();
            if (idMunicipioNew != null) {
                idMunicipioNew = em.getReference(idMunicipioNew.getClass(), idMunicipioNew.getIdMunicipio());
                tblDireccion.setIdMunicipio(idMunicipioNew);
            }
            if (idDatPersonNew != null) {
                idDatPersonNew = em.getReference(idDatPersonNew.getClass(), idDatPersonNew.getIdDatPerson());
                tblDireccion.setIdDatPerson(idDatPersonNew);
            }
            if (idEmpleoEstudioNew != null) {
                idEmpleoEstudioNew = em.getReference(idEmpleoEstudioNew.getClass(), idEmpleoEstudioNew.getIdEmpleoEstudio());
                tblDireccion.setIdEmpleoEstudio(idEmpleoEstudioNew);
            }
            if (idReferenciasNew != null) {
                idReferenciasNew = em.getReference(idReferenciasNew.getClass(), idReferenciasNew.getIdReferencia());
                tblDireccion.setIdReferencias(idReferenciasNew);
            }
            if (idDatosMedicosNew != null) {
                idDatosMedicosNew = em.getReference(idDatosMedicosNew.getClass(), idDatosMedicosNew.getIdDatosMedicos());
                tblDireccion.setIdDatosMedicos(idDatosMedicosNew);
            }
            if (idSeccionNew != null) {
                idSeccionNew = em.getReference(idSeccionNew.getClass(), idSeccionNew.getIdSeccion());
                tblDireccion.setIdSeccion(idSeccionNew);
            }
            tblDireccion = em.merge(tblDireccion);
            if (idMunicipioOld != null && !idMunicipioOld.equals(idMunicipioNew)) {
                idMunicipioOld.getTblDireccionCollection().remove(tblDireccion);
                idMunicipioOld = em.merge(idMunicipioOld);
            }
            if (idMunicipioNew != null && !idMunicipioNew.equals(idMunicipioOld)) {
                idMunicipioNew.getTblDireccionCollection().add(tblDireccion);
                idMunicipioNew = em.merge(idMunicipioNew);
            }
            if (idDatPersonOld != null && !idDatPersonOld.equals(idDatPersonNew)) {
                idDatPersonOld.getTblDireccionCollection().remove(tblDireccion);
                idDatPersonOld = em.merge(idDatPersonOld);
            }
            if (idDatPersonNew != null && !idDatPersonNew.equals(idDatPersonOld)) {
                idDatPersonNew.getTblDireccionCollection().add(tblDireccion);
                idDatPersonNew = em.merge(idDatPersonNew);
            }
            if (idEmpleoEstudioOld != null && !idEmpleoEstudioOld.equals(idEmpleoEstudioNew)) {
                idEmpleoEstudioOld.getTblDireccionCollection().remove(tblDireccion);
                idEmpleoEstudioOld = em.merge(idEmpleoEstudioOld);
            }
            if (idEmpleoEstudioNew != null && !idEmpleoEstudioNew.equals(idEmpleoEstudioOld)) {
                idEmpleoEstudioNew.getTblDireccionCollection().add(tblDireccion);
                idEmpleoEstudioNew = em.merge(idEmpleoEstudioNew);
            }
            if (idReferenciasOld != null && !idReferenciasOld.equals(idReferenciasNew)) {
                idReferenciasOld.getTblDireccionCollection().remove(tblDireccion);
                idReferenciasOld = em.merge(idReferenciasOld);
            }
            if (idReferenciasNew != null && !idReferenciasNew.equals(idReferenciasOld)) {
                idReferenciasNew.getTblDireccionCollection().add(tblDireccion);
                idReferenciasNew = em.merge(idReferenciasNew);
            }
            if (idDatosMedicosOld != null && !idDatosMedicosOld.equals(idDatosMedicosNew)) {
                idDatosMedicosOld.getTblDireccionCollection().remove(tblDireccion);
                idDatosMedicosOld = em.merge(idDatosMedicosOld);
            }
            if (idDatosMedicosNew != null && !idDatosMedicosNew.equals(idDatosMedicosOld)) {
                idDatosMedicosNew.getTblDireccionCollection().add(tblDireccion);
                idDatosMedicosNew = em.merge(idDatosMedicosNew);
            }
            if (idSeccionOld != null && !idSeccionOld.equals(idSeccionNew)) {
                idSeccionOld.getTblDireccionCollection().remove(tblDireccion);
                idSeccionOld = em.merge(idSeccionOld);
            }
            if (idSeccionNew != null && !idSeccionNew.equals(idSeccionOld)) {
                idSeccionNew.getTblDireccionCollection().add(tblDireccion);
                idSeccionNew = em.merge(idSeccionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblDireccion.getIdDireccion();
                if (findTblDireccion(id) == null) {
                    throw new NonexistentEntityException("The tblDireccion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblDireccion tblDireccion;
            try {
                tblDireccion = em.getReference(TblDireccion.class, id);
                tblDireccion.getIdDireccion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblDireccion with id " + id + " no longer exists.", enfe);
            }
            TblMunicipio idMunicipio = tblDireccion.getIdMunicipio();
            if (idMunicipio != null) {
                idMunicipio.getTblDireccionCollection().remove(tblDireccion);
                idMunicipio = em.merge(idMunicipio);
            }
            TblDatosPersonales idDatPerson = tblDireccion.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson.getTblDireccionCollection().remove(tblDireccion);
                idDatPerson = em.merge(idDatPerson);
            }
            TblDatosEmpleoEstudioActual idEmpleoEstudio = tblDireccion.getIdEmpleoEstudio();
            if (idEmpleoEstudio != null) {
                idEmpleoEstudio.getTblDireccionCollection().remove(tblDireccion);
                idEmpleoEstudio = em.merge(idEmpleoEstudio);
            }
            TblReferencias idReferencias = tblDireccion.getIdReferencias();
            if (idReferencias != null) {
                idReferencias.getTblDireccionCollection().remove(tblDireccion);
                idReferencias = em.merge(idReferencias);
            }
            TblDatosMedicos idDatosMedicos = tblDireccion.getIdDatosMedicos();
            if (idDatosMedicos != null) {
                idDatosMedicos.getTblDireccionCollection().remove(tblDireccion);
                idDatosMedicos = em.merge(idDatosMedicos);
            }
            TblSeccion idSeccion = tblDireccion.getIdSeccion();
            if (idSeccion != null) {
                idSeccion.getTblDireccionCollection().remove(tblDireccion);
                idSeccion = em.merge(idSeccion);
            }
            em.remove(tblDireccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblDireccion> findTblDireccionEntities() {
        return findTblDireccionEntities(true, -1, -1);
    }

    public List<TblDireccion> findTblDireccionEntities(int maxResults, int firstResult) {
        return findTblDireccionEntities(false, maxResults, firstResult);
    }

    private List<TblDireccion> findTblDireccionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblDireccion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblDireccion findTblDireccion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblDireccion.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblDireccionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblDireccion> rt = cq.from(TblDireccion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
