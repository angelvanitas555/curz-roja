/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblArea;
import PERSISTENCIA.TblSeccion;
import PERSISTENCIA.TblDatosPersonales;
import PERSISTENCIA.TblUsuario;
import PERSISTENCIA.TblDatosMedicos;
import java.util.ArrayList;
import java.util.Collection;
import PERSISTENCIA.TblFormularioEntrevista;
import PERSISTENCIA.TblRegistro;
import PERSISTENCIA.TblSolicitudIngreso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblSolicitudIngresoJpaController implements Serializable {

    public TblSolicitudIngresoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblSolicitudIngreso tblSolicitudIngreso) {
        if (tblSolicitudIngreso.getTblDatosMedicosCollection() == null) {
            tblSolicitudIngreso.setTblDatosMedicosCollection(new ArrayList<TblDatosMedicos>());
        }
        if (tblSolicitudIngreso.getTblFormularioEntrevistaCollection() == null) {
            tblSolicitudIngreso.setTblFormularioEntrevistaCollection(new ArrayList<TblFormularioEntrevista>());
        }
        if (tblSolicitudIngreso.getTblRegistroCollection() == null) {
            tblSolicitudIngreso.setTblRegistroCollection(new ArrayList<TblRegistro>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblArea idArea = tblSolicitudIngreso.getIdArea();
            if (idArea != null) {
                idArea = em.getReference(idArea.getClass(), idArea.getIdArea());
                tblSolicitudIngreso.setIdArea(idArea);
            }
            TblSeccion idSeccion = tblSolicitudIngreso.getIdSeccion();
            if (idSeccion != null) {
                idSeccion = em.getReference(idSeccion.getClass(), idSeccion.getIdSeccion());
                tblSolicitudIngreso.setIdSeccion(idSeccion);
            }
            TblDatosPersonales idDatPerson = tblSolicitudIngreso.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson = em.getReference(idDatPerson.getClass(), idDatPerson.getIdDatPerson());
                tblSolicitudIngreso.setIdDatPerson(idDatPerson);
            }
            TblUsuario idUsuario = tblSolicitudIngreso.getIdUsuario();
            if (idUsuario != null) {
                idUsuario = em.getReference(idUsuario.getClass(), idUsuario.getIdUsuario());
                tblSolicitudIngreso.setIdUsuario(idUsuario);
            }
            Collection<TblDatosMedicos> attachedTblDatosMedicosCollection = new ArrayList<TblDatosMedicos>();
            for (TblDatosMedicos tblDatosMedicosCollectionTblDatosMedicosToAttach : tblSolicitudIngreso.getTblDatosMedicosCollection()) {
                tblDatosMedicosCollectionTblDatosMedicosToAttach = em.getReference(tblDatosMedicosCollectionTblDatosMedicosToAttach.getClass(), tblDatosMedicosCollectionTblDatosMedicosToAttach.getIdDatosMedicos());
                attachedTblDatosMedicosCollection.add(tblDatosMedicosCollectionTblDatosMedicosToAttach);
            }
            tblSolicitudIngreso.setTblDatosMedicosCollection(attachedTblDatosMedicosCollection);
            Collection<TblFormularioEntrevista> attachedTblFormularioEntrevistaCollection = new ArrayList<TblFormularioEntrevista>();
            for (TblFormularioEntrevista tblFormularioEntrevistaCollectionTblFormularioEntrevistaToAttach : tblSolicitudIngreso.getTblFormularioEntrevistaCollection()) {
                tblFormularioEntrevistaCollectionTblFormularioEntrevistaToAttach = em.getReference(tblFormularioEntrevistaCollectionTblFormularioEntrevistaToAttach.getClass(), tblFormularioEntrevistaCollectionTblFormularioEntrevistaToAttach.getIdformularioEntrevista());
                attachedTblFormularioEntrevistaCollection.add(tblFormularioEntrevistaCollectionTblFormularioEntrevistaToAttach);
            }
            tblSolicitudIngreso.setTblFormularioEntrevistaCollection(attachedTblFormularioEntrevistaCollection);
            Collection<TblRegistro> attachedTblRegistroCollection = new ArrayList<TblRegistro>();
            for (TblRegistro tblRegistroCollectionTblRegistroToAttach : tblSolicitudIngreso.getTblRegistroCollection()) {
                tblRegistroCollectionTblRegistroToAttach = em.getReference(tblRegistroCollectionTblRegistroToAttach.getClass(), tblRegistroCollectionTblRegistroToAttach.getIdRegistro());
                attachedTblRegistroCollection.add(tblRegistroCollectionTblRegistroToAttach);
            }
            tblSolicitudIngreso.setTblRegistroCollection(attachedTblRegistroCollection);
            em.persist(tblSolicitudIngreso);
            if (idArea != null) {
                idArea.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idArea = em.merge(idArea);
            }
            if (idSeccion != null) {
                idSeccion.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idSeccion = em.merge(idSeccion);
            }
            if (idDatPerson != null) {
                idDatPerson.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idDatPerson = em.merge(idDatPerson);
            }
            if (idUsuario != null) {
                idUsuario.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idUsuario = em.merge(idUsuario);
            }
            for (TblDatosMedicos tblDatosMedicosCollectionTblDatosMedicos : tblSolicitudIngreso.getTblDatosMedicosCollection()) {
                TblSolicitudIngreso oldIdSolicitudOfTblDatosMedicosCollectionTblDatosMedicos = tblDatosMedicosCollectionTblDatosMedicos.getIdSolicitud();
                tblDatosMedicosCollectionTblDatosMedicos.setIdSolicitud(tblSolicitudIngreso);
                tblDatosMedicosCollectionTblDatosMedicos = em.merge(tblDatosMedicosCollectionTblDatosMedicos);
                if (oldIdSolicitudOfTblDatosMedicosCollectionTblDatosMedicos != null) {
                    oldIdSolicitudOfTblDatosMedicosCollectionTblDatosMedicos.getTblDatosMedicosCollection().remove(tblDatosMedicosCollectionTblDatosMedicos);
                    oldIdSolicitudOfTblDatosMedicosCollectionTblDatosMedicos = em.merge(oldIdSolicitudOfTblDatosMedicosCollectionTblDatosMedicos);
                }
            }
            for (TblFormularioEntrevista tblFormularioEntrevistaCollectionTblFormularioEntrevista : tblSolicitudIngreso.getTblFormularioEntrevistaCollection()) {
                TblSolicitudIngreso oldIdSolicitudOfTblFormularioEntrevistaCollectionTblFormularioEntrevista = tblFormularioEntrevistaCollectionTblFormularioEntrevista.getIdSolicitud();
                tblFormularioEntrevistaCollectionTblFormularioEntrevista.setIdSolicitud(tblSolicitudIngreso);
                tblFormularioEntrevistaCollectionTblFormularioEntrevista = em.merge(tblFormularioEntrevistaCollectionTblFormularioEntrevista);
                if (oldIdSolicitudOfTblFormularioEntrevistaCollectionTblFormularioEntrevista != null) {
                    oldIdSolicitudOfTblFormularioEntrevistaCollectionTblFormularioEntrevista.getTblFormularioEntrevistaCollection().remove(tblFormularioEntrevistaCollectionTblFormularioEntrevista);
                    oldIdSolicitudOfTblFormularioEntrevistaCollectionTblFormularioEntrevista = em.merge(oldIdSolicitudOfTblFormularioEntrevistaCollectionTblFormularioEntrevista);
                }
            }
            for (TblRegistro tblRegistroCollectionTblRegistro : tblSolicitudIngreso.getTblRegistroCollection()) {
                TblSolicitudIngreso oldIdSolicitudOfTblRegistroCollectionTblRegistro = tblRegistroCollectionTblRegistro.getIdSolicitud();
                tblRegistroCollectionTblRegistro.setIdSolicitud(tblSolicitudIngreso);
                tblRegistroCollectionTblRegistro = em.merge(tblRegistroCollectionTblRegistro);
                if (oldIdSolicitudOfTblRegistroCollectionTblRegistro != null) {
                    oldIdSolicitudOfTblRegistroCollectionTblRegistro.getTblRegistroCollection().remove(tblRegistroCollectionTblRegistro);
                    oldIdSolicitudOfTblRegistroCollectionTblRegistro = em.merge(oldIdSolicitudOfTblRegistroCollectionTblRegistro);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblSolicitudIngreso tblSolicitudIngreso) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSolicitudIngreso persistentTblSolicitudIngreso = em.find(TblSolicitudIngreso.class, tblSolicitudIngreso.getIdSolicitud());
            TblArea idAreaOld = persistentTblSolicitudIngreso.getIdArea();
            TblArea idAreaNew = tblSolicitudIngreso.getIdArea();
            TblSeccion idSeccionOld = persistentTblSolicitudIngreso.getIdSeccion();
            TblSeccion idSeccionNew = tblSolicitudIngreso.getIdSeccion();
            TblDatosPersonales idDatPersonOld = persistentTblSolicitudIngreso.getIdDatPerson();
            TblDatosPersonales idDatPersonNew = tblSolicitudIngreso.getIdDatPerson();
            TblUsuario idUsuarioOld = persistentTblSolicitudIngreso.getIdUsuario();
            TblUsuario idUsuarioNew = tblSolicitudIngreso.getIdUsuario();
            Collection<TblDatosMedicos> tblDatosMedicosCollectionOld = persistentTblSolicitudIngreso.getTblDatosMedicosCollection();
            Collection<TblDatosMedicos> tblDatosMedicosCollectionNew = tblSolicitudIngreso.getTblDatosMedicosCollection();
            Collection<TblFormularioEntrevista> tblFormularioEntrevistaCollectionOld = persistentTblSolicitudIngreso.getTblFormularioEntrevistaCollection();
            Collection<TblFormularioEntrevista> tblFormularioEntrevistaCollectionNew = tblSolicitudIngreso.getTblFormularioEntrevistaCollection();
            Collection<TblRegistro> tblRegistroCollectionOld = persistentTblSolicitudIngreso.getTblRegistroCollection();
            Collection<TblRegistro> tblRegistroCollectionNew = tblSolicitudIngreso.getTblRegistroCollection();
            List<String> illegalOrphanMessages = null;
            for (TblDatosMedicos tblDatosMedicosCollectionOldTblDatosMedicos : tblDatosMedicosCollectionOld) {
                if (!tblDatosMedicosCollectionNew.contains(tblDatosMedicosCollectionOldTblDatosMedicos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDatosMedicos " + tblDatosMedicosCollectionOldTblDatosMedicos + " since its idSolicitud field is not nullable.");
                }
            }
            for (TblFormularioEntrevista tblFormularioEntrevistaCollectionOldTblFormularioEntrevista : tblFormularioEntrevistaCollectionOld) {
                if (!tblFormularioEntrevistaCollectionNew.contains(tblFormularioEntrevistaCollectionOldTblFormularioEntrevista)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblFormularioEntrevista " + tblFormularioEntrevistaCollectionOldTblFormularioEntrevista + " since its idSolicitud field is not nullable.");
                }
            }
            for (TblRegistro tblRegistroCollectionOldTblRegistro : tblRegistroCollectionOld) {
                if (!tblRegistroCollectionNew.contains(tblRegistroCollectionOldTblRegistro)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblRegistro " + tblRegistroCollectionOldTblRegistro + " since its idSolicitud field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idAreaNew != null) {
                idAreaNew = em.getReference(idAreaNew.getClass(), idAreaNew.getIdArea());
                tblSolicitudIngreso.setIdArea(idAreaNew);
            }
            if (idSeccionNew != null) {
                idSeccionNew = em.getReference(idSeccionNew.getClass(), idSeccionNew.getIdSeccion());
                tblSolicitudIngreso.setIdSeccion(idSeccionNew);
            }
            if (idDatPersonNew != null) {
                idDatPersonNew = em.getReference(idDatPersonNew.getClass(), idDatPersonNew.getIdDatPerson());
                tblSolicitudIngreso.setIdDatPerson(idDatPersonNew);
            }
            if (idUsuarioNew != null) {
                idUsuarioNew = em.getReference(idUsuarioNew.getClass(), idUsuarioNew.getIdUsuario());
                tblSolicitudIngreso.setIdUsuario(idUsuarioNew);
            }
            Collection<TblDatosMedicos> attachedTblDatosMedicosCollectionNew = new ArrayList<TblDatosMedicos>();
            for (TblDatosMedicos tblDatosMedicosCollectionNewTblDatosMedicosToAttach : tblDatosMedicosCollectionNew) {
                tblDatosMedicosCollectionNewTblDatosMedicosToAttach = em.getReference(tblDatosMedicosCollectionNewTblDatosMedicosToAttach.getClass(), tblDatosMedicosCollectionNewTblDatosMedicosToAttach.getIdDatosMedicos());
                attachedTblDatosMedicosCollectionNew.add(tblDatosMedicosCollectionNewTblDatosMedicosToAttach);
            }
            tblDatosMedicosCollectionNew = attachedTblDatosMedicosCollectionNew;
            tblSolicitudIngreso.setTblDatosMedicosCollection(tblDatosMedicosCollectionNew);
            Collection<TblFormularioEntrevista> attachedTblFormularioEntrevistaCollectionNew = new ArrayList<TblFormularioEntrevista>();
            for (TblFormularioEntrevista tblFormularioEntrevistaCollectionNewTblFormularioEntrevistaToAttach : tblFormularioEntrevistaCollectionNew) {
                tblFormularioEntrevistaCollectionNewTblFormularioEntrevistaToAttach = em.getReference(tblFormularioEntrevistaCollectionNewTblFormularioEntrevistaToAttach.getClass(), tblFormularioEntrevistaCollectionNewTblFormularioEntrevistaToAttach.getIdformularioEntrevista());
                attachedTblFormularioEntrevistaCollectionNew.add(tblFormularioEntrevistaCollectionNewTblFormularioEntrevistaToAttach);
            }
            tblFormularioEntrevistaCollectionNew = attachedTblFormularioEntrevistaCollectionNew;
            tblSolicitudIngreso.setTblFormularioEntrevistaCollection(tblFormularioEntrevistaCollectionNew);
            Collection<TblRegistro> attachedTblRegistroCollectionNew = new ArrayList<TblRegistro>();
            for (TblRegistro tblRegistroCollectionNewTblRegistroToAttach : tblRegistroCollectionNew) {
                tblRegistroCollectionNewTblRegistroToAttach = em.getReference(tblRegistroCollectionNewTblRegistroToAttach.getClass(), tblRegistroCollectionNewTblRegistroToAttach.getIdRegistro());
                attachedTblRegistroCollectionNew.add(tblRegistroCollectionNewTblRegistroToAttach);
            }
            tblRegistroCollectionNew = attachedTblRegistroCollectionNew;
            tblSolicitudIngreso.setTblRegistroCollection(tblRegistroCollectionNew);
            tblSolicitudIngreso = em.merge(tblSolicitudIngreso);
            if (idAreaOld != null && !idAreaOld.equals(idAreaNew)) {
                idAreaOld.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idAreaOld = em.merge(idAreaOld);
            }
            if (idAreaNew != null && !idAreaNew.equals(idAreaOld)) {
                idAreaNew.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idAreaNew = em.merge(idAreaNew);
            }
            if (idSeccionOld != null && !idSeccionOld.equals(idSeccionNew)) {
                idSeccionOld.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idSeccionOld = em.merge(idSeccionOld);
            }
            if (idSeccionNew != null && !idSeccionNew.equals(idSeccionOld)) {
                idSeccionNew.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idSeccionNew = em.merge(idSeccionNew);
            }
            if (idDatPersonOld != null && !idDatPersonOld.equals(idDatPersonNew)) {
                idDatPersonOld.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idDatPersonOld = em.merge(idDatPersonOld);
            }
            if (idDatPersonNew != null && !idDatPersonNew.equals(idDatPersonOld)) {
                idDatPersonNew.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idDatPersonNew = em.merge(idDatPersonNew);
            }
            if (idUsuarioOld != null && !idUsuarioOld.equals(idUsuarioNew)) {
                idUsuarioOld.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idUsuarioOld = em.merge(idUsuarioOld);
            }
            if (idUsuarioNew != null && !idUsuarioNew.equals(idUsuarioOld)) {
                idUsuarioNew.getTblSolicitudIngresoCollection().add(tblSolicitudIngreso);
                idUsuarioNew = em.merge(idUsuarioNew);
            }
            for (TblDatosMedicos tblDatosMedicosCollectionNewTblDatosMedicos : tblDatosMedicosCollectionNew) {
                if (!tblDatosMedicosCollectionOld.contains(tblDatosMedicosCollectionNewTblDatosMedicos)) {
                    TblSolicitudIngreso oldIdSolicitudOfTblDatosMedicosCollectionNewTblDatosMedicos = tblDatosMedicosCollectionNewTblDatosMedicos.getIdSolicitud();
                    tblDatosMedicosCollectionNewTblDatosMedicos.setIdSolicitud(tblSolicitudIngreso);
                    tblDatosMedicosCollectionNewTblDatosMedicos = em.merge(tblDatosMedicosCollectionNewTblDatosMedicos);
                    if (oldIdSolicitudOfTblDatosMedicosCollectionNewTblDatosMedicos != null && !oldIdSolicitudOfTblDatosMedicosCollectionNewTblDatosMedicos.equals(tblSolicitudIngreso)) {
                        oldIdSolicitudOfTblDatosMedicosCollectionNewTblDatosMedicos.getTblDatosMedicosCollection().remove(tblDatosMedicosCollectionNewTblDatosMedicos);
                        oldIdSolicitudOfTblDatosMedicosCollectionNewTblDatosMedicos = em.merge(oldIdSolicitudOfTblDatosMedicosCollectionNewTblDatosMedicos);
                    }
                }
            }
            for (TblFormularioEntrevista tblFormularioEntrevistaCollectionNewTblFormularioEntrevista : tblFormularioEntrevistaCollectionNew) {
                if (!tblFormularioEntrevistaCollectionOld.contains(tblFormularioEntrevistaCollectionNewTblFormularioEntrevista)) {
                    TblSolicitudIngreso oldIdSolicitudOfTblFormularioEntrevistaCollectionNewTblFormularioEntrevista = tblFormularioEntrevistaCollectionNewTblFormularioEntrevista.getIdSolicitud();
                    tblFormularioEntrevistaCollectionNewTblFormularioEntrevista.setIdSolicitud(tblSolicitudIngreso);
                    tblFormularioEntrevistaCollectionNewTblFormularioEntrevista = em.merge(tblFormularioEntrevistaCollectionNewTblFormularioEntrevista);
                    if (oldIdSolicitudOfTblFormularioEntrevistaCollectionNewTblFormularioEntrevista != null && !oldIdSolicitudOfTblFormularioEntrevistaCollectionNewTblFormularioEntrevista.equals(tblSolicitudIngreso)) {
                        oldIdSolicitudOfTblFormularioEntrevistaCollectionNewTblFormularioEntrevista.getTblFormularioEntrevistaCollection().remove(tblFormularioEntrevistaCollectionNewTblFormularioEntrevista);
                        oldIdSolicitudOfTblFormularioEntrevistaCollectionNewTblFormularioEntrevista = em.merge(oldIdSolicitudOfTblFormularioEntrevistaCollectionNewTblFormularioEntrevista);
                    }
                }
            }
            for (TblRegistro tblRegistroCollectionNewTblRegistro : tblRegistroCollectionNew) {
                if (!tblRegistroCollectionOld.contains(tblRegistroCollectionNewTblRegistro)) {
                    TblSolicitudIngreso oldIdSolicitudOfTblRegistroCollectionNewTblRegistro = tblRegistroCollectionNewTblRegistro.getIdSolicitud();
                    tblRegistroCollectionNewTblRegistro.setIdSolicitud(tblSolicitudIngreso);
                    tblRegistroCollectionNewTblRegistro = em.merge(tblRegistroCollectionNewTblRegistro);
                    if (oldIdSolicitudOfTblRegistroCollectionNewTblRegistro != null && !oldIdSolicitudOfTblRegistroCollectionNewTblRegistro.equals(tblSolicitudIngreso)) {
                        oldIdSolicitudOfTblRegistroCollectionNewTblRegistro.getTblRegistroCollection().remove(tblRegistroCollectionNewTblRegistro);
                        oldIdSolicitudOfTblRegistroCollectionNewTblRegistro = em.merge(oldIdSolicitudOfTblRegistroCollectionNewTblRegistro);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblSolicitudIngreso.getIdSolicitud();
                if (findTblSolicitudIngreso(id) == null) {
                    throw new NonexistentEntityException("The tblSolicitudIngreso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSolicitudIngreso tblSolicitudIngreso;
            try {
                tblSolicitudIngreso = em.getReference(TblSolicitudIngreso.class, id);
                tblSolicitudIngreso.getIdSolicitud();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblSolicitudIngreso with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblDatosMedicos> tblDatosMedicosCollectionOrphanCheck = tblSolicitudIngreso.getTblDatosMedicosCollection();
            for (TblDatosMedicos tblDatosMedicosCollectionOrphanCheckTblDatosMedicos : tblDatosMedicosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblSolicitudIngreso (" + tblSolicitudIngreso + ") cannot be destroyed since the TblDatosMedicos " + tblDatosMedicosCollectionOrphanCheckTblDatosMedicos + " in its tblDatosMedicosCollection field has a non-nullable idSolicitud field.");
            }
            Collection<TblFormularioEntrevista> tblFormularioEntrevistaCollectionOrphanCheck = tblSolicitudIngreso.getTblFormularioEntrevistaCollection();
            for (TblFormularioEntrevista tblFormularioEntrevistaCollectionOrphanCheckTblFormularioEntrevista : tblFormularioEntrevistaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblSolicitudIngreso (" + tblSolicitudIngreso + ") cannot be destroyed since the TblFormularioEntrevista " + tblFormularioEntrevistaCollectionOrphanCheckTblFormularioEntrevista + " in its tblFormularioEntrevistaCollection field has a non-nullable idSolicitud field.");
            }
            Collection<TblRegistro> tblRegistroCollectionOrphanCheck = tblSolicitudIngreso.getTblRegistroCollection();
            for (TblRegistro tblRegistroCollectionOrphanCheckTblRegistro : tblRegistroCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblSolicitudIngreso (" + tblSolicitudIngreso + ") cannot be destroyed since the TblRegistro " + tblRegistroCollectionOrphanCheckTblRegistro + " in its tblRegistroCollection field has a non-nullable idSolicitud field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TblArea idArea = tblSolicitudIngreso.getIdArea();
            if (idArea != null) {
                idArea.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idArea = em.merge(idArea);
            }
            TblSeccion idSeccion = tblSolicitudIngreso.getIdSeccion();
            if (idSeccion != null) {
                idSeccion.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idSeccion = em.merge(idSeccion);
            }
            TblDatosPersonales idDatPerson = tblSolicitudIngreso.getIdDatPerson();
            if (idDatPerson != null) {
                idDatPerson.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idDatPerson = em.merge(idDatPerson);
            }
            TblUsuario idUsuario = tblSolicitudIngreso.getIdUsuario();
            if (idUsuario != null) {
                idUsuario.getTblSolicitudIngresoCollection().remove(tblSolicitudIngreso);
                idUsuario = em.merge(idUsuario);
            }
            em.remove(tblSolicitudIngreso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblSolicitudIngreso> findTblSolicitudIngresoEntities() {
        return findTblSolicitudIngresoEntities(true, -1, -1);
    }

    public List<TblSolicitudIngreso> findTblSolicitudIngresoEntities(int maxResults, int firstResult) {
        return findTblSolicitudIngresoEntities(false, maxResults, firstResult);
    }

    private List<TblSolicitudIngreso> findTblSolicitudIngresoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblSolicitudIngreso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblSolicitudIngreso findTblSolicitudIngreso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblSolicitudIngreso.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblSolicitudIngresoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblSolicitudIngreso> rt = cq.from(TblSolicitudIngreso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
