/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblRegistro;
import PERSISTENCIA.TblCapacitaciones;
import PERSISTENCIA.TblRegistroCapacidad;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblRegistroCapacidadJpaController implements Serializable {

    public TblRegistroCapacidadJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblRegistroCapacidad tblRegistroCapacidad) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistro idRegistro = tblRegistroCapacidad.getIdRegistro();
            if (idRegistro != null) {
                idRegistro = em.getReference(idRegistro.getClass(), idRegistro.getIdRegistro());
                tblRegistroCapacidad.setIdRegistro(idRegistro);
            }
            TblCapacitaciones idCapacitacion = tblRegistroCapacidad.getIdCapacitacion();
            if (idCapacitacion != null) {
                idCapacitacion = em.getReference(idCapacitacion.getClass(), idCapacitacion.getIdCapacitaciones());
                tblRegistroCapacidad.setIdCapacitacion(idCapacitacion);
            }
            em.persist(tblRegistroCapacidad);
            if (idRegistro != null) {
                idRegistro.getTblRegistroCapacidadCollection().add(tblRegistroCapacidad);
                idRegistro = em.merge(idRegistro);
            }
            if (idCapacitacion != null) {
                idCapacitacion.getTblRegistroCapacidadCollection().add(tblRegistroCapacidad);
                idCapacitacion = em.merge(idCapacitacion);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblRegistroCapacidad tblRegistroCapacidad) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistroCapacidad persistentTblRegistroCapacidad = em.find(TblRegistroCapacidad.class, tblRegistroCapacidad.getIdRegistroCapacidad());
            TblRegistro idRegistroOld = persistentTblRegistroCapacidad.getIdRegistro();
            TblRegistro idRegistroNew = tblRegistroCapacidad.getIdRegistro();
            TblCapacitaciones idCapacitacionOld = persistentTblRegistroCapacidad.getIdCapacitacion();
            TblCapacitaciones idCapacitacionNew = tblRegistroCapacidad.getIdCapacitacion();
            if (idRegistroNew != null) {
                idRegistroNew = em.getReference(idRegistroNew.getClass(), idRegistroNew.getIdRegistro());
                tblRegistroCapacidad.setIdRegistro(idRegistroNew);
            }
            if (idCapacitacionNew != null) {
                idCapacitacionNew = em.getReference(idCapacitacionNew.getClass(), idCapacitacionNew.getIdCapacitaciones());
                tblRegistroCapacidad.setIdCapacitacion(idCapacitacionNew);
            }
            tblRegistroCapacidad = em.merge(tblRegistroCapacidad);
            if (idRegistroOld != null && !idRegistroOld.equals(idRegistroNew)) {
                idRegistroOld.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidad);
                idRegistroOld = em.merge(idRegistroOld);
            }
            if (idRegistroNew != null && !idRegistroNew.equals(idRegistroOld)) {
                idRegistroNew.getTblRegistroCapacidadCollection().add(tblRegistroCapacidad);
                idRegistroNew = em.merge(idRegistroNew);
            }
            if (idCapacitacionOld != null && !idCapacitacionOld.equals(idCapacitacionNew)) {
                idCapacitacionOld.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidad);
                idCapacitacionOld = em.merge(idCapacitacionOld);
            }
            if (idCapacitacionNew != null && !idCapacitacionNew.equals(idCapacitacionOld)) {
                idCapacitacionNew.getTblRegistroCapacidadCollection().add(tblRegistroCapacidad);
                idCapacitacionNew = em.merge(idCapacitacionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblRegistroCapacidad.getIdRegistroCapacidad();
                if (findTblRegistroCapacidad(id) == null) {
                    throw new NonexistentEntityException("The tblRegistroCapacidad with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblRegistroCapacidad tblRegistroCapacidad;
            try {
                tblRegistroCapacidad = em.getReference(TblRegistroCapacidad.class, id);
                tblRegistroCapacidad.getIdRegistroCapacidad();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblRegistroCapacidad with id " + id + " no longer exists.", enfe);
            }
            TblRegistro idRegistro = tblRegistroCapacidad.getIdRegistro();
            if (idRegistro != null) {
                idRegistro.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidad);
                idRegistro = em.merge(idRegistro);
            }
            TblCapacitaciones idCapacitacion = tblRegistroCapacidad.getIdCapacitacion();
            if (idCapacitacion != null) {
                idCapacitacion.getTblRegistroCapacidadCollection().remove(tblRegistroCapacidad);
                idCapacitacion = em.merge(idCapacitacion);
            }
            em.remove(tblRegistroCapacidad);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblRegistroCapacidad> findTblRegistroCapacidadEntities() {
        return findTblRegistroCapacidadEntities(true, -1, -1);
    }

    public List<TblRegistroCapacidad> findTblRegistroCapacidadEntities(int maxResults, int firstResult) {
        return findTblRegistroCapacidadEntities(false, maxResults, firstResult);
    }

    private List<TblRegistroCapacidad> findTblRegistroCapacidadEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblRegistroCapacidad.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblRegistroCapacidad findTblRegistroCapacidad(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblRegistroCapacidad.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblRegistroCapacidadCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblRegistroCapacidad> rt = cq.from(TblRegistroCapacidad.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
