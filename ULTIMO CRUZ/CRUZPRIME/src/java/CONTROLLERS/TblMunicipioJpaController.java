/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblDepartamento;
import java.util.ArrayList;
import java.util.Collection;
import PERSISTENCIA.TblDireccion;
import PERSISTENCIA.TblMunicipio;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblMunicipioJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TblMunicipioJpaController() {
        this.emf = Persistence.createEntityManagerFactory("CRUZPRIMEPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblMunicipio tblMunicipio) {
        if (tblMunicipio.getTblDepartamentoCollection() == null) {
            tblMunicipio.setTblDepartamentoCollection(new ArrayList<TblDepartamento>());
        }
        if (tblMunicipio.getTblDireccionCollection() == null) {
            tblMunicipio.setTblDireccionCollection(new ArrayList<TblDireccion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<TblDepartamento> attachedTblDepartamentoCollection = new ArrayList<TblDepartamento>();
            for (TblDepartamento tblDepartamentoCollectionTblDepartamentoToAttach : tblMunicipio.getTblDepartamentoCollection()) {
                tblDepartamentoCollectionTblDepartamentoToAttach = em.getReference(tblDepartamentoCollectionTblDepartamentoToAttach.getClass(), tblDepartamentoCollectionTblDepartamentoToAttach.getIdDepto());
                attachedTblDepartamentoCollection.add(tblDepartamentoCollectionTblDepartamentoToAttach);
            }
            tblMunicipio.setTblDepartamentoCollection(attachedTblDepartamentoCollection);
            Collection<TblDireccion> attachedTblDireccionCollection = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionTblDireccionToAttach : tblMunicipio.getTblDireccionCollection()) {
                tblDireccionCollectionTblDireccionToAttach = em.getReference(tblDireccionCollectionTblDireccionToAttach.getClass(), tblDireccionCollectionTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollection.add(tblDireccionCollectionTblDireccionToAttach);
            }
            tblMunicipio.setTblDireccionCollection(attachedTblDireccionCollection);
            em.persist(tblMunicipio);
            for (TblDepartamento tblDepartamentoCollectionTblDepartamento : tblMunicipio.getTblDepartamentoCollection()) {
                TblMunicipio oldIdMunicipioOfTblDepartamentoCollectionTblDepartamento = tblDepartamentoCollectionTblDepartamento.getIdMunicipio();
                tblDepartamentoCollectionTblDepartamento.setIdMunicipio(tblMunicipio);
                tblDepartamentoCollectionTblDepartamento = em.merge(tblDepartamentoCollectionTblDepartamento);
                if (oldIdMunicipioOfTblDepartamentoCollectionTblDepartamento != null) {
                    oldIdMunicipioOfTblDepartamentoCollectionTblDepartamento.getTblDepartamentoCollection().remove(tblDepartamentoCollectionTblDepartamento);
                    oldIdMunicipioOfTblDepartamentoCollectionTblDepartamento = em.merge(oldIdMunicipioOfTblDepartamentoCollectionTblDepartamento);
                }
            }
            for (TblDireccion tblDireccionCollectionTblDireccion : tblMunicipio.getTblDireccionCollection()) {
                TblMunicipio oldIdMunicipioOfTblDireccionCollectionTblDireccion = tblDireccionCollectionTblDireccion.getIdMunicipio();
                tblDireccionCollectionTblDireccion.setIdMunicipio(tblMunicipio);
                tblDireccionCollectionTblDireccion = em.merge(tblDireccionCollectionTblDireccion);
                if (oldIdMunicipioOfTblDireccionCollectionTblDireccion != null) {
                    oldIdMunicipioOfTblDireccionCollectionTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionTblDireccion);
                    oldIdMunicipioOfTblDireccionCollectionTblDireccion = em.merge(oldIdMunicipioOfTblDireccionCollectionTblDireccion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblMunicipio tblMunicipio) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblMunicipio persistentTblMunicipio = em.find(TblMunicipio.class, tblMunicipio.getIdMunicipio());
            Collection<TblDepartamento> tblDepartamentoCollectionOld = persistentTblMunicipio.getTblDepartamentoCollection();
            Collection<TblDepartamento> tblDepartamentoCollectionNew = tblMunicipio.getTblDepartamentoCollection();
            Collection<TblDireccion> tblDireccionCollectionOld = persistentTblMunicipio.getTblDireccionCollection();
            Collection<TblDireccion> tblDireccionCollectionNew = tblMunicipio.getTblDireccionCollection();
            List<String> illegalOrphanMessages = null;
            for (TblDepartamento tblDepartamentoCollectionOldTblDepartamento : tblDepartamentoCollectionOld) {
                if (!tblDepartamentoCollectionNew.contains(tblDepartamentoCollectionOldTblDepartamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDepartamento " + tblDepartamentoCollectionOldTblDepartamento + " since its idMunicipio field is not nullable.");
                }
            }
            for (TblDireccion tblDireccionCollectionOldTblDireccion : tblDireccionCollectionOld) {
                if (!tblDireccionCollectionNew.contains(tblDireccionCollectionOldTblDireccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblDireccion " + tblDireccionCollectionOldTblDireccion + " since its idMunicipio field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<TblDepartamento> attachedTblDepartamentoCollectionNew = new ArrayList<TblDepartamento>();
            for (TblDepartamento tblDepartamentoCollectionNewTblDepartamentoToAttach : tblDepartamentoCollectionNew) {
                tblDepartamentoCollectionNewTblDepartamentoToAttach = em.getReference(tblDepartamentoCollectionNewTblDepartamentoToAttach.getClass(), tblDepartamentoCollectionNewTblDepartamentoToAttach.getIdDepto());
                attachedTblDepartamentoCollectionNew.add(tblDepartamentoCollectionNewTblDepartamentoToAttach);
            }
            tblDepartamentoCollectionNew = attachedTblDepartamentoCollectionNew;
            tblMunicipio.setTblDepartamentoCollection(tblDepartamentoCollectionNew);
            Collection<TblDireccion> attachedTblDireccionCollectionNew = new ArrayList<TblDireccion>();
            for (TblDireccion tblDireccionCollectionNewTblDireccionToAttach : tblDireccionCollectionNew) {
                tblDireccionCollectionNewTblDireccionToAttach = em.getReference(tblDireccionCollectionNewTblDireccionToAttach.getClass(), tblDireccionCollectionNewTblDireccionToAttach.getIdDireccion());
                attachedTblDireccionCollectionNew.add(tblDireccionCollectionNewTblDireccionToAttach);
            }
            tblDireccionCollectionNew = attachedTblDireccionCollectionNew;
            tblMunicipio.setTblDireccionCollection(tblDireccionCollectionNew);
            tblMunicipio = em.merge(tblMunicipio);
            for (TblDepartamento tblDepartamentoCollectionNewTblDepartamento : tblDepartamentoCollectionNew) {
                if (!tblDepartamentoCollectionOld.contains(tblDepartamentoCollectionNewTblDepartamento)) {
                    TblMunicipio oldIdMunicipioOfTblDepartamentoCollectionNewTblDepartamento = tblDepartamentoCollectionNewTblDepartamento.getIdMunicipio();
                    tblDepartamentoCollectionNewTblDepartamento.setIdMunicipio(tblMunicipio);
                    tblDepartamentoCollectionNewTblDepartamento = em.merge(tblDepartamentoCollectionNewTblDepartamento);
                    if (oldIdMunicipioOfTblDepartamentoCollectionNewTblDepartamento != null && !oldIdMunicipioOfTblDepartamentoCollectionNewTblDepartamento.equals(tblMunicipio)) {
                        oldIdMunicipioOfTblDepartamentoCollectionNewTblDepartamento.getTblDepartamentoCollection().remove(tblDepartamentoCollectionNewTblDepartamento);
                        oldIdMunicipioOfTblDepartamentoCollectionNewTblDepartamento = em.merge(oldIdMunicipioOfTblDepartamentoCollectionNewTblDepartamento);
                    }
                }
            }
            for (TblDireccion tblDireccionCollectionNewTblDireccion : tblDireccionCollectionNew) {
                if (!tblDireccionCollectionOld.contains(tblDireccionCollectionNewTblDireccion)) {
                    TblMunicipio oldIdMunicipioOfTblDireccionCollectionNewTblDireccion = tblDireccionCollectionNewTblDireccion.getIdMunicipio();
                    tblDireccionCollectionNewTblDireccion.setIdMunicipio(tblMunicipio);
                    tblDireccionCollectionNewTblDireccion = em.merge(tblDireccionCollectionNewTblDireccion);
                    if (oldIdMunicipioOfTblDireccionCollectionNewTblDireccion != null && !oldIdMunicipioOfTblDireccionCollectionNewTblDireccion.equals(tblMunicipio)) {
                        oldIdMunicipioOfTblDireccionCollectionNewTblDireccion.getTblDireccionCollection().remove(tblDireccionCollectionNewTblDireccion);
                        oldIdMunicipioOfTblDireccionCollectionNewTblDireccion = em.merge(oldIdMunicipioOfTblDireccionCollectionNewTblDireccion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblMunicipio.getIdMunicipio();
                if (findTblMunicipio(id) == null) {
                    throw new NonexistentEntityException("The tblMunicipio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblMunicipio tblMunicipio;
            try {
                tblMunicipio = em.getReference(TblMunicipio.class, id);
                tblMunicipio.getIdMunicipio();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblMunicipio with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblDepartamento> tblDepartamentoCollectionOrphanCheck = tblMunicipio.getTblDepartamentoCollection();
            for (TblDepartamento tblDepartamentoCollectionOrphanCheckTblDepartamento : tblDepartamentoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblMunicipio (" + tblMunicipio + ") cannot be destroyed since the TblDepartamento " + tblDepartamentoCollectionOrphanCheckTblDepartamento + " in its tblDepartamentoCollection field has a non-nullable idMunicipio field.");
            }
            Collection<TblDireccion> tblDireccionCollectionOrphanCheck = tblMunicipio.getTblDireccionCollection();
            for (TblDireccion tblDireccionCollectionOrphanCheckTblDireccion : tblDireccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblMunicipio (" + tblMunicipio + ") cannot be destroyed since the TblDireccion " + tblDireccionCollectionOrphanCheckTblDireccion + " in its tblDireccionCollection field has a non-nullable idMunicipio field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tblMunicipio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblMunicipio> findTblMunicipioEntities() {
        return findTblMunicipioEntities(true, -1, -1);
    }

    public List<TblMunicipio> findTblMunicipioEntities(int maxResults, int firstResult) {
        return findTblMunicipioEntities(false, maxResults, firstResult);
    }

    private List<TblMunicipio> findTblMunicipioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblMunicipio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblMunicipio findTblMunicipio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblMunicipio.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblMunicipioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblMunicipio> rt = cq.from(TblMunicipio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
