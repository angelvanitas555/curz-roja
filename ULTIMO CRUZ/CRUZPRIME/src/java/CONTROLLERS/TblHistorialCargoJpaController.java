/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import PERSISTENCIA.TblHistorialCargo;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblSeccion;
import PERSISTENCIA.TblRegistroHistorial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblHistorialCargoJpaController implements Serializable {

    public TblHistorialCargoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblHistorialCargo tblHistorialCargo) {
        if (tblHistorialCargo.getTblRegistroHistorialCollection() == null) {
            tblHistorialCargo.setTblRegistroHistorialCollection(new ArrayList<TblRegistroHistorial>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblSeccion idSeccion = tblHistorialCargo.getIdSeccion();
            if (idSeccion != null) {
                idSeccion = em.getReference(idSeccion.getClass(), idSeccion.getIdSeccion());
                tblHistorialCargo.setIdSeccion(idSeccion);
            }
            Collection<TblRegistroHistorial> attachedTblRegistroHistorialCollection = new ArrayList<TblRegistroHistorial>();
            for (TblRegistroHistorial tblRegistroHistorialCollectionTblRegistroHistorialToAttach : tblHistorialCargo.getTblRegistroHistorialCollection()) {
                tblRegistroHistorialCollectionTblRegistroHistorialToAttach = em.getReference(tblRegistroHistorialCollectionTblRegistroHistorialToAttach.getClass(), tblRegistroHistorialCollectionTblRegistroHistorialToAttach.getIdRegistroHistorial());
                attachedTblRegistroHistorialCollection.add(tblRegistroHistorialCollectionTblRegistroHistorialToAttach);
            }
            tblHistorialCargo.setTblRegistroHistorialCollection(attachedTblRegistroHistorialCollection);
            em.persist(tblHistorialCargo);
            if (idSeccion != null) {
                idSeccion.getTblHistorialCargoCollection().add(tblHistorialCargo);
                idSeccion = em.merge(idSeccion);
            }
            for (TblRegistroHistorial tblRegistroHistorialCollectionTblRegistroHistorial : tblHistorialCargo.getTblRegistroHistorialCollection()) {
                TblHistorialCargo oldIdHistorialCargoOfTblRegistroHistorialCollectionTblRegistroHistorial = tblRegistroHistorialCollectionTblRegistroHistorial.getIdHistorialCargo();
                tblRegistroHistorialCollectionTblRegistroHistorial.setIdHistorialCargo(tblHistorialCargo);
                tblRegistroHistorialCollectionTblRegistroHistorial = em.merge(tblRegistroHistorialCollectionTblRegistroHistorial);
                if (oldIdHistorialCargoOfTblRegistroHistorialCollectionTblRegistroHistorial != null) {
                    oldIdHistorialCargoOfTblRegistroHistorialCollectionTblRegistroHistorial.getTblRegistroHistorialCollection().remove(tblRegistroHistorialCollectionTblRegistroHistorial);
                    oldIdHistorialCargoOfTblRegistroHistorialCollectionTblRegistroHistorial = em.merge(oldIdHistorialCargoOfTblRegistroHistorialCollectionTblRegistroHistorial);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblHistorialCargo tblHistorialCargo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblHistorialCargo persistentTblHistorialCargo = em.find(TblHistorialCargo.class, tblHistorialCargo.getIdHistorialCargo());
            TblSeccion idSeccionOld = persistentTblHistorialCargo.getIdSeccion();
            TblSeccion idSeccionNew = tblHistorialCargo.getIdSeccion();
            Collection<TblRegistroHistorial> tblRegistroHistorialCollectionOld = persistentTblHistorialCargo.getTblRegistroHistorialCollection();
            Collection<TblRegistroHistorial> tblRegistroHistorialCollectionNew = tblHistorialCargo.getTblRegistroHistorialCollection();
            List<String> illegalOrphanMessages = null;
            for (TblRegistroHistorial tblRegistroHistorialCollectionOldTblRegistroHistorial : tblRegistroHistorialCollectionOld) {
                if (!tblRegistroHistorialCollectionNew.contains(tblRegistroHistorialCollectionOldTblRegistroHistorial)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblRegistroHistorial " + tblRegistroHistorialCollectionOldTblRegistroHistorial + " since its idHistorialCargo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idSeccionNew != null) {
                idSeccionNew = em.getReference(idSeccionNew.getClass(), idSeccionNew.getIdSeccion());
                tblHistorialCargo.setIdSeccion(idSeccionNew);
            }
            Collection<TblRegistroHistorial> attachedTblRegistroHistorialCollectionNew = new ArrayList<TblRegistroHistorial>();
            for (TblRegistroHistorial tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach : tblRegistroHistorialCollectionNew) {
                tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach = em.getReference(tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach.getClass(), tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach.getIdRegistroHistorial());
                attachedTblRegistroHistorialCollectionNew.add(tblRegistroHistorialCollectionNewTblRegistroHistorialToAttach);
            }
            tblRegistroHistorialCollectionNew = attachedTblRegistroHistorialCollectionNew;
            tblHistorialCargo.setTblRegistroHistorialCollection(tblRegistroHistorialCollectionNew);
            tblHistorialCargo = em.merge(tblHistorialCargo);
            if (idSeccionOld != null && !idSeccionOld.equals(idSeccionNew)) {
                idSeccionOld.getTblHistorialCargoCollection().remove(tblHistorialCargo);
                idSeccionOld = em.merge(idSeccionOld);
            }
            if (idSeccionNew != null && !idSeccionNew.equals(idSeccionOld)) {
                idSeccionNew.getTblHistorialCargoCollection().add(tblHistorialCargo);
                idSeccionNew = em.merge(idSeccionNew);
            }
            for (TblRegistroHistorial tblRegistroHistorialCollectionNewTblRegistroHistorial : tblRegistroHistorialCollectionNew) {
                if (!tblRegistroHistorialCollectionOld.contains(tblRegistroHistorialCollectionNewTblRegistroHistorial)) {
                    TblHistorialCargo oldIdHistorialCargoOfTblRegistroHistorialCollectionNewTblRegistroHistorial = tblRegistroHistorialCollectionNewTblRegistroHistorial.getIdHistorialCargo();
                    tblRegistroHistorialCollectionNewTblRegistroHistorial.setIdHistorialCargo(tblHistorialCargo);
                    tblRegistroHistorialCollectionNewTblRegistroHistorial = em.merge(tblRegistroHistorialCollectionNewTblRegistroHistorial);
                    if (oldIdHistorialCargoOfTblRegistroHistorialCollectionNewTblRegistroHistorial != null && !oldIdHistorialCargoOfTblRegistroHistorialCollectionNewTblRegistroHistorial.equals(tblHistorialCargo)) {
                        oldIdHistorialCargoOfTblRegistroHistorialCollectionNewTblRegistroHistorial.getTblRegistroHistorialCollection().remove(tblRegistroHistorialCollectionNewTblRegistroHistorial);
                        oldIdHistorialCargoOfTblRegistroHistorialCollectionNewTblRegistroHistorial = em.merge(oldIdHistorialCargoOfTblRegistroHistorialCollectionNewTblRegistroHistorial);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblHistorialCargo.getIdHistorialCargo();
                if (findTblHistorialCargo(id) == null) {
                    throw new NonexistentEntityException("The tblHistorialCargo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblHistorialCargo tblHistorialCargo;
            try {
                tblHistorialCargo = em.getReference(TblHistorialCargo.class, id);
                tblHistorialCargo.getIdHistorialCargo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblHistorialCargo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblRegistroHistorial> tblRegistroHistorialCollectionOrphanCheck = tblHistorialCargo.getTblRegistroHistorialCollection();
            for (TblRegistroHistorial tblRegistroHistorialCollectionOrphanCheckTblRegistroHistorial : tblRegistroHistorialCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblHistorialCargo (" + tblHistorialCargo + ") cannot be destroyed since the TblRegistroHistorial " + tblRegistroHistorialCollectionOrphanCheckTblRegistroHistorial + " in its tblRegistroHistorialCollection field has a non-nullable idHistorialCargo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TblSeccion idSeccion = tblHistorialCargo.getIdSeccion();
            if (idSeccion != null) {
                idSeccion.getTblHistorialCargoCollection().remove(tblHistorialCargo);
                idSeccion = em.merge(idSeccion);
            }
            em.remove(tblHistorialCargo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblHistorialCargo> findTblHistorialCargoEntities() {
        return findTblHistorialCargoEntities(true, -1, -1);
    }

    public List<TblHistorialCargo> findTblHistorialCargoEntities(int maxResults, int firstResult) {
        return findTblHistorialCargoEntities(false, maxResults, firstResult);
    }

    private List<TblHistorialCargo> findTblHistorialCargoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblHistorialCargo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblHistorialCargo findTblHistorialCargo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblHistorialCargo.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblHistorialCargoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblHistorialCargo> rt = cq.from(TblHistorialCargo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
