/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import PERSISTENCIA.TblArea;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblSolicitudIngreso;
import java.util.ArrayList;
import java.util.Collection;
import PERSISTENCIA.TblRegistro;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblAreaJpaController implements Serializable {

    public TblAreaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblArea tblArea) {
        if (tblArea.getTblSolicitudIngresoCollection() == null) {
            tblArea.setTblSolicitudIngresoCollection(new ArrayList<TblSolicitudIngreso>());
        }
        if (tblArea.getTblRegistroCollection() == null) {
            tblArea.setTblRegistroCollection(new ArrayList<TblRegistro>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollection = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach : tblArea.getTblSolicitudIngresoCollection()) {
                tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollection.add(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach);
            }
            tblArea.setTblSolicitudIngresoCollection(attachedTblSolicitudIngresoCollection);
            Collection<TblRegistro> attachedTblRegistroCollection = new ArrayList<TblRegistro>();
            for (TblRegistro tblRegistroCollectionTblRegistroToAttach : tblArea.getTblRegistroCollection()) {
                tblRegistroCollectionTblRegistroToAttach = em.getReference(tblRegistroCollectionTblRegistroToAttach.getClass(), tblRegistroCollectionTblRegistroToAttach.getIdRegistro());
                attachedTblRegistroCollection.add(tblRegistroCollectionTblRegistroToAttach);
            }
            tblArea.setTblRegistroCollection(attachedTblRegistroCollection);
            em.persist(tblArea);
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngreso : tblArea.getTblSolicitudIngresoCollection()) {
                TblArea oldIdAreaOfTblSolicitudIngresoCollectionTblSolicitudIngreso = tblSolicitudIngresoCollectionTblSolicitudIngreso.getIdArea();
                tblSolicitudIngresoCollectionTblSolicitudIngreso.setIdArea(tblArea);
                tblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                if (oldIdAreaOfTblSolicitudIngresoCollectionTblSolicitudIngreso != null) {
                    oldIdAreaOfTblSolicitudIngresoCollectionTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                    oldIdAreaOfTblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(oldIdAreaOfTblSolicitudIngresoCollectionTblSolicitudIngreso);
                }
            }
            for (TblRegistro tblRegistroCollectionTblRegistro : tblArea.getTblRegistroCollection()) {
                TblArea oldIdAreaOfTblRegistroCollectionTblRegistro = tblRegistroCollectionTblRegistro.getIdArea();
                tblRegistroCollectionTblRegistro.setIdArea(tblArea);
                tblRegistroCollectionTblRegistro = em.merge(tblRegistroCollectionTblRegistro);
                if (oldIdAreaOfTblRegistroCollectionTblRegistro != null) {
                    oldIdAreaOfTblRegistroCollectionTblRegistro.getTblRegistroCollection().remove(tblRegistroCollectionTblRegistro);
                    oldIdAreaOfTblRegistroCollectionTblRegistro = em.merge(oldIdAreaOfTblRegistroCollectionTblRegistro);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblArea tblArea) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblArea persistentTblArea = em.find(TblArea.class, tblArea.getIdArea());
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOld = persistentTblArea.getTblSolicitudIngresoCollection();
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionNew = tblArea.getTblSolicitudIngresoCollection();
            Collection<TblRegistro> tblRegistroCollectionOld = persistentTblArea.getTblRegistroCollection();
            Collection<TblRegistro> tblRegistroCollectionNew = tblArea.getTblRegistroCollection();
            List<String> illegalOrphanMessages = null;
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOldTblSolicitudIngreso : tblSolicitudIngresoCollectionOld) {
                if (!tblSolicitudIngresoCollectionNew.contains(tblSolicitudIngresoCollectionOldTblSolicitudIngreso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblSolicitudIngreso " + tblSolicitudIngresoCollectionOldTblSolicitudIngreso + " since its idArea field is not nullable.");
                }
            }
            for (TblRegistro tblRegistroCollectionOldTblRegistro : tblRegistroCollectionOld) {
                if (!tblRegistroCollectionNew.contains(tblRegistroCollectionOldTblRegistro)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblRegistro " + tblRegistroCollectionOldTblRegistro + " since its idArea field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollectionNew = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach : tblSolicitudIngresoCollectionNew) {
                tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollectionNew.add(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach);
            }
            tblSolicitudIngresoCollectionNew = attachedTblSolicitudIngresoCollectionNew;
            tblArea.setTblSolicitudIngresoCollection(tblSolicitudIngresoCollectionNew);
            Collection<TblRegistro> attachedTblRegistroCollectionNew = new ArrayList<TblRegistro>();
            for (TblRegistro tblRegistroCollectionNewTblRegistroToAttach : tblRegistroCollectionNew) {
                tblRegistroCollectionNewTblRegistroToAttach = em.getReference(tblRegistroCollectionNewTblRegistroToAttach.getClass(), tblRegistroCollectionNewTblRegistroToAttach.getIdRegistro());
                attachedTblRegistroCollectionNew.add(tblRegistroCollectionNewTblRegistroToAttach);
            }
            tblRegistroCollectionNew = attachedTblRegistroCollectionNew;
            tblArea.setTblRegistroCollection(tblRegistroCollectionNew);
            tblArea = em.merge(tblArea);
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngreso : tblSolicitudIngresoCollectionNew) {
                if (!tblSolicitudIngresoCollectionOld.contains(tblSolicitudIngresoCollectionNewTblSolicitudIngreso)) {
                    TblArea oldIdAreaOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = tblSolicitudIngresoCollectionNewTblSolicitudIngreso.getIdArea();
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso.setIdArea(tblArea);
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    if (oldIdAreaOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso != null && !oldIdAreaOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.equals(tblArea)) {
                        oldIdAreaOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                        oldIdAreaOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(oldIdAreaOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    }
                }
            }
            for (TblRegistro tblRegistroCollectionNewTblRegistro : tblRegistroCollectionNew) {
                if (!tblRegistroCollectionOld.contains(tblRegistroCollectionNewTblRegistro)) {
                    TblArea oldIdAreaOfTblRegistroCollectionNewTblRegistro = tblRegistroCollectionNewTblRegistro.getIdArea();
                    tblRegistroCollectionNewTblRegistro.setIdArea(tblArea);
                    tblRegistroCollectionNewTblRegistro = em.merge(tblRegistroCollectionNewTblRegistro);
                    if (oldIdAreaOfTblRegistroCollectionNewTblRegistro != null && !oldIdAreaOfTblRegistroCollectionNewTblRegistro.equals(tblArea)) {
                        oldIdAreaOfTblRegistroCollectionNewTblRegistro.getTblRegistroCollection().remove(tblRegistroCollectionNewTblRegistro);
                        oldIdAreaOfTblRegistroCollectionNewTblRegistro = em.merge(oldIdAreaOfTblRegistroCollectionNewTblRegistro);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblArea.getIdArea();
                if (findTblArea(id) == null) {
                    throw new NonexistentEntityException("The tblArea with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblArea tblArea;
            try {
                tblArea = em.getReference(TblArea.class, id);
                tblArea.getIdArea();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblArea with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOrphanCheck = tblArea.getTblSolicitudIngresoCollection();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso : tblSolicitudIngresoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblArea (" + tblArea + ") cannot be destroyed since the TblSolicitudIngreso " + tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso + " in its tblSolicitudIngresoCollection field has a non-nullable idArea field.");
            }
            Collection<TblRegistro> tblRegistroCollectionOrphanCheck = tblArea.getTblRegistroCollection();
            for (TblRegistro tblRegistroCollectionOrphanCheckTblRegistro : tblRegistroCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblArea (" + tblArea + ") cannot be destroyed since the TblRegistro " + tblRegistroCollectionOrphanCheckTblRegistro + " in its tblRegistroCollection field has a non-nullable idArea field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tblArea);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblArea> findTblAreaEntities() {
        return findTblAreaEntities(true, -1, -1);
    }

    public List<TblArea> findTblAreaEntities(int maxResults, int firstResult) {
        return findTblAreaEntities(false, maxResults, firstResult);
    }

    private List<TblArea> findTblAreaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblArea.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblArea findTblArea(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblArea.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblAreaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblArea> rt = cq.from(TblArea.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
