/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import CONTROLLERS.exceptions.IllegalOrphanException;
import CONTROLLERS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import PERSISTENCIA.TblSolicitudIngreso;
import PERSISTENCIA.TblUsuario;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author jose.azucenaUSAM
 */
public class TblUsuarioJpaController implements Serializable {

    public TblUsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblUsuario tblUsuario) {
        if (tblUsuario.getTblSolicitudIngresoCollection() == null) {
            tblUsuario.setTblSolicitudIngresoCollection(new ArrayList<TblSolicitudIngreso>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollection = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach : tblUsuario.getTblSolicitudIngresoCollection()) {
                tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollection.add(tblSolicitudIngresoCollectionTblSolicitudIngresoToAttach);
            }
            tblUsuario.setTblSolicitudIngresoCollection(attachedTblSolicitudIngresoCollection);
            em.persist(tblUsuario);
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionTblSolicitudIngreso : tblUsuario.getTblSolicitudIngresoCollection()) {
                TblUsuario oldIdUsuarioOfTblSolicitudIngresoCollectionTblSolicitudIngreso = tblSolicitudIngresoCollectionTblSolicitudIngreso.getIdUsuario();
                tblSolicitudIngresoCollectionTblSolicitudIngreso.setIdUsuario(tblUsuario);
                tblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                if (oldIdUsuarioOfTblSolicitudIngresoCollectionTblSolicitudIngreso != null) {
                    oldIdUsuarioOfTblSolicitudIngresoCollectionTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionTblSolicitudIngreso);
                    oldIdUsuarioOfTblSolicitudIngresoCollectionTblSolicitudIngreso = em.merge(oldIdUsuarioOfTblSolicitudIngresoCollectionTblSolicitudIngreso);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblUsuario tblUsuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblUsuario persistentTblUsuario = em.find(TblUsuario.class, tblUsuario.getIdUsuario());
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOld = persistentTblUsuario.getTblSolicitudIngresoCollection();
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionNew = tblUsuario.getTblSolicitudIngresoCollection();
            List<String> illegalOrphanMessages = null;
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOldTblSolicitudIngreso : tblSolicitudIngresoCollectionOld) {
                if (!tblSolicitudIngresoCollectionNew.contains(tblSolicitudIngresoCollectionOldTblSolicitudIngreso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TblSolicitudIngreso " + tblSolicitudIngresoCollectionOldTblSolicitudIngreso + " since its idUsuario field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<TblSolicitudIngreso> attachedTblSolicitudIngresoCollectionNew = new ArrayList<TblSolicitudIngreso>();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach : tblSolicitudIngresoCollectionNew) {
                tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach = em.getReference(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getClass(), tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach.getIdSolicitud());
                attachedTblSolicitudIngresoCollectionNew.add(tblSolicitudIngresoCollectionNewTblSolicitudIngresoToAttach);
            }
            tblSolicitudIngresoCollectionNew = attachedTblSolicitudIngresoCollectionNew;
            tblUsuario.setTblSolicitudIngresoCollection(tblSolicitudIngresoCollectionNew);
            tblUsuario = em.merge(tblUsuario);
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionNewTblSolicitudIngreso : tblSolicitudIngresoCollectionNew) {
                if (!tblSolicitudIngresoCollectionOld.contains(tblSolicitudIngresoCollectionNewTblSolicitudIngreso)) {
                    TblUsuario oldIdUsuarioOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = tblSolicitudIngresoCollectionNewTblSolicitudIngreso.getIdUsuario();
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso.setIdUsuario(tblUsuario);
                    tblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    if (oldIdUsuarioOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso != null && !oldIdUsuarioOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.equals(tblUsuario)) {
                        oldIdUsuarioOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso.getTblSolicitudIngresoCollection().remove(tblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                        oldIdUsuarioOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso = em.merge(oldIdUsuarioOfTblSolicitudIngresoCollectionNewTblSolicitudIngreso);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblUsuario.getIdUsuario();
                if (findTblUsuario(id) == null) {
                    throw new NonexistentEntityException("The tblUsuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblUsuario tblUsuario;
            try {
                tblUsuario = em.getReference(TblUsuario.class, id);
                tblUsuario.getIdUsuario();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblUsuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<TblSolicitudIngreso> tblSolicitudIngresoCollectionOrphanCheck = tblUsuario.getTblSolicitudIngresoCollection();
            for (TblSolicitudIngreso tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso : tblSolicitudIngresoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TblUsuario (" + tblUsuario + ") cannot be destroyed since the TblSolicitudIngreso " + tblSolicitudIngresoCollectionOrphanCheckTblSolicitudIngreso + " in its tblSolicitudIngresoCollection field has a non-nullable idUsuario field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tblUsuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblUsuario> findTblUsuarioEntities() {
        return findTblUsuarioEntities(true, -1, -1);
    }

    public List<TblUsuario> findTblUsuarioEntities(int maxResults, int firstResult) {
        return findTblUsuarioEntities(false, maxResults, firstResult);
    }

    private List<TblUsuario> findTblUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblUsuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblUsuario findTblUsuario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblUsuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblUsuario> rt = cq.from(TblUsuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
