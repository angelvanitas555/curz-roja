
// Para mantener la clase active en el area en la que estoy
(function (d) {
    let tabs = Array.prototype.slice.apply(d.querySelectorAll('.menu__item'));
    let secciones = Array.prototype.slice.apply(d.querySelectorAll('.sec'));

    d.getElementById('menu').addEventListener("click", e => {
        if (e.target.classList.contains('menu__item')) {

            let i = tabs.indexOf(e.target);
            tabs.map(tab => tab.classList.remove('active'));
            tabs[i].classList.add('active');

            secciones.map(seccion => seccion.classList.remove('activeSec'));
            secciones[i].classList.add('activeSec');

        }
    });


})(document);


